package com.watabou.pd.desktop;

import com.badlogic.gdx.Files;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;
import com.badlogic.gdx.utils.SharedLibraryLoader;
import com.watabou.input.NoosaInputProcessor;
import com.watabou.pixeldungeon.PixelDungeon;
import com.watabou.utils.PDPlatformSupport;

public class DesktopLauncher {
	public static void main (String[] arg) {
		String version = DesktopLauncher.class.getPackage().getSpecificationVersion();
		if (version == null) {
			version = "???";
		}
		Lwjgl3ApplicationConfiguration config = new Lwjgl3ApplicationConfiguration();

		String preferencesDirectory = "Pixel_Dungeon/";
		if (SharedLibraryLoader.isMac) {
			preferencesDirectory = "Library/Application Support/Pixel Dungeon/";
		} else if (SharedLibraryLoader.isLinux) {
			preferencesDirectory = ".watabou/pixel-dungeon/";
		} else if (SharedLibraryLoader.isWindows) {
			preferencesDirectory = "Saved Games/";
		}
		config.setPreferencesConfig(preferencesDirectory, Files.FileType.External);

		config.setWindowIcon(Files.FileType.Internal, "ic_launcher_128.png", "ic_launcher_32.png",
				"ic_launcher_16.png");

		// TODO: It have to be pulled from build.gradle, but I don't know how it can be done
		config.setTitle( "Pixel Dungeon" );

		new Lwjgl3Application(new PixelDungeon(
				new DesktopPlatformSupport(version, preferencesDirectory, new DesktopInputProcessor())
		), config);
	}

	private static class DesktopPlatformSupport extends PDPlatformSupport {
		public DesktopPlatformSupport( String version, String basePath, NoosaInputProcessor inputProcessor ) {
			super( version, basePath, inputProcessor );
		}

		@Override
		public void updateDisplaySize(boolean isLandscapeMode) {

		}

		@Override
		public void updateSystemUI(boolean isFullscreen) {

		}

		@Override
		public boolean isFullscreenEnabled() {
            return !SharedLibraryLoader.isMac;
		}
	}
}
