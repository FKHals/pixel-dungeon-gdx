package com.watabou.pd.desktop;

import com.watabou.pixeldungeon.levels.*;
import com.watabou.pixeldungeon.quests.QuestManager;
import com.watabou.utils.Point;
import com.watabou.utils.Random;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * This draws a (random) level as an image for level generation debugging purposes.
 */
public class LevelGenerator {

    static final String FILE_NAME = "level.png";
    // size of the side of each pixel in the image (16 -> each cell in a level corresponds to 16x16 subpixels)
    static final int UP_SCALE_FACTOR = 16;

    public static void main (String[] arg) {

        /**
         * This just empties the Level#createMobs and Level#createItems methods to
         * prevent Actor-init-errors which are not needed to paint the level anyway.
         */
        RegularLevel level = new PrisonLevel(){
            @Override
            protected void createMobs(GenerationConstraints constraints) { /* do nothing */ }
            @Override
            protected void createItems(GenerationConstraints constraints) { /* do nothing */ }
        };

        Random random = new Random(0);

        level.create(
                new Level.GenerationConstraints(
                        0,
                        // this flag disables generation of items etc. (otherwise a NullException gets raised since
                        // Generator.random is not initialized)
                        true,
                        false,
                        false,
                        false,
                        false,
                        false,
                        false,
                        random,
                        new ArrayList<>(Arrays.asList(random.nextShuffle(Room.getSpecials())))
                ),
                new QuestManager()
        );
        try {
            printImage(level, UP_SCALE_FACTOR, FILE_NAME);
        } catch (IOException | AssertionError e) {
            e.printStackTrace();
        }
    }

    /**
     * @param level The level that the layout id drawn of
     * @param upScaleFactor The factor by which the image gets upscaled (each side gets longer by that factor)
     * @param filename The name of the output image file
     */
    private static void printImage(Level level, int upScaleFactor, String filename) throws IOException {

        BufferedImage image = new BufferedImage(
                level.width() * upScaleFactor,
                level.height() * upScaleFactor,
                BufferedImage.TYPE_INT_RGB);

        // define colors
        final int black = 0x000000;
        final int white = 0xffffff;
        final int brown = 0x995500;
        final int redBright = 0xff0000;
        final int redDark = 0xbb0000;
        final int blue = 0x0055ff;
        final int green = 0x009900;
        // pixel color as RGB
        int pixelColor;

        if (level == null) {
            throw new AssertionError("Invalid level");
        }

        // draw image
        for (int levelPosX = 0; levelPosX < level.width(); levelPosX++) {
            for (int levelPosY = 0; levelPosY < level.height(); levelPosY++) {

                Point cell = new Point(levelPosX, levelPosY);

                // pick the color according to certain terrain
                Terrain terrain = level.getCell(cell);
                if (terrain == Terrain.WALL || terrain == Terrain.WALL_DECO) {
                    pixelColor = black;
                } else if (terrain == Terrain.DOOR || terrain == Terrain.LOCKED_DOOR) {
                    pixelColor = brown;
                } else if (terrain == Terrain.WATER
                        || terrain == Terrain.WATER_TILE_0 || terrain == Terrain.WATER_TILE_1
                        || terrain == Terrain.WATER_TILE_2 || terrain == Terrain.WATER_TILE_3
                        || terrain == Terrain.WATER_TILE_4 || terrain == Terrain.WATER_TILE_5
                        || terrain == Terrain.WATER_TILE_6 || terrain == Terrain.WATER_TILE_7
                        || terrain == Terrain.WATER_TILE_8 || terrain == Terrain.WATER_TILE_9
                        || terrain == Terrain.WATER_TILE_10 || terrain == Terrain.WATER_TILE_11
                        || terrain == Terrain.WATER_TILE_12 || terrain == Terrain.WATER_TILE_13
                        || terrain == Terrain.WATER_TILE_14) {
                    pixelColor = blue;
                } else if (terrain == Terrain.GRASS || terrain == Terrain.HIGH_GRASS) {
                    pixelColor = green;
                } else if (terrain == Terrain.ENTRANCE) {
                    pixelColor = redBright;
                } else if (terrain == Terrain.EXIT) {
                    pixelColor = redDark;
                } else {
                    pixelColor = white;
                }

                // set the pixels to the appropriate color accourding to the scaling
                for (int pixelPosX = 0; pixelPosX < upScaleFactor; pixelPosX++) {
                    for (int pixelPosY = 0; pixelPosY < upScaleFactor; pixelPosY++) {
                        int picX = cell.x * upScaleFactor + pixelPosX;
                        int picY = cell.y * upScaleFactor + pixelPosY;
                        image.setRGB(picX, picY, pixelColor);
                    }
                }
            }
        }

        // create image file
        String fileExtension = filename.substring(filename.lastIndexOf(".") + 1);
        ImageIO.write(image, fileExtension, new File(filename));
        System.out.println("Created level image '" + System.getProperty("user.dir") + "/" + filename + "'");
    }
}
