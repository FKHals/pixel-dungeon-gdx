pixel-dungeon-gdx
=================

GDX port of the awesome [Pixel Dungeon](https://github.com/watabou/pixel-dungeon)

Requirements
------------

It must be run with **Java 8** when using **OpenJDK** (`java-1.8.0-openjdk.x86_64`).

Quickstart
----------

Do `./gradlew <task>` to compile and run the project, where `task` is:

* Desktop: `desktop:run`
* Android: `android:installDebug android:run`
* Generate IDEA project: `idea`

For more info about those and other tasks: https://github.com/libgdx/libgdx/wiki/Gradle-on-the-Commandline#running-the-html-project

Troubleshooting
---------------

**Error**: The build succeeds with `Inconsistency detected [...]` but the game won't start.
```shell
$ ./gradlew desktop:run
[...]
Inconsistency detected by ld.so: dl-lookup.c: 111: check_match: Assertion `version->filename == NULL || ! _dl_name_match_p (version->filename, map)' failed!

BUILD SUCCESSFUL
```
or:
```shell
$ ./gradlew desktop:genlevel
[...]
Exception in thread "main" java.lang.UnsatisfiedLinkError: Can't load library: /usr/lib/jvm/java-11-openjdk-11.0.14.1.1-5.fc35.x86_64/lib/libawt_xawt.so
        at java.base/java.lang.ClassLoader.loadLibrary(ClassLoader.java:2630)
[...]
```

**Problem**: It has been run with some other OpenJDK version than 8.

**Solution**:

On e.g. Fedora Linux the following command can be used to switch between multiple java versions:
```shell
sudo alternatives --config java
```
This opens a dialogue. Select `java-1.8.0-openjdk.x86_64`. Done.

-----

**Error**:
```shell
$ ./gradlew android:installDebug
[...]
Execution failed for task ':android:installDebug'.
> com.android.builder.testing.api.DeviceException: No connected devices!
```

**Problem**: Either no android device is connected or the adb server isn't running or crashed.

**Solution**:

Assuming that an android device is connected, restart the adb server:
```shell
adb kill-server
adb start-server
```

-----

**Error**:
```shell
$ ./gradlew desktop:run
[...]
> Task :android:installDebug
Installing APK 'android-debug.apk' on 'RAZR HD - 4.4.4' for android:debug
Unable to install /home/beach/Dokumente/Games/pixel-dungeon-gdx/android/build/outputs/apk/debug/android-debug.apk
com.android.ddmlib.InstallException: INSTALL_FAILED_VERSION_DOWNGRADE
        at com.android.ddmlib.Device.installRemotePackage(Device.java:1143)
[...]
FAILURE: Build failed with an exception.

* What went wrong:
Execution failed for task ':android:installDebug'.
> java.util.concurrent.ExecutionException: com.android.builder.testing.api.DeviceException: com.android.ddmlib.InstallException: INSTALL_FAILED_VERSION_DOWNGRADE
```

**Problem**: A newer version of Pixel Dungeon is already installed on the device.

**Solution**:

Uninstall any *Pixel Dungeon* application currently installed on the android device (differently named forks like *Shattered Pixel Dungeon* should not be a problem).

-----

**Error**:
```shell
Error: java.lang.ArrayIndexOutOfBoundsException: 0
```

**Problem**: Most probably Intellij was used to run the program which sometimes causes that error (might need to be further investigated).

**Solution**:

Run the game from the terminal using the commands in *Quickstart* (see above).

