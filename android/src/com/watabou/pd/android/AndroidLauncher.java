package com.watabou.pd.android;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.opengl.GLSurfaceView;
import android.os.Build;
import android.os.Bundle;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.WindowManager;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.watabou.input.NoosaInputProcessor;
import com.watabou.noosa.Game;
import com.watabou.noosa.audio.Music;
import com.watabou.pixeldungeon.PixelDungeon;
import com.watabou.pixeldungeon.input.GameAction;
import com.watabou.utils.DLog;
import com.watabou.utils.PDPlatformSupport;

import com.watabou.pixeldungeon.PixelDungeon;

public class AndroidLauncher extends AndroidApplication {

	public static AndroidApplication instance;
	protected static GLSurfaceView view;

	private AndroidPlatformSupport<GameAction> support;

	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		instance = this;

		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		String version;
		try {
			version = getPackageManager().getPackageInfo( getPackageName(), 0 ).versionName;
		} catch (PackageManager.NameNotFoundException e) {
			DLog.error("Generic", "generic error?", e);
			version = "???";
		}

		// grab preferences directly using our instance first
		// so that we don't need to rely on Gdx.app, which isn't initialized yet.
		//Preferences.setPrefsFromInstance(instance);

		config.useCompass = false;
		config.useAccelerometer = false;

		support = new AndroidPlatformSupport(version, null, new AndroidInputProcessor());
		//support.updateSystemUI();

		initialize(new PixelDungeon(new AndroidPlatformSupport<GameAction>(version, null, new AndroidInputProcessor())), config);

		view = (GLSurfaceView)graphics.getView();

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
			TelephonyManager mgr =
					(TelephonyManager) instance.getSystemService(Activity.TELEPHONY_SERVICE);
			mgr.listen(new PhoneStateListener(){

				@Override
				public void onCallStateChanged(int state, String incomingNumber)
				{
					if( state == TelephonyManager.CALL_STATE_RINGING ) {
						Music.INSTANCE.pause();

					} else if( state == TelephonyManager.CALL_STATE_IDLE ) {
						if (!Game.instance.isPaused()) {
							Music.INSTANCE.resume();
						}
					}

					super.onCallStateChanged(state, incomingNumber);
				}
			}, PhoneStateListener.LISTEN_CALL_STATE);
		}
	}

	private static class AndroidPlatformSupport<GameAction> extends PDPlatformSupport<GameAction> {
		public AndroidPlatformSupport(String version, String basePath, NoosaInputProcessor inputProcessor) {
			super(version, basePath, inputProcessor);
		}

		@Override
		public void updateDisplaySize(boolean isLandscapeMode) {
			AndroidLauncher.instance.setRequestedOrientation(isLandscapeMode ?
					ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE :
					ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
		}

		@Override
		public void updateSystemUI(final boolean isFullscreen) {

			AndroidLauncher.instance.runOnUiThread(new Runnable() {
				@SuppressLint("NewApi")
				@Override
				public void run() {
					boolean fullscreen = Build.VERSION.SDK_INT < Build.VERSION_CODES.N
							|| !AndroidLauncher.instance.isInMultiWindowMode();

					if (fullscreen) {
						AndroidLauncher.instance.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
								WindowManager.LayoutParams.FLAG_FULLSCREEN | WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
					} else {
						AndroidLauncher.instance.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN,
								WindowManager.LayoutParams.FLAG_FULLSCREEN | WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
					}

					if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
						if (isFullscreen) {
							AndroidLauncher.instance.getWindow().getDecorView().setSystemUiVisibility(
									View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
											| View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN
											| View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
						} else {
							AndroidLauncher.instance.getWindow().getDecorView().setSystemUiVisibility(
									View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
						}
					}
				}
			});

		}
	}
}
