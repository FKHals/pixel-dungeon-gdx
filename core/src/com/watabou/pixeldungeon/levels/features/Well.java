package com.watabou.pixeldungeon.levels.features;

import com.watabou.pixeldungeon.actors.Char;
import com.watabou.pixeldungeon.actors.blobs.WaterOfAwareness;
import com.watabou.pixeldungeon.actors.blobs.WaterOfHealth;
import com.watabou.pixeldungeon.actors.blobs.WaterOfTransmutation;
import com.watabou.pixeldungeon.actors.blobs.WellWater;
import com.watabou.pixeldungeon.levels.Level;
import com.watabou.pixeldungeon.levels.Terrain;
import com.watabou.pixeldungeon.scenes.GameScene;

public class Well implements TerrainFeature {

    @Override
    public void enter(Level level, int cell, Char ch ) {
        affectCell(level, cell);
    }

    public static void affectCell( Level level, int cell ) {

        Class<?>[] waters = {WaterOfHealth.class, WaterOfAwareness.class, WaterOfTransmutation.class};

        for (Class<?>waterClass : waters) {
            WellWater water = (WellWater) level.blobs.get( waterClass );
            if (water != null &&
                    water.volume > 0 &&
                    water.getPos() == cell &&
                    water.affect()) {

                Level.set( cell, Terrain.EMPTY_WELL );
                GameScene.updateMap( cell );

                return;
            }
        }
    }
}
