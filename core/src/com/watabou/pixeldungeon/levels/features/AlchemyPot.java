/*
 * Pixel Dungeon
 * Copyright (C) 2012-2014  Oleg Dolya
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.watabou.pixeldungeon.levels.features;

import com.watabou.pixeldungeon.Dungeon;
import com.watabou.pixeldungeon.actors.Char;
import com.watabou.pixeldungeon.actors.hero.Hero;
import com.watabou.pixeldungeon.items.Heap;
import com.watabou.pixeldungeon.items.Item;
import com.watabou.pixeldungeon.levels.Level;
import com.watabou.pixeldungeon.scenes.GameScene;
import com.watabou.pixeldungeon.windows.WndBag;

public class AlchemyPot implements TerrainFeature {

	@Override
	public void enter(Level level, int cell, Char ch ) {
		transmute(level, cell);
	}

	public static void transmute( Level level, int cell ) {
		Heap heap = Dungeon.level.heaps.get( cell );
		if (heap != null) {

			Item result = heap.transmute();
			if (result != null) {
				Dungeon.level.drop( result, cell ).sprite.drop( cell );
			}
		}
	}

	public static class Pot {

		private static final String TXT_SELECT_SEED = "Select a seed to throw";

		private static Hero hero;
		private static int pos;

		public static void operate(Hero hero, int pos) {

			Pot.hero = hero;
			Pot.pos = pos;

			GameScene.selectItem(itemSelector, WndBag.Mode.SEED, TXT_SELECT_SEED);
		}

		private static final WndBag.Listener itemSelector = new WndBag.Listener() {
			@Override
			public void onSelect(Item item) {
				if (item != null) {
					item.cast(hero, pos);
				}
			}
		};
	}
}
