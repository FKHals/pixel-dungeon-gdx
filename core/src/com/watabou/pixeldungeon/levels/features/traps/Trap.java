package com.watabou.pixeldungeon.levels.features.traps;

import com.watabou.pixeldungeon.actors.Char;
import com.watabou.pixeldungeon.levels.Level;
import com.watabou.pixeldungeon.levels.features.TerrainFeature;

/**
 * This is just a different class so that the classes can be checked for being a trap instead of
 * just a feature.
 */
public abstract class Trap implements TerrainFeature {

    abstract void trigger(Level level, int pos, Char ch);

    @Override
    public void enter(Level level, int pos, Char ch) {
        trigger(level, pos, ch);
    }
}
