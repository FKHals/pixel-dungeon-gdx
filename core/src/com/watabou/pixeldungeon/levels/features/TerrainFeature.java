package com.watabou.pixeldungeon.levels.features;

import com.watabou.pixeldungeon.actors.Char;
import com.watabou.pixeldungeon.levels.Level;

/**
 * An Interface that is used to define behaviours of Terrain features (e. g. what happens on
 * entering/moving onto a tile)
 */
public interface TerrainFeature {
    /**
     * @param level The Level which contains the entered cell
     * @param cell The cell that's entered
     * @param ch The character that does the entering
     */
    void enter(Level level, int cell, Char ch );
}
