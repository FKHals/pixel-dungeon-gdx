package com.watabou.pixeldungeon.levels.features;

import com.watabou.pixeldungeon.actors.Char;
import com.watabou.pixeldungeon.levels.Level;

/**
 * A TerrainFeature that does nothing to avoid using null for an empty TerrainFeature
 */
public class None implements TerrainFeature {
    @Override
    public void enter(Level level, int cell, Char ch) {
        // do nothing
    }
}
