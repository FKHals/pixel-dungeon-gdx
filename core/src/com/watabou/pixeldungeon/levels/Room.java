/*
 * Pixel Dungeon
 * Copyright (C) 2012-2014  Oleg Dolya
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.watabou.pixeldungeon.levels;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;

import com.watabou.pixeldungeon.DungeonTilemap;
import com.watabou.pixeldungeon.levels.painters.*;
import com.watabou.utils.*;
import com.watabou.utils.Optional;
import com.watabou.utils.Random;


public class Room extends Rect implements Graph.Node, Bundlable {

	// this must be ordered data structures (instead of just a HashSet and HashMap) so that randomized operations are
	// deterministic according to the seed which is necessary for implementing seeded dungeons
	public LinkedHashSet<Room> neigbours = new LinkedHashSet<>();
	public LinkedHashMap<Room, Door> connected = new LinkedHashMap<>();
	
	public int distance;
	public int price = 1;
	
	public enum Type {
		NULL		(new Painter() {
			@Override
			public void paint(Level level, Room room, Level.GenerationConstraints constraints) {
				// do nothing (filling the room with walls instead would make the CHASM feeling
				// impossible)
			}

			@Override
			public void addActors(Level level, Room room, Level.GenerationConstraints constraints) {
				// do nothing
			}

			@Override
			public void addItems(Level level, Room room, Level.GenerationConstraints constraints) {
				// do nothing
			}
		}),
		STANDARD	(new StandardPainter()),
		ENTRANCE	(new EntrancePainter()),
		EXIT		(new ExitPainter()),
		BOSS_EXIT	(new BossExitPainter()),
		TUNNEL		(new TunnelPainter()),
		PASSAGE		(new PassagePainter()),
		SHOP		(new ShopPainter()) {
			@Override
			public boolean areRequirementsMetBy(Room room, RegularLevel level) {
				boolean isConnectedToEntrance = level.getEntranceRoom().connected.containsKey(room);
				return isConnectedToEntrance && room.numberOfConnectedRooms() == 1
						&& room.width() >= 5 && room.height() >= 5;
			}
		},
		BLACKSMITH	(new BlacksmithPainter()) {
			@Override
			public boolean areRequirementsMetBy(Room room, RegularLevel level) {
				return room.numberOfConnectedRooms() >= 1 && room.width() > 4 && room.height() > 4;
			}
		},
		TREASURY	(new TreasuryPainter()),
		ARMORY		(new ArmoryPainter()),
		LIBRARY		(new LibraryPainter()),
		LABORATORY	(new LaboratoryPainter()),
		VAULT		(new VaultPainter()),
		TRAPS		(new TrapsPainter()),
		STORAGE		(new StoragePainter()),
		MAGIC_WELL	(new MagicWellPainter()),
		GARDEN		(new GardenPainter()),
		CRYPT		(new CryptPainter()),
		STATUE		(new StatuePainter()),
		POOL		(new PoolPainter()),
		RAT_KING	(new RatKingPainter()),
		WEAK_FLOOR	(new WeakFloorPainter()),
		PIT			(new PitPainter()),
		ALTAR		(new AltarPainter());

		private final Painter painter;

		private Type( Painter painter ) {
			this.painter = painter;
		}

		public void paint( Level level, Room room, Level.GenerationConstraints constraints ) {
			painter.paint(level, room, constraints);
		}

		public void addActors( Level level, Room room, Level.GenerationConstraints constraints ) {
			painter.addActors(level, room, constraints);
		}
		
		public void addItems( Level level, Room room, Level.GenerationConstraints constraints ) {
			painter.addItems(level, room, constraints);
		}

		/**
		 * @param room The Room that gets checked for meeting the requirements for this Type
		 * @return If the room meets the requirements
		 */
		public boolean areRequirementsMetBy(Room room, RegularLevel level) {
			return true;
		}
	};
	
	public static Type[] getSpecials() {
		return new Type[]{
				Type.ARMORY, Type.WEAK_FLOOR, Type.MAGIC_WELL, Type.CRYPT, Type.POOL, Type.GARDEN, Type.LIBRARY,
				Type.TREASURY, Type.TRAPS, Type.STORAGE, Type.STATUE, Type.LABORATORY, Type.VAULT, Type.ALTAR
		};
	}
	
	public Type type = Type.NULL;

	public int random(TwoDimensional level, Random random) {
		return random(0, level, random);
	}

	/**
	 * @param m The minimum distance to the walls/edge of the room (e.g. if set to one then it will
	 *          not be spawned right beside a wall but further to the middle of the room)
	 * @return A random cell inside the room
	 */
	public int random(int m, TwoDimensional level, Random random) {
		int x = random.nextInt( left + 1 + m, right - m );
		int y = random.nextInt( top + 1 + m, bottom - m );
		return x + y * level.width();
	}

	private ArrayList<Integer> filterRoomPositions(Predicate<Integer> predicate, TwoDimensional level) {
		ArrayList<Integer> possiblePositions = new ArrayList<>();
		for (Point p: this.getPointsInside(true)) {
			int position = p.y * level.width() + p.x;
			if (predicate.test(position)) {
				possiblePositions.add(position);
			}
		}
		return possiblePositions;
	}

	public Optional<Integer> random(Predicate<Integer> conditions, TwoDimensional level, Random random) {
		ArrayList<Integer> possiblePositions = filterRoomPositions(conditions, level);
		return Optional.ofNullable(random.nextElement(possiblePositions));
	}

	/**
	 * @return A number of distinct randomly chosen positions in the room that match certain
	 * requirements (number of returned positions <= count since there may be less distinct
	 * positions available)
	 * @param count maximum number of needed positions
	 */
	public Optional<ArrayList<Integer>> multipleDistinctRandom(
			int count,
			Predicate<Integer> conditions,
			TwoDimensional level,
			Random random) {
		ArrayList<Integer> shuffledPossiblePositions
				= random.nextShuffle(filterRoomPositions(conditions, level));
		ArrayList<Integer> randomElements = new ArrayList<>();
		for(int i = 0; i < Math.min(count, shuffledPossiblePositions.size()); i++) {
			randomElements.add(shuffledPossiblePositions.get(i));
		}
		return Optional.ofNullable(randomElements);
	}

	/**
	 * @return If the given Room shares an edge with this Room that is long enough to at least
	 * contain a door (>= 3 tiles long)
	 */
	public boolean isNeighbour( Room other ) {
		Rect i = intersect( other );
		return ((i.width() == 0 && i.height() >= 3) ||
				(i.height() == 0 && i.width() >= 3));
	}

	public void addNeighbour(Room other ) {
		if (isNeighbour(other)) {
			neigbours.add( other );
			other.neigbours.add( this );
		}
	}
	
	public void connect( Room room ) {
		if (!connected.containsKey( room )) {	
			connected.put( room, null );
			room.connected.put( this, null );			
		}
	}
	
	public Door entrance() {
		return connected.values().iterator().next();
	}
	
	public Point center(Random random) {
		return new Point( 
			(left + right) / 2 + (((right - left) & 1) == 1 ? random.nextInt( 2 ) : 0),
			(top + bottom) / 2 + (((bottom - top) & 1) == 1 ? random.nextInt( 2 ) : 0) );
	}

	/**
	 * @return The number of other rooms the room connected with (reachable by)
	 */
	public int numberOfConnectedRooms() {
		return connected.size();
	}

	/**
	 * @return The positions of a certain Terrain in the Room
	 */
	public ArrayList<Point> findTerrainPositions(Terrain terrainToFind, Level level) {
		ArrayList<Point> foundPositions = new ArrayList<>();
		for (Point cell: getPointsInside()) {
			Terrain terrain = level.getCell(cell);
			if (terrain == terrainToFind) {
				foundPositions.add(cell);
			}
		}
		return foundPositions;
	}

	/**
	 * @return The positions of a unique Terrain that appears exactly once in the Room
	 */
	public Point findUniqueTerrainPosition(Terrain uniqueTerrain, Level level) {
		Point uniqueTerrainPosition = new Point(-1, -1);
		ArrayList<Point> terrainPositions = findTerrainPositions(uniqueTerrain, level);
		if (terrainPositions.size() == 1) {
			uniqueTerrainPosition = terrainPositions.get(0);
		} else {
			DLog.error("Room", "More or less than one of the allegedly unique Terrain has been found in the room. This should never happen");
		}
		return uniqueTerrainPosition;
	}

	public boolean containsTerrain(Terrain searchedForTerrain, Level level, Room room) {
		for (Point cell: room.getPointsInside()) {
			Terrain terrain = level.getCell(cell);
			if (terrain == searchedForTerrain) {
				return true;
			}
		}
		return false;
	}
	
	// **** Graph.Node interface ****

	@Override
	public int distance() {
		return distance;
	}

	@Override
	public void distance( int value ) {
		distance = value;
	}
	
	@Override
	public int price() {
		return price;
	}

	@Override
	public void price( int value ) {
		price = value;
	}

	@Override
	public Collection<Room> edges() {
		return neigbours;
	}

	// FIXME: use proper string constants

	@Override
	public void storeInBundle( Bundle bundle ) {	
		bundle.put( "left", left );
		bundle.put( "top", top );
		bundle.put( "right", right );
		bundle.put( "bottom", bottom );
		bundle.put( "type", type.toString() );
	}
	
	@Override
	public void restoreFromBundle( Bundle bundle ) {
		left = bundle.getInt( "left" );
		top = bundle.getInt( "top" );
		right = bundle.getInt( "right" );
		bottom = bundle.getInt( "bottom" );		
		type = Type.valueOf( bundle.getString( "type" ) );
	}

	public static class Door extends Point {
		
		public static enum Type {
			EMPTY, TUNNEL, REGULAR, UNLOCKED, HIDDEN, BARRICADE, LOCKED
		}
		public Type type = Type.EMPTY;
		
		public Door( int x, int y ) {
			super( x, y );
		}
		
		public void set( Type type ) {
			if (type.compareTo( this.type ) > 0) {
				this.type = type;
			}
		}
	}

	public interface Painter {
		/**
		 * Fills a Level's Room with certain Terrain and features
		 */
		void paint(Level level, Room room, Level.GenerationConstraints constraints);

		/**
		 * Fills a Level's Room with certain Mobs
		 */
		void addActors(Level level, Room room, Level.GenerationConstraints constraints);

		/**
		 * Fills a Level's Room with certain Items
		 */
		void addItems(Level level, Room room, Level.GenerationConstraints constraints);
	}
}














