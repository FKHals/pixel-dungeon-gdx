/*
 * Pixel Dungeon
 * Copyright (C) 2012-2014  Oleg Dolya
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.watabou.pixeldungeon.levels.painters;

import com.watabou.utils.Field;
import com.watabou.utils.Point;
import com.watabou.utils.Rect;

public abstract class Painter {

	public static <PropertyType> void set( Field<PropertyType> field, int cell, PropertyType value ) {
		field.setCell(new Point(cell % field.width(), cell / field.width()), value);
	}

	public static <PropertyType> void set( Field<PropertyType> field, int x, int y, PropertyType value ) {
		set( field, x + y * field.width(), value );
	}

	public static <PropertyType> void set( Field<PropertyType> field, Point p, PropertyType value ) {
		field.setCell( p, value );
	}

	public static <PropertyType> void fill( Field<PropertyType> field, int x, int y, int w, int h, PropertyType value ) {
		fill(field, new Rect(x, y, x + w - 1, y + h - 1), value);
	}

	public static <PropertyType> void fill( Field<PropertyType> field, Rect rect, PropertyType value ) {
		// fill only the intersection of the Rect with the level to avoid drawing outside the level
		Rect intersection = rect.intersect(new Rect(field));
		for (int x = intersection.left; x <= intersection.right; x++) {
			for (int y = intersection.top; y <= intersection.bottom; y++) {
				field.setCell(new Point(x, y), value);
			}
		}
	}

	/**
	 * @param rect The shrank Rect to be filled
	 * @param shrinkSubtrahend The value that the width and height should be shortened by
	 */
	public static <PropertyType> void fill( Field<PropertyType> field, Rect rect, int shrinkSubtrahend, PropertyType value ) {
		fill(field, rect.shrink(shrinkSubtrahend), value);
	}
	
	public static <PropertyType> Point drawInside( Field<PropertyType> field, Rect room, Point from, int n, PropertyType value ) {
		
		Point step = new Point();
		if (from.x == room.left) {
			step.set( +1, 0 );
		} else if (from.x == room.right) {
			step.set( -1, 0 );
		} else if (from.y == room.top) {
			step.set( 0, +1 );
		} else if (from.y == room.bottom) {
			step.set( 0, -1 );
		}
		
		Point p = new Point( from ).offset( step );
		for (int i=0; i < n; i++) {
			set( field, p, value );
			p.offset( step );
		}
		
		return p;
	}
}
