/*
 * Pixel Dungeon
 * Copyright (C) 2012-2014  Oleg Dolya
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.watabou.pixeldungeon.levels.painters;

import com.watabou.pixeldungeon.actors.mobs.npcs.RatKing;
import com.watabou.pixeldungeon.items.Generator;
import com.watabou.pixeldungeon.items.Gold;
import com.watabou.pixeldungeon.items.Heap;
import com.watabou.pixeldungeon.items.Item;
import com.watabou.pixeldungeon.items.weapon.missiles.MissileWeapon;
import com.watabou.pixeldungeon.levels.Level;
import com.watabou.pixeldungeon.levels.Room;
import com.watabou.pixeldungeon.levels.Terrain;
import com.watabou.utils.DLog;
import com.watabou.utils.Optional;
import com.watabou.utils.Predicate;
import com.watabou.utils.Random;

/**
 * A Painter that creates a hidden Room that contains the ratking and a lot of (low quality
 * treasure) chests.
 */
public class RatKingPainter implements Room.Painter {

	@Override
	public void paint( Level level, Room room, Level.GenerationConstraints constraints ) {

		Painter.fill( level, room, Terrain.WALL );
		Painter.fill( level, room, 1, Terrain.EMPTY_SP );

		Room.Door entrance = room.entrance();
		entrance.set( Room.Door.Type.HIDDEN );
	}

	@Override
	public void addActors(Level level, Room room, Level.GenerationConstraints constraints) {
		RatKing king = new RatKing();
		king.pos = room.random( 1, level, new Random());
		level.addMob( king );
	}

	@Override
	public void addItems(final Level level, Room room, Level.GenerationConstraints constraints) {
		Room.Door entrance = room.entrance();
		int door = entrance.x + entrance.y * level.width();

		for (int i=room.left + 1; i < room.right; i++) {
			addChest( level, (room.top + 1) * level.width() + i, door );
			addChest( level, (room.bottom - 1) * level.width() + i, door );
		}

		for (int i=room.top + 2; i < room.bottom - 1; i++) {
			addChest( level, i * level.width() + room.left + 1, door );
			addChest( level, i * level.width() + room.right - 1, door );
		}

		// replace one of the chests in the room with a Mimic
		Optional<Integer> pos = room.random(new Predicate<Integer>() {
			@Override
			public boolean test(Integer pos) {
				return level.heaps.get( pos ) != null;
			}
		}, level, new Random());
		if (pos.isPresent()) {
			Heap chest = level.heaps.get( pos.get() );
			chest.type = Heap.Type.MIMIC;
		} else {
			DLog.error("RatKingPainter", "Could not place mimic into " + room.type);
			// TODO Throw exception and regenerate level?!
		}
	}

	private static void addChest( Level level, int pos, int door ) {
		
		if (pos == door - 1 || 
			pos == door + 1 || 
			pos == door - level.width() ||
			pos == door + level.width()) {
			return;
		}
		
		Item prize;
		switch (Random.Int( 10 )) {
		case 0:
			prize = Generator.random( Generator.Category.WEAPON );
			if (prize instanceof MissileWeapon) {
				prize.quantity( 1 );
			} else {
				prize.degrade( Random.Int( 3 ) );
			}
			break;
		case 1:
			prize = Generator.random( Generator.Category.ARMOR ).degrade( Random.Int( 3 ) );
			break;
		default:
			prize = new Gold( Random.IntRange( 1, 5 ) );
			break;
		}
		
		level.drop( prize, pos ).type = Heap.Type.CHEST;
	}
}
