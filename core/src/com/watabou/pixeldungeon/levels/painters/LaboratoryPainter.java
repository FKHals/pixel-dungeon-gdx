/*
 * Pixel Dungeon
 * Copyright (C) 2012-2014  Oleg Dolya
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.watabou.pixeldungeon.levels.painters;

import com.watabou.pixeldungeon.actors.blobs.Alchemy;
import com.watabou.pixeldungeon.items.Generator;
import com.watabou.pixeldungeon.items.Item;
import com.watabou.pixeldungeon.items.keys.IronKey;
import com.watabou.pixeldungeon.items.potions.Potion;
import com.watabou.pixeldungeon.levels.Level;
import com.watabou.pixeldungeon.levels.Room;
import com.watabou.pixeldungeon.levels.Terrain;
import com.watabou.utils.*;

import java.util.ArrayList;

/**
 * A Painter that creates a locked Room that contains a couple of potions and an alchemy pot.
 */
public class LaboratoryPainter implements Room.Painter {

	@Override
	public void paint( Level level, Room room, Level.GenerationConstraints constraints ) {

		Painter.fill( level, room, Terrain.WALL );
		Painter.fill( level, room, 1, Terrain.EMPTY_SP );
		
		Room.Door entrance = room.entrance();
		
		Point pot = null;
		if (entrance.x == room.left) {
			pot = new Point( room.right-1, constraints.random.nextInt( 2 ) == 0 ? room.top + 1 : room.bottom - 1 );
		} else if (entrance.x == room.right) {
			pot = new Point( room.left+1, constraints.random.nextInt( 2 ) == 0 ? room.top + 1 : room.bottom - 1 );
		} else if (entrance.y == room.top) {
			pot = new Point( constraints.random.nextInt( 2 ) == 0 ? room.left + 1 : room.right - 1, room.bottom-1 );
		} else if (entrance.y == room.bottom) {
			pot = new Point( constraints.random.nextInt( 2 ) == 0 ? room.left + 1 : room.right - 1, room.top+1 );
		}
		Painter.set( level, pot, Terrain.ALCHEMY );
		
		entrance.set( Room.Door.Type.LOCKED );
	}

	@Override
	public void addActors(Level level, Room room, Level.GenerationConstraints constraints) {
		Point position = room.findUniqueTerrainPosition(Terrain.ALCHEMY, level);
		if (!position.equals(new Point(-1, -1))) {
			Alchemy alchemy = new Alchemy();
			alchemy.seed( level.toCell(position), 1 );
			level.blobs.put( Alchemy.class, alchemy );
		}
	}

	@Override
	public void addItems(final Level level, Room room, Level.GenerationConstraints constraints) {
		final int numberOfItems = Random.IntRange( 2, 3 );
		ArrayList<Item> itemsToDrop = new ArrayList<>();
		for (int i=0; i < numberOfItems; i++) {
			itemsToDrop.add( prize(level) );
		}
		level.dropMultipleInRoom(itemsToDrop, room, new Random(), new Predicate<Integer>() {
			@Override
			public boolean test(Integer pos) {
				return (level.getCell(pos) == Terrain.EMPTY_SP && level.heaps.get( pos ) == null);
			}
		});

		level.addItemToSpawn( new IronKey() );
	}

	private static Item prize( Level level ) {
		
		Item prize = level.itemToSpanAsPrize();
		if (prize instanceof Potion) {
			return prize;
		} else if (prize != null) {
			level.addItemToSpawn( prize );
		}
		
		return Generator.random( Generator.Category.POTION );
	}
}
