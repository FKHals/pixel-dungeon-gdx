/*
 * Pixel Dungeon
 * Copyright (C) 2012-2014  Oleg Dolya
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.watabou.pixeldungeon.levels.painters;

import com.watabou.pixeldungeon.items.Generator;
import com.watabou.pixeldungeon.items.Item;
import com.watabou.pixeldungeon.items.Heap.Type;
import com.watabou.pixeldungeon.items.keys.GoldenKey;
import com.watabou.pixeldungeon.items.keys.IronKey;
import com.watabou.pixeldungeon.levels.Level;
import com.watabou.pixeldungeon.levels.Room;
import com.watabou.pixeldungeon.levels.Terrain;
import com.watabou.utils.Random;

/**
 * A Painter that creates a locked Room that contains a locked treasure chests or a pedestal with
 * treasure laying on it.
 */
public class VaultPainter implements Room.Painter {

	@Override
	public void paint( Level level, Room room, Level.GenerationConstraints constraints ) {

		Painter.fill( level, room, Terrain.WALL );
		Painter.fill( level, room, 1, Terrain.EMPTY_SP );
		Painter.fill( level, room, 2, Terrain.EMPTY );
		
		room.entrance().set( Room.Door.Type.LOCKED );
	}

	@Override
	public void addActors(Level level, Room room, Level.GenerationConstraints constraints) {
		
	}

	@Override
	public void addItems(Level level, Room room, Level.GenerationConstraints constraints) {
		int cx = (room.left + room.right) / 2;
		int cy = (room.top + room.bottom) / 2;
		int c = cx + cy * level.width();

		switch (Random.Int( 3 )) {

			case 0:
				level.drop( prize( level ), c ).type = Type.LOCKED_CHEST;
				level.addItemToSpawn( new GoldenKey() );
				break;

			case 1:
				Item i1, i2;
				do {
					i1 = prize( level );
					i2 = prize( level );
				} while (i1.getClass() == i2.getClass());
				level.drop( i1, c ).type = Type.CRYSTAL_CHEST;
				level.drop( i2, c + Level.NEIGHBOURS8[Random.Int( 8 )]).type = Type.CRYSTAL_CHEST;
				level.addItemToSpawn( new GoldenKey() );
				break;

			case 2:
				level.drop( prize( level ), c );
				//TODO This actually belongs to #paint() but that would make it more complicated
				Painter.set( level, c, Terrain.PEDESTAL );
				break;
		}

		level.addItemToSpawn( new IronKey() );
	}

	private static Item prize( Level level ) {
		return Generator.random( Random.oneOf(  
			Generator.Category.WAND, 
			Generator.Category.RING 
		) );
	}
}
