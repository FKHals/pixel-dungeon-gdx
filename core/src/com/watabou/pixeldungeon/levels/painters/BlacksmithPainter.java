/*
 * Pixel Dungeon
 * Copyright (C) 2012-2014  Oleg Dolya
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.watabou.pixeldungeon.levels.painters;

import com.watabou.pixeldungeon.items.Generator;
import com.watabou.pixeldungeon.items.Item;
import com.watabou.pixeldungeon.levels.Level;
import com.watabou.pixeldungeon.levels.Room;
import com.watabou.pixeldungeon.levels.Terrain;
import com.watabou.utils.DLog;
import com.watabou.utils.Optional;
import com.watabou.utils.Predicate;
import com.watabou.utils.Random;

import java.util.ArrayList;

/**
 * A Painter that creates a Room designed like a forge for the Blacksmith-Quests.
 */
public class BlacksmithPainter implements Room.Painter {

	@Override
	public void paint( Level level, Room room, Level.GenerationConstraints constraints ) {

		Painter.fill( level, room, Terrain.WALL );
		Painter.fill( level, room, 1, Terrain.FIRE_TRAP );
		Painter.fill( level, room, 2, Terrain.EMPTY_SP );
		
		for (Room.Door door : room.connected.values()) {
			door.set( Room.Door.Type.UNLOCKED );
			Painter.drawInside( level, room, door, 1, Terrain.EMPTY );
		}
	}

	@Override
	public void addActors(Level level, Room room, Level.GenerationConstraints constraints) {
		
	}

	@Override
	public void addItems(final Level level, Room room, Level.GenerationConstraints constraints) {
		final int numberOfItems = 2;
		ArrayList<Item> itemsToDrop = new ArrayList<>();
		for (int i=0; i < numberOfItems; i++) {
			itemsToDrop.add( Generator.random( Random.oneOf(
					Generator.Category.ARMOR,
					Generator.Category.WEAPON
			) ) );
		}
		level.dropMultipleInRoom(itemsToDrop, room, new Random(), new Predicate<Integer>() {
			@Override
			public boolean test(Integer pos) {
				return (level.getCell(pos) == Terrain.EMPTY_SP);
			}
		});
	}
}
