/*
 * Pixel Dungeon
 * Copyright (C) 2012-2014  Oleg Dolya
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.watabou.pixeldungeon.levels.painters;

import com.watabou.pixeldungeon.actors.blobs.WaterOfAwareness;
import com.watabou.pixeldungeon.actors.blobs.WaterOfHealth;
import com.watabou.pixeldungeon.actors.blobs.WaterOfTransmutation;
import com.watabou.pixeldungeon.actors.blobs.WellWater;
import com.watabou.pixeldungeon.levels.Level;
import com.watabou.pixeldungeon.levels.Room;
import com.watabou.pixeldungeon.levels.Terrain;
import com.watabou.utils.Point;
import com.watabou.utils.Random;

/**
 * A Painter that creates a Room that contains a magic well (awareness, health or transmutation).
 */
public class MagicWellPainter implements Room.Painter {

	@Override
	public void paint( Level level, Room room, Level.GenerationConstraints constraints ) {

		Painter.fill( level, room, Terrain.WALL );
		Painter.fill( level, room, 1, Terrain.EMPTY );

		Point c = room.center(constraints.random);
		Painter.set( level, c.x, c.y, Terrain.WELL );

		room.entrance().set( Room.Door.Type.REGULAR );
	}

	@Override
	public void addActors(Level level, Room room, Level.GenerationConstraints constraints) {
		final WellWater[] waters = {new WaterOfAwareness(), new WaterOfHealth(), new WaterOfTransmutation()};

		WellWater currentWater = Random.element(waters);
		Class<? extends WellWater> waterClass = currentWater.getClass();

		WellWater water = (WellWater)level.blobs.get( waterClass );
		if (water == null) {
			water = currentWater;
		}
		
		Point position = room.findUniqueTerrainPosition(Terrain.WELL, level);
		if (!position.equals(new Point(-1, -1))) {
			water.seed(level.toCell(position), 1);
			level.blobs.put(waterClass, water);
		}
	}

	@Override
	public void addItems(Level level, Room room, Level.GenerationConstraints constraints) {

	}
}