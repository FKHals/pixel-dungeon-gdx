/*
 * Pixel Dungeon
 * Copyright (C) 2012-2014  Oleg Dolya
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.watabou.pixeldungeon.levels.painters;

;
import com.watabou.pixeldungeon.items.Generator;
import com.watabou.pixeldungeon.items.Heap;
import com.watabou.pixeldungeon.items.Item;
import com.watabou.pixeldungeon.items.potions.PotionOfLevitation;
import com.watabou.pixeldungeon.levels.Level;
import com.watabou.pixeldungeon.levels.Room;
import com.watabou.pixeldungeon.levels.Terrain;
import com.watabou.utils.Point;
import com.watabou.utils.Random;

/**
 * A Painter that creates a Room that contains a field of visible traps and a pedestal behind it
 * with an armor or a weapon on it.
 */
public class TrapsPainter implements Room.Painter {

	@Override
	public void paint( Level level, Room room, Level.GenerationConstraints constraints ) {
		 
		Terrain[] traps = {
			Terrain.TOXIC_TRAP, Terrain.TOXIC_TRAP, Terrain.TOXIC_TRAP, 
			Terrain.PARALYTIC_TRAP, Terrain.PARALYTIC_TRAP,
			!constraints.isNextLevelBossLevel ? Terrain.CHASM : Terrain.SUMMONING_TRAP };
		Painter.fill( level, room, Terrain.WALL );
		Painter.fill( level, room, 1, constraints.random.nextOneOf( traps ) );
		
		Room.Door door = room.entrance(); 
		door.set( Room.Door.Type.REGULAR );
		
		Terrain lastRow = level.getCell(room.left + 1 + (room.top + 1) * level.width()) == Terrain.CHASM ? Terrain.CHASM : Terrain.EMPTY;

		int x = -1;
		int y = -1;
		if (door.x == room.left) {
			x = room.right - 1;
			y = room.top + room.height() / 2;
			Painter.fill( level, x, room.top + 1, 1, room.height() - 1 , lastRow );
		} else if (door.x == room.right) {
			x = room.left + 1;
			y = room.top + room.height() / 2;
			Painter.fill( level, x, room.top + 1, 1, room.height() - 1 , lastRow );
		} else if (door.y == room.top) {
			x = room.left + room.width() / 2;
			y = room.bottom - 1;
			Painter.fill( level, room.left + 1, y, room.width() - 1, 1 , lastRow );
		} else if (door.y == room.bottom) {
			x = room.left + room.width() / 2;
			y = room.top + 1;
			Painter.fill( level, room.left + 1, y, room.width() - 1, 1 , lastRow );
		}
		
		int pos = x + y * level.width();
		if (constraints.random.nextInt( 3 ) == 0) {
			if (lastRow == Terrain.CHASM) {
				Painter.set( level, pos, Terrain.EMPTY );
			}
		} else {
			Painter.set( level, pos, Terrain.PEDESTAL );
		}
		
	}

	@Override
	public void addActors(Level level, Room room, Level.GenerationConstraints constraints) {
		
	}

	@Override
	public void addItems(Level level, Room room, Level.GenerationConstraints constraints) {
		Point pedestalPosition = room.findUniqueTerrainPosition(Terrain.PEDESTAL, level);
		boolean isPedestalFound = !pedestalPosition.equals(new Point(-1, -1));
		if (isPedestalFound) {
			level.drop( prize( level ), level.toCell(pedestalPosition) );
		} else {
			Room.Door door = room.entrance();

			Point centerOfFurthestRow = new Point();
			if (door.x == room.left) {
				centerOfFurthestRow.x = room.right - 1;
				centerOfFurthestRow.y = room.top + room.height() / 2;
			} else if (door.x == room.right) {
				centerOfFurthestRow.x = room.left + 1;
				centerOfFurthestRow.y = room.top + room.height() / 2;
			} else if (door.y == room.top) {
				centerOfFurthestRow.x = room.left + room.width() / 2;
				centerOfFurthestRow.y = room.bottom - 1;
			} else if (door.y == room.bottom) {
				centerOfFurthestRow.x = room.left + room.width() / 2;
				centerOfFurthestRow.y = room.top + 1;
			}

			level.drop( prize( level ), level.toCell(centerOfFurthestRow) ).type = Heap.Type.CHEST;
		}

		level.addItemToSpawn( new PotionOfLevitation() );
	}

	private static Item prize( Level level ) {
		
		Item prize = level.itemToSpanAsPrize();
		if (prize != null) {
			return prize;
		}
		
		prize = Generator.random( Random.oneOf(  
			Generator.Category.WEAPON, 
			Generator.Category.ARMOR 
		) );

		for (int i=0; i < 3; i++) {
			Item another = Generator.random( Random.oneOf(  
				Generator.Category.WEAPON, 
				Generator.Category.ARMOR 
			) );
			if (another.level() > prize.level()) {
				prize = another;
			}
		}
		
		return prize;
	}
}
