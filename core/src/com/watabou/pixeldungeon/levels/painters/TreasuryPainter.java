/*
 * Pixel Dungeon
 * Copyright (C) 2012-2014  Oleg Dolya
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.watabou.pixeldungeon.levels.painters;

import com.watabou.pixeldungeon.items.Gold;
import com.watabou.pixeldungeon.items.Heap;
import com.watabou.pixeldungeon.items.Item;
import com.watabou.pixeldungeon.items.keys.IronKey;
import com.watabou.pixeldungeon.levels.Level;
import com.watabou.pixeldungeon.levels.Room;
import com.watabou.pixeldungeon.levels.Terrain;
import com.watabou.utils.Predicate;
import com.watabou.utils.Random;

import java.util.ArrayList;

/**
 * A Painter that creates a locked Room that contains a treasure chests and may contain a mimic.
 */
public class TreasuryPainter implements Room.Painter {

	@Override
	public void paint( Level level, Room room, Level.GenerationConstraints constraints ) {

		Painter.fill( level, room, Terrain.WALL );
		Painter.fill( level, room, 1, Terrain.EMPTY );
		
		Painter.set( level, room.center(constraints.random), Terrain.STATUE );
		
		room.entrance().set( Room.Door.Type.LOCKED );
	}

	@Override
	public void addActors(Level level, Room room, Level.GenerationConstraints constraints) {
		
	}

	@Override
	public void addItems(final Level level, Room room, Level.GenerationConstraints constraints) {
		Heap.Type heapType = Random.Int( 2 ) == 0 ? Heap.Type.CHEST : Heap.Type.HEAP;

		final int numberOfItems = Random.IntRange( 2, 3 );
		ArrayList<Item> itemsToDrop = new ArrayList<>();
		ArrayList<Heap.Type> heapTypesToDrop = new ArrayList<>();
		for (int i=0; i < numberOfItems; i++) {
			itemsToDrop.add( new Gold().random() );
			heapTypesToDrop.add(i == 0 && heapType == Heap.Type.CHEST ? Heap.Type.MIMIC : heapType);
		}
		level.dropMultipleInRoom(itemsToDrop, heapTypesToDrop, room, new Random(), new Predicate<Integer>() {
			@Override
			public boolean test(Integer pos) {
				return (level.getCell(pos) == Terrain.EMPTY && level.heaps.get( pos ) == null);
			}
		});

		if (heapType == Heap.Type.HEAP) {
			final int numberOfHeaps = 6;
			ArrayList<Item> heapsToDrop = new ArrayList<>();
			for (int i=0; i < numberOfHeaps; i++) {
				heapsToDrop.add( new Gold( Random.IntRange( 1, 3 ) ) );
			}
			level.dropMultipleInRoom(heapsToDrop, room, new Random(), new Predicate<Integer>() {
				@Override
				public boolean test(Integer pos) {
					return (level.getCell(pos) == Terrain.EMPTY);
				}
			});
		}

		level.addItemToSpawn( new IronKey() );
	}
}
