/*
 * Pixel Dungeon
 * Copyright (C) 2012-2014  Oleg Dolya
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.watabou.pixeldungeon.levels.painters;

import com.watabou.pixeldungeon.levels.Level;
import com.watabou.pixeldungeon.levels.Room;
import com.watabou.pixeldungeon.levels.Terrain;
import com.watabou.utils.Point;
import com.watabou.utils.Random;

/**
 * A Painter that creates a (1-tile-width) tunnel parallel to the longer edges of the room that
 * connects adjacent rooms.
 */
public class TunnelPainter implements Room.Painter {

	//TODO This could need some abstraction and resulting deduplication since both cases (tall and
	// wide room) are symmetric and should not have be treated differently (just rotated 90°)
	/**
	 * Create center (1-tile-width) tunnel connects the narrow ends of the Room-rectangle (if the
	 * room is quadratic and has no narrow ends a pair of opposing side gets picked randomly).
	 * Every Door that connects to an adjacent room on the room sides parallel to the center path
	 * gets orthogonally connected with a straight path from the door to the center tunnel.
	 * Doors on one of the other two (narrow) sides gets connected by extending the center path
	 * in its direction until an orthogonal path (relative to the center path) can reach it which
	 * then gets drawn and thereby connects the door to the center path.
	 *
	 * So the resulting tunnel this painter would draw would look like this:
	 * ('#': wall, '_': empty, 'D': door)
	 * <pre>
	 * {@code
	 *    0  1  2  3  4
	 *  0 #  #  #  #  #
	 *  1 D  _  _  #  #
	 *  2 #  #  _  _  D
	 *  3 #  #  _  #  #
	 *  4 #  #  _  #  #
	 *  5 #  _  _  #  #
	 *  6 #  D  #  #  #
	 * }
	 * </pre>
	 *
	 * @param level The Level that the Tunnel-Room gets painted into
	 * @param room The Room rectangle that the tunnel gets painted into
	 */
	@Override
	public void paint( Level level, Room room, Level.GenerationConstraints constraints ) {
		
		Terrain floor = level.tunnelTile();
		
		Point c = room.center(constraints.random);

		// if the room is wider than tall
		if (room.width() > room.height() || (room.width() == room.height() && constraints.random.nextInt( 2 ) == 0)) {
			
			int from = room.right - 1;
			int to = room.left + 1;
			
			for (Room.Door door : room.connected.values()) {
				
				int step = door.y < c.y ? +1 : -1;

				// draw path from left door to center path
				if (door.x == room.left) {
					
					from = room.left + 1;
					for (int i=door.y; i != c.y; i += step) {
						Painter.set( level, from, i, floor );
					}

				// draw path from right door to center path
				} else if (door.x == room.right) {
					
					to = room.right - 1;
					for (int i=door.y; i != c.y; i += step) {
						Painter.set( level, to, i, floor );
					}

				// extend the length of the center path so that it can reach the door a the narrow
				// side and draw a path from the door orthogonally to the center path
				} else {
					if (door.x < from) {
						from = door.x;
					}
					if (door.x > to) {
						to = door.x;
					}
					
					for (int i=door.y+step; i != c.y; i += step) {
						Painter.set( level, door.x, i, floor );
					}
				}
			}

			// draw center path from right to left
			for (int i=from; i <= to; i++) {
				Painter.set( level, i, c.y, floor );
			}

		// if the room is taller than wide
		} else {
			
			int from = room.bottom - 1;
			int to = room.top + 1;
			
			for (Room.Door door : room.connected.values()) {
				
				int step = door.x < c.x ? +1 : -1;

				// draw path from top door to center path
				if (door.y == room.top) {
					
					from = room.top + 1;
					for (int i=door.x; i != c.x; i += step) {
						Painter.set( level, i, from, floor );
					}

				// draw path from bottom door to center path
				} else if (door.y == room.bottom) {
					
					to = room.bottom - 1;
					for (int i=door.x; i != c.x; i += step) {
						Painter.set( level, i, to, floor );
					}

				// extend the length of the center path so that it can reach the door a the narrow
				// side and draw a path from the door orthogonally to the center path
				} else {
					if (door.y < from) {
						from = door.y;
					}
					if (door.y > to) {
						to = door.y;
					}
					
					for (int i=door.x+step; i != c.x; i += step) {
						Painter.set( level, i, door.y, floor );
					}
				}
			}

			// draw center path from bottom to top
			for (int i=from; i <= to; i++) {
				Painter.set( level, c.x, i, floor );
			}
		}
		
		for (Room.Door door : room.connected.values()) {
			door.set( Room.Door.Type.TUNNEL );
		}
	}

	@Override
	public void addActors(Level level, Room room, Level.GenerationConstraints constraints) {
		
	}

	@Override
	public void addItems(Level level, Room room, Level.GenerationConstraints constraints) {

	}
}
