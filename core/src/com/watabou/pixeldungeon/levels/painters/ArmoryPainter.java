/*
 * Pixel Dungeon
 * Copyright (C) 2012-2014  Oleg Dolya
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.watabou.pixeldungeon.levels.painters;

import com.watabou.pixeldungeon.items.Bomb;
import com.watabou.pixeldungeon.items.Generator;
import com.watabou.pixeldungeon.items.Heap;
import com.watabou.pixeldungeon.items.Item;
import com.watabou.pixeldungeon.items.keys.IronKey;
import com.watabou.pixeldungeon.levels.Level;
import com.watabou.pixeldungeon.levels.Room;
import com.watabou.pixeldungeon.levels.Terrain;
import com.watabou.utils.*;

import java.util.ArrayList;

/**
 * A Painter that creates a locked Room that contains armors and/or weapons as loot.
 */
public class ArmoryPainter implements Room.Painter {

	@Override
	public void paint( Level level, Room room, Level.GenerationConstraints constraints ) {

		Painter.fill( level, room, Terrain.WALL );
		Painter.fill( level, room, 1, Terrain.EMPTY );
		
		Room.Door entrance = room.entrance();
		Point statue = null;
		if (entrance.x == room.left) {
			statue = new Point( room.right-1, constraints.random.nextInt( 2 ) == 0 ? room.top+1 : room.bottom-1 );
		} else if (entrance.x == room.right) {
			statue = new Point( room.left+1, constraints.random.nextInt( 2 ) == 0 ? room.top+1 : room.bottom-1 );
		} else if (entrance.y == room.top) {
			statue = new Point( constraints.random.nextInt( 2 ) == 0 ? room.left+1 : room.right-1, room.bottom-1 );
		} else if (entrance.y == room.bottom) {
			statue = new Point( constraints.random.nextInt( 2 ) == 0 ? room.left+1 : room.right-1, room.top+1 );
		}
		if (statue != null) {
			Painter.set( level, statue, Terrain.STATUE );
		}
		
		entrance.set( Room.Door.Type.LOCKED );
	}

	@Override
	public void addActors(Level level, Room room, Level.GenerationConstraints constraints) {

	}

	@Override
	public void addItems(final Level level, Room room, Level.GenerationConstraints constraints) {
		int numberOfItems = 3 + (Random.Int( 4 ) == 0 ? 1 : 0);
		ArrayList<Item> itemsToDrop = new ArrayList<>();
		for (int i=0; i < numberOfItems; i++) {
			itemsToDrop.add( prize(level) );
		}
		level.dropMultipleInRoom(itemsToDrop, room, new Random(), new Predicate<Integer>() {
			@Override
			public boolean test(Integer pos) {
				return (level.getCell(pos) == Terrain.EMPTY && level.heaps.get( pos ) == null);
			}
		});

		level.addItemToSpawn( new IronKey() );
	}

	private static Item prize( Level level ) {
		return Random.Int( 6 ) == 0 ?
				new Bomb().random() :
				Generator.random( Random.oneOf(
						Generator.Category.ARMOR,
						Generator.Category.WEAPON
		) );
	}
}
