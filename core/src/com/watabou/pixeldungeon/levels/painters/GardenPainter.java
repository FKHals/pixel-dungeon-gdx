/*
 * Pixel Dungeon
 * Copyright (C) 2012-2014  Oleg Dolya
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.watabou.pixeldungeon.levels.painters;

import com.watabou.pixeldungeon.actors.blobs.Foliage;
import com.watabou.pixeldungeon.items.Honeypot;
import com.watabou.pixeldungeon.levels.Level;
import com.watabou.pixeldungeon.levels.Room;
import com.watabou.pixeldungeon.levels.Terrain;
import com.watabou.pixeldungeon.plants.Plant;
import com.watabou.pixeldungeon.plants.Sungrass;
import com.watabou.utils.Point;
import com.watabou.utils.Random;

/**
 * A Painter that creates a Room that is covered with grass, has low visibility and may contain a
 * honeypot.
 */
public class GardenPainter implements Room.Painter {

	@Override
	public void paint( Level level, Room room, Level.GenerationConstraints constraints ) {

		Painter.fill( level, room, Terrain.WALL );
		Painter.fill( level, room, 1, Terrain.HIGH_GRASS );
		Painter.fill( level, room, 2, Terrain.GRASS );

		room.entrance().set( Room.Door.Type.REGULAR );
	}

	@Override
	public void addActors(Level level, Room room, Level.GenerationConstraints constraints) {
		if (Random.Int( 2 ) != 0) {
			int bushes = (Random.Int( 5 ) == 0 ? 2 : 1);
			for (int i=0; i < bushes; i++) {
				int pos = room.random(level, new Random());
				//TODO This actually belongs to #paint() but that would make it more complicated
				Painter.set( level, pos, Terrain.GRASS );
				level.plant( new Sungrass.Seed(), pos );
			}
		}

		Foliage light = (Foliage)level.blobs.get( Foliage.class );
		if (light == null) {
			light = new Foliage();
		}
		for (int i=room.top + 1; i < room.bottom; i++) {
			for (int j=room.left + 1; j < room.right; j++) {
				light.seed( level.toCell(new Point(j, i)), 1 );
			}
		}
		level.blobs.put( Foliage.class, light );
	}

	@Override
	public void addItems(Level level, Room room, Level.GenerationConstraints constraints) {
		if (roomContainsSungrass(level, room)) {
			level.drop(new Honeypot(), room.random(level, new Random()));
		}
	}
	
	private boolean roomContainsSungrass(Level level, Room room) {
		for (Point cell: room.getPointsInside()) {
			Plant plant = level.getPlant(level.toCell(cell));
			if (plant instanceof Sungrass) {
				return true;
			}
		}
		return false;
	}
}
