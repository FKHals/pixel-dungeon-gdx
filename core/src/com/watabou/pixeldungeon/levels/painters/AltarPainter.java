/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.watabou.pixeldungeon.levels.painters;

import com.watabou.pixeldungeon.actors.blobs.SacrificialFire;
import com.watabou.pixeldungeon.levels.Level;
import com.watabou.pixeldungeon.levels.Room;
import com.watabou.pixeldungeon.levels.Terrain;
import com.watabou.utils.Point;

/**
 * A Painter that creates a Room that contains a sacrificial pedestal which spawns a reward when
 * sacrificing enemies on it.
 */
public class AltarPainter implements Room.Painter {

	@Override
	public void paint( Level level, Room room, Level.GenerationConstraints constraints ) {

		Painter.fill( level, room, Terrain.WALL );
		Painter.fill( level, room, 1, constraints.isNextLevelBossLevel ? Terrain.HIGH_GRASS : Terrain.CHASM );

		Point c = room.center(constraints.random);
		Room.Door door = room.entrance();
		if (door.x == room.left || door.x == room.right) {
			Point p = Painter.drawInside( level, room, door, Math.abs( door.x - c.x ) - 2, Terrain.EMPTY_SP );
			for (; p.y != c.y; p.y += p.y < c.y ? +1 : -1) {
				Painter.set( level, p, Terrain.EMPTY_SP );
			}
		} else {
			Point p = Painter.drawInside( level, room, door, Math.abs( door.y - c.y ) - 2, Terrain.EMPTY_SP );
			for (; p.x != c.x; p.x += p.x < c.x ? +1 : -1) {
				Painter.set( level, p, Terrain.EMPTY_SP );
			}
		}

		Painter.fill( level, c.x - 1, c.y - 1, 3, 3, Terrain.EMBERS );
		Painter.set( level, c, Terrain.PEDESTAL );

		door.set( Room.Door.Type.EMPTY );
	}

	@Override
	public void addActors(Level level, Room room, Level.GenerationConstraints constraints) {
		SacrificialFire fire = (SacrificialFire)level.blobs.get( SacrificialFire.class );
		if (fire == null) {
			fire = new SacrificialFire();
		}

		Point position = room.findUniqueTerrainPosition(Terrain.PEDESTAL, level);
		if (!position.equals(new Point(-1, -1))) {
			fire.seed( level.toCell(position), 5 + constraints.depth * 5 );
			level.blobs.put( SacrificialFire.class, fire );
		}

	}

	@Override
	public void addItems(Level level, Room room, Level.GenerationConstraints constraints) {

	}
}
