/*
 * Pixel Dungeon
 * Copyright (C) 2012-2014  Oleg Dolya
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.watabou.pixeldungeon.levels.painters;


import com.watabou.pixeldungeon.items.Generator;
import com.watabou.pixeldungeon.items.Item;
import com.watabou.pixeldungeon.items.Heap.Type;
import com.watabou.pixeldungeon.items.keys.IronKey;
import com.watabou.pixeldungeon.levels.Level;
import com.watabou.pixeldungeon.levels.Room;
import com.watabou.pixeldungeon.levels.Terrain;
import com.watabou.utils.Point;
import com.watabou.utils.Predicate;
import com.watabou.utils.Random;

import java.util.ArrayList;

/**
 * A Painter that creates a locked Room which contains an empty well, a skeleton with the key to
 * entrance door and some loot.
 */
public class PitPainter implements Room.Painter {

	@Override
	public void paint( Level level, Room room, Level.GenerationConstraints constraints ) {

		Painter.fill( level, room, Terrain.WALL );
		Painter.fill( level, room, 1, Terrain.EMPTY );
		
		Room.Door entrance = room.entrance();
		entrance.set( Room.Door.Type.LOCKED );
		
		Point well = null;
		if (entrance.x == room.left) {
			well = new Point( room.right-1, constraints.random.nextInt( 2 ) == 0 ? room.top + 1 : room.bottom - 1 );
		} else if (entrance.x == room.right) {
			well = new Point( room.left+1, constraints.random.nextInt( 2 ) == 0 ? room.top + 1 : room.bottom - 1 );
		} else if (entrance.y == room.top) {
			well = new Point( constraints.random.nextInt( 2 ) == 0 ? room.left + 1 : room.right - 1, room.bottom-1 );
		} else if (entrance.y == room.bottom) {
			well = new Point( constraints.random.nextInt( 2 ) == 0 ? room.left + 1 : room.right - 1, room.top+1 );
		}
		Painter.set( level, well, Terrain.EMPTY_WELL );
	}

	@Override
	public void addActors(Level level, Room room, Level.GenerationConstraints constraints) {
		
	}

	@Override
	public void addItems(final Level level, Room room, Level.GenerationConstraints constraints) {

		ArrayList<Item> itemsInRemains = new ArrayList<>();
		itemsInRemains.add(new IronKey());
		if (Random.Int( 5 ) == 0) {
			itemsInRemains.add( Generator.random( Generator.Category.RING ) );
		} else {
			itemsInRemains.add( Generator.random( Random.oneOf(
					Generator.Category.WEAPON,
					Generator.Category.ARMOR
			)));
		}
		int numberOfPrizes = Random.IntRange( 1, 2 );
		for (int i=0; i < numberOfPrizes; i++) {
			itemsInRemains.add( prize( level ) );
		}

		level.dropMultipleAtSameSpotInRoom(itemsInRemains, Type.SKELETON, room, new Random(),
				new Predicate<Integer>() {
			@Override
			public boolean test(Integer pos) {
				return (level.getCell(pos) != Terrain.EMPTY_WELL);
			}
		});
	}

	private static Item prize( Level level ) {
		
		Item prize = level.itemToSpanAsPrize();
		if (prize != null) {
			return prize;
		}
		
		return Generator.random( Random.oneOf( 
			Generator.Category.POTION, 
			Generator.Category.SCROLL,
			Generator.Category.FOOD, 
			Generator.Category.GOLD
		) );
	}
}
