/*
 * Pixel Dungeon
 * Copyright (C) 2012-2014  Oleg Dolya
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.watabou.pixeldungeon.levels.painters;

import java.util.ArrayList;
import java.util.Collections;

import com.watabou.pixeldungeon.levels.Level;
import com.watabou.pixeldungeon.levels.Room;
import com.watabou.pixeldungeon.levels.Terrain;
import com.watabou.utils.Point;

/**
 * A Painter that creates a passage-tunnel that follows the edge of the Room it draws into
 * which that connects adjacent rooms.
 *
 * So the main difference between a tunnel ({@link TunnelPainter}) and a passage is that the tunnel
 * has a straight path that all doors connect to while the passage has an outer path that "passes"
 * and thereby connects all the doors.
 */
public class PassagePainter implements Room.Painter {

	private static int pasWidth;
	private static int pasHeight;

	/**
	 * Creates a (1-tile-width) tunnel that follows the edge of the Room it draws into that
	 * connects adjacent rooms while leaving a gap between the two Doors (with no other door
	 * between them) with the largest perimeter-wall-gap between them (so that a full circular
	 * tunnel is never created).
	 *
	 * Example of a (4x3) Room (`#` are wall-tiles and the numbers are the integer representation
	 * of the corresponding wall tile that is located nearest too it at the edge of the room):
	 * <pre>
	 * {@code
	 *     0  1  2  3  4  5
	 *     #  #  #  #  #  # <- Walls
	 *  18 #  _  _  _  _  # 6
	 *  17 #  _  Room  _  # 7
	 *  16 #  _  _  _  _  # 8
	 *     #  #  #  #  #  # 9
	 *    14 13 12 11 10
	 * }
	 * </pre>
	 *  If there would be doors at the positions 0, 3, 7 and 14 then the largest gap would have a
	 *  length of 7 (14-7) and would be between 7 and 14.
	 *
	 *  So the resulting tunnel-passage this painter would draw would look like this:
	 *  ('#': wall, '_': empty, 'D': empty with an adjacent door)
	 *  <pre>
	 *  {@code
	 *     0  1  2  3  4  5
	 *     D  _  _  D  _  _ <- Walls
	 *  18 _  #  #  #  #  _ 6
	 *  17 _  #  #  #  #  D 7
	 *  16 _  #  #  #  #  # 8
	 *     D  #  #  #  #  # 9
	 *    14 13 12 11 10
	 * }
	 * </pre>
	 * @param level The Level that the Passage-Room gets painted into
	 * @param room The Room rectangle that the passage gets painted into
	 */
	@Override
	public void paint( Level level, Room room, Level.GenerationConstraints constraints ) {
		
		pasWidth = room.width() - 2;
		pasHeight = room.height() - 2;
		
		Terrain floor = level.tunnelTile();
		
		ArrayList<Integer> joints = new ArrayList<Integer>();
		for (Point door : room.connected.values()) {
			joints.add( xy2p( room, door ) );
		}
		// the locations on the edge need to be sorted so that the largest gap between two doors
		// can be found
		Collections.sort( joints );
		
		int nJoints = joints.size();
		int perimeter = pasWidth * 2 + pasHeight * 2;

		// find the largest gap (measured in tiles on the perimeter) between two doors (with no
		// other doors between them)
		int start = 0;
		int maxD = joints.get( 0 ) + perimeter - joints.get( nJoints - 1 );
		for (int i=1; i < nJoints; i++) {
			int d = joints.get( i ) - joints.get( i - 1 );
			if (d > maxD) {
				maxD = d;
				start = i;
			}
		}
		
		int end = (start + nJoints - 1) % nJoints;

		// draw the tunnel-passage starting with the first door of the pair with the biggest gap
		// between them and ending with the second door of that pair
		int p = joints.get( start );
		do {
			Painter.set( level, p2xy( room, p ), floor );
			p = (p + 1) % perimeter;
		} while (p != joints.get( end ));

		// finally also draw the end point of the passage
		Painter.set( level, p2xy( room, p ), floor );
		
		for (Room.Door door : room.connected.values()) {
			door.set( Room.Door.Type.TUNNEL );
		}
	}

	@Override
	public void addActors(Level level, Room room, Level.GenerationConstraints constraints) {
		
	}

	@Override
	public void addItems(Level level, Room room, Level.GenerationConstraints constraints) {

	}

	/**
	 * Converts an (x,y)-Point to a perimeter-index representation of that point on the perimeter
	 * of that Room (Since the given points are always a Door, it is assured that they are always
	 * located on the perimeter, otherwise this conversion would not work).
	 * Example of a (4x3) Room (`#` are wall-tiles and the numbers are the integer representation
	 * of the corresponding wall tile that is located nearest too it at the edge of the room):
	 *    0  1  2  3  4  5
	 *    #  #  #  #  #  # <- Walls
	 * 18 #  _  _  _  _  # 6
	 * 17 #  _  Room  _  # 7
	 * 16 #  _  _  _  _  # 8
	 * 15 #  #  #  #  #  # 9
	 *   14 13 12 11 10
	 *
	 * @param room The Room on what edge the point is located on
	 * @param xy The Point(x,y) that gets converted
	 * @return An integer that represents a given point in the given Room
	 */
	private static int xy2p( Room room, Point xy ) {
		if (xy.y == room.top) {

			return (xy.x - room.left - 1);

		} else if (xy.x == room.right) {

			return (xy.y - room.top - 1) + pasWidth;

		} else if (xy.y == room.bottom) {

			return (room.right - xy.x - 1) + pasWidth + pasHeight;

		} else /*if (xy.x == room.left)*/ {

			if (xy.y == room.top + 1) {
				return 0;
			} else {
				return (room.bottom - xy.y - 1) + pasWidth * 2 + pasHeight;
			}

		}
	}

	/**
	 * Convert the perimeter-index representation to a (2D) Point.
	 * See {@link #xy2p(Room, Point)} for reverse operation.
	 */
	private static Point p2xy( Room room, int p ) {
		if (p < pasWidth) {
			
			return new Point( room.left + 1 + p, room.top + 1);
			
		} else if (p < pasWidth + pasHeight) {
			
			return new Point( room.right - 1, room.top + 1 + (p - pasWidth) );
			
		} else if (p < pasWidth * 2 + pasHeight) {
			
			return new Point( room.right - 1 - (p - (pasWidth + pasHeight)), room.bottom - 1 );
			
		} else {
			
			return new Point( room.left + 1, room.bottom - 1 - (p - (pasWidth * 2 + pasHeight)) );
			
		}
	}
}
