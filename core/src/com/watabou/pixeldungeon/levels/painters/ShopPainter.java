/*
 * Pixel Dungeon
 * Copyright (C) 2012-2014  Oleg Dolya
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.watabou.pixeldungeon.levels.painters;

import java.util.ArrayList;

;
import com.watabou.pixeldungeon.actors.Actor;
import com.watabou.pixeldungeon.actors.mobs.Mob;
import com.watabou.pixeldungeon.actors.mobs.npcs.ImpShopkeeper;
import com.watabou.pixeldungeon.actors.mobs.npcs.Shopkeeper;
import com.watabou.pixeldungeon.items.Ankh;
import com.watabou.pixeldungeon.items.Generator;
import com.watabou.pixeldungeon.items.Heap;
import com.watabou.pixeldungeon.items.Item;
import com.watabou.pixeldungeon.items.Torch;
import com.watabou.pixeldungeon.items.Weightstone;
import com.watabou.pixeldungeon.items.armor.*;
import com.watabou.pixeldungeon.items.bags.ScrollHolder;
import com.watabou.pixeldungeon.items.bags.SeedPouch;
import com.watabou.pixeldungeon.items.bags.WandHolster;
import com.watabou.pixeldungeon.items.food.OverpricedRation;
import com.watabou.pixeldungeon.items.keys.IronKey;
import com.watabou.pixeldungeon.items.potions.PotionOfHealing;
import com.watabou.pixeldungeon.items.scrolls.ScrollOfIdentify;
import com.watabou.pixeldungeon.items.scrolls.ScrollOfMagicMapping;
import com.watabou.pixeldungeon.items.scrolls.ScrollOfRemoveCurse;
import com.watabou.pixeldungeon.items.weapon.melee.*;
import com.watabou.pixeldungeon.levels.LastShopLevel;
import com.watabou.pixeldungeon.levels.Level;
import com.watabou.pixeldungeon.levels.Room;
import com.watabou.pixeldungeon.levels.Terrain;
import com.watabou.utils.*;

/**
 * A Painter that creates a Room that contains a shopkeeper and its items that are up for sale.
 */
public class ShopPainter implements Room.Painter {

	private static int pasWidth;
	private static int pasHeight;

	@Override
	public void paint( Level level, Room room, Level.GenerationConstraints constraints ) {
		
		Painter.fill( level, room, Terrain.WALL );
		Painter.fill( level, room, 1, Terrain.EMPTY_SP );
		
		for (Room.Door door : room.connected.values()) {
			door.set( Room.Door.Type.REGULAR );
		}
	}

	@Override
	public void addActors(Level level, Room room, Level.GenerationConstraints constraints) {
		placeShopkeeper( level, room );
	}

	@Override
	public void addItems(final Level level, Room room, Level.GenerationConstraints constraints) {
		pasWidth = room.width() - 2;
		pasHeight = room.height() - 2;
		int per = pasWidth * 2 + pasHeight * 2;

		Item[] range = range(constraints.depth);

		int pos = xy2p( room, room.entrance() ) + (per - range.length) / 2;
		for (Item item : range) {

			Point xy = p2xy(room, (pos + per) % per);
			int cell = xy.x + xy.y * level.width();

			if (level.heaps.get(cell) != null) {
				// randomly choose a different empty spot if the position is already occupied by
				// another item-heap
				level.dropInRoom(item, Heap.Type.FOR_SALE, room, new Random(), new Predicate<Integer>() {
					@Override
					public boolean test(Integer pos) {
						return level.heaps.get(pos) == null;
					}
				});
			} else {
				level.drop(item, cell).type = Heap.Type.FOR_SALE;
			}

			pos++;
		}
	}

	private static Item[] range(int depth) {
		
		ArrayList<Item> items = new ArrayList<Item>();
		
		switch (depth) {
		
		case 6:
			items.add( (Random.Int( 2 ) == 0 ? new Quarterstaff() : new Spear()).identify() );
			items.add( new LeatherArmor().identify() );
			items.add( new SeedPouch() );
			items.add( new Weightstone() );
			break;
			
		case 11:
			items.add( (Random.Int( 2 ) == 0 ? new Sword() : new Mace()).identify() );
			items.add( new MailArmor().identify() );
			items.add( new ScrollHolder() );
			items.add( new Weightstone() );
			break;
			
		case 16:
			items.add( (Random.Int( 2 ) == 0 ? new Longsword() : new BattleAxe()).identify() );
			items.add( new ScaleArmor().identify() );
			items.add( new WandHolster() );
			items.add( new Weightstone() );
			break;
			
		case 21:
			switch (Random.Int( 3 )) {
			case 0:
				items.add( new Glaive().identify() );
				break;
			case 1:
				items.add( new WarHammer().identify() );
				break;
			case 2:
				items.add( new PlateArmor().identify() );
				break;
			}
			items.add( new Torch() );
			items.add( new Torch() );
			break;
		}
		
		items.add( new PotionOfHealing() );
		for (int i=0; i < 3; i++) {
			items.add( Generator.random( Generator.Category.POTION ) );
		}
		
		items.add( new ScrollOfIdentify() );
		items.add( new ScrollOfRemoveCurse() );
		items.add( new ScrollOfMagicMapping() );
		items.add( Generator.random( Generator.Category.SCROLL ) );
		
		items.add( new OverpricedRation() );
		items.add( new OverpricedRation() );
		
		items.add( new Ankh() );

		return Random.shuffle(items.toArray( new Item[0] ));
	}
	
	private static void placeShopkeeper( final Level level, Room room ) {
		Mob shopkeeper = level instanceof LastShopLevel ? new ImpShopkeeper() : new Shopkeeper();
		level.placeInRoom(shopkeeper, room, new Random(), new Predicate<Integer>() {
			@Override
			public boolean test(Integer pos) {
				return (level.heaps.get( pos ) == null);
			}
		});
		
		//TODO The following part actually belongs to the #paint() method but since the pos depends
		// on the placement of the items for sale it is not that easy to move out. It could be
		// accomplished by first placing the water and then placing the items relative to that.
		if (level instanceof LastShopLevel) {
			// find the imp shopkeeper and surround him with water
			Optional<Integer> shopkeeperPosition = room.random(new Predicate<Integer>() {
				@Override
				public boolean test(Integer pos) {
					return Actor.findChar(pos) instanceof Shopkeeper;
				}
			}, level, new Random());
			if (shopkeeperPosition.isPresent()) {
				for (int i=0; i < Level.NEIGHBOURS9.length; i++) {
					int p = shopkeeperPosition.get() + Level.NEIGHBOURS9[i];
					if (level.getCell(p) == Terrain.EMPTY_SP) {
						level.setCell(p, Terrain.WATER);
					}
				}
			} else {
				DLog.error("Level", "Could not find the shopkeeper!");
				// TODO Throw exception and regenerate level?!
			}
		}
	}

	private static int xy2p( Room room, Point xy ) {
		if (xy.y == room.top) {

			return (xy.x - room.left - 1);

		} else if (xy.x == room.right) {

			return (xy.y - room.top - 1) + pasWidth;

		} else if (xy.y == room.bottom) {

			return (room.right - xy.x - 1) + pasWidth + pasHeight;

		} else /*if (xy.x == room.left)*/ {

			if (xy.y == room.top + 1) {
				return 0;
			} else {
				return (room.bottom - xy.y - 1) + pasWidth * 2 + pasHeight;
			}

		}
	}
	
	private static Point p2xy( Room room, int p ) {
		if (p < pasWidth) {
			
			return new Point( room.left + 1 + p, room.top + 1);
			
		} else if (p < pasWidth + pasHeight) {
			
			return new Point( room.right - 1, room.top + 1 + (p - pasWidth) );
			
		} else if (p < pasWidth * 2 + pasHeight) {
			
			return new Point( room.right - 1 - (p - (pasWidth + pasHeight)), room.bottom - 1 );
			
		} else {
			
			return new Point( room.left + 1, room.bottom - 1 - (p - (pasWidth * 2 + pasHeight)) );
			
		}
	}
}
