/*
 * Pixel Dungeon
 * Copyright (C) 2012-2014  Oleg Dolya
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.watabou.pixeldungeon.levels;

import java.util.List;

import com.watabou.noosa.Scene;
import com.watabou.pixeldungeon.Bones;
import com.watabou.pixeldungeon.Dungeon;
import com.watabou.pixeldungeon.actors.Actor;
import com.watabou.pixeldungeon.actors.Char;
import com.watabou.pixeldungeon.actors.mobs.Bestiary;
import com.watabou.pixeldungeon.actors.mobs.Mob;
import com.watabou.pixeldungeon.items.Heap;
import com.watabou.pixeldungeon.items.Item;
import com.watabou.pixeldungeon.items.keys.IronKey;
import com.watabou.pixeldungeon.items.keys.SkeletonKey;
import com.watabou.pixeldungeon.levels.Room.Type;
import com.watabou.pixeldungeon.levels.painters.Painter;
import com.watabou.pixeldungeon.levels.terraintypes.PrisonTerrain;
import com.watabou.pixeldungeon.scenes.GameScene;
import com.watabou.utils.*;

public class PrisonBossLevel extends RegularLevel {

	{
		terrain = new PrisonTerrain();
	}
	
	private Room anteroom;
	private int arenaDoor;
	
	private boolean enteredArena = false;
	private boolean keyDropped = false;
	
	private static final String ARENA	= "arena";
	private static final String DOOR	= "door";
	private static final String ENTERED	= "entered";
	private static final String DROPPED	= "droppped";
	
	@Override
	public void storeInBundle( Bundle bundle ) {
		super.storeInBundle( bundle );
		bundle.put( ARENA, roomExit );
		bundle.put( DOOR, arenaDoor );
		bundle.put( ENTERED, enteredArena );
		bundle.put( DROPPED, keyDropped );
	}
	
	@Override
	public void restoreFromBundle( Bundle bundle ) {
		super.restoreFromBundle( bundle );
		roomExit = (Room)bundle.get( ARENA );
		arenaDoor = bundle.getInt( DOOR );
		enteredArena = bundle.getBoolean( ENTERED );
		keyDropped = bundle.getBoolean( DROPPED );
	}
	
	@Override
	protected boolean build(GenerationConstraints constraints) {
		
		initRooms(constraints.random);
	
		int distance;
		int retry = 0;

		do {
			
			if (retry++ > 10) {
				return false;
			}
			
			int innerRetry = 0;
			do {
				if (innerRetry++ > 10) {
					return false;
				}
				roomEntrance = constraints.random.nextElement( rooms );
			} while (roomEntrance.width() < 4 || roomEntrance.height() < 4);
			
			innerRetry = 0;
			do {
				if (innerRetry++ > 10) {
					return false;
				}
				roomExit = constraints.random.nextElement( rooms );
			} while (
				roomExit == roomEntrance || 
				roomExit.width() < 7 || 
				roomExit.height() < 7 || 
				roomExit.top == 0);
	
			Graph.buildDistanceMap( rooms, roomExit );
			distance = Graph.buildPath( rooms, roomEntrance, roomExit ).size();
			
		} while (distance < 3);
		
		roomEntrance.type = Type.ENTRANCE;
		roomExit.type = Type.BOSS_EXIT;
		
		List<Room> path = Graph.buildPath( rooms, roomEntrance, roomExit );	
		Graph.setPrice( path, roomEntrance.distance );
		
		Graph.buildDistanceMap( rooms, roomExit );
		path = Graph.buildPath( rooms, roomEntrance, roomExit );
		
		anteroom = path.get( path.size() - 2 );
		anteroom.type = Type.STANDARD;
		
		Room room = roomEntrance;
		for (Room next : path) {
			room.connect( next );
			room = next;
		}
		
		for (Room r : rooms) {
			if (r.type == Type.NULL && r.connected.size() > 0) {
				r.type = Type.PASSAGE; 
			}
		}
		
		paint(constraints);
		
		Room r = (Room)roomExit.connected.keySet().toArray()[0];
		if (roomExit.connected.get( r ).y == roomExit.top) {
			return false;
		}
		
		paintWater(constraints.random);
		paintGrass(constraints.random);
		
		placeTraps(constraints);
		
		return true;
	}
		
	protected boolean[] water(Random random) {
		return Patch.generate( 0.45f, 5, random);
	}
	
	protected boolean[] grass(Random random) {
		return Patch.generate( 0.30f, 4, random);
	}
	
	protected void paintDoors( Room r ) {
		
		for (Room n : r.connected.keySet()) {
			
			if (r.type == Type.NULL) {
				continue;
			}
			
			Point door = r.connected.get( n );
			
			if (r.type == Room.Type.PASSAGE && n.type == Room.Type.PASSAGE) {
				
				Painter.set( this, door, Terrain.EMPTY );
				
			} else {
				
				Painter.set( this, door, Terrain.DOOR );
				
			}
			
		}
	}
	
	@Override
	protected void placeTraps(GenerationConstraints constraints) {
		
		int nTraps = nTraps(constraints.depth, constraints.random);

		for (int i=0; i < nTraps; i++) {
			
			int trapPos = constraints.random.nextInt( LENGTH );
			
			if (getCell(trapPos) == Terrain.EMPTY) {
				setCell(trapPos, Terrain.POISON_TRAP);
			}
		}
	}
	
	@Override
	protected void decorate(GenerationConstraints constraints) {
		
		for (int i=WIDTH + 1; i < LENGTH - WIDTH - 1; i++) {
			if (getCell(i) == Terrain.EMPTY) {
				
				float c = 0.15f;
				if (getCell(i + 1) == Terrain.WALL && getCell(i + WIDTH) == Terrain.WALL) {
					c += 0.2f;
				}
				if (getCell(i - 1) == Terrain.WALL && getCell(i + WIDTH) == Terrain.WALL) {
					c += 0.2f;
				}
				if (getCell(i + 1) == Terrain.WALL && getCell(i - WIDTH) == Terrain.WALL) {
					c += 0.2f;
				}
				if (getCell(i - 1) == Terrain.WALL && getCell(i - WIDTH) == Terrain.WALL) {
					c += 0.2f;
				}
				
				if (constraints.random.nextFloat() < c) {
					setCell(i, Terrain.EMPTY_DECO);
				}
			}
		}
		
		for (int i=0; i < WIDTH; i++) {
			if (getCell(i) == Terrain.WALL &&
				(getCell(i + WIDTH) == Terrain.EMPTY || getCell(i + WIDTH) == Terrain.EMPTY_SP) &&
				constraints.random.nextInt( 4 ) == 0) {
				
				setCell(i, Terrain.WALL_DECO);
			}
		}
		
		for (int i=WIDTH; i < LENGTH - WIDTH; i++) {
			if (getCell(i) == Terrain.WALL &&
				getCell(i - WIDTH) == Terrain.WALL &&
				(getCell(i + WIDTH) == Terrain.EMPTY || getCell(i + WIDTH) == Terrain.EMPTY_SP) &&
				constraints.random.nextInt( 2 ) == 0) {
				
				setCell(i, Terrain.WALL_DECO);
			}
		}
		
		placeSign(constraints.random);
		
		Point door = roomExit.entrance();
		arenaDoor = door.x + door.y * WIDTH;
		Painter.set( this, arenaDoor, Terrain.LOCKED_DOOR );
		
		Painter.fill( this, 
			roomExit.left + 2,
			roomExit.top + 2,
			roomExit.width() - 3,
			roomExit.height() - 3,
			Terrain.INACTIVE_TRAP );
	}
	
	@Override
	protected void createMobs(GenerationConstraints constraints) {
	}
	
	public Actor respawner() {
		return null;
	}
	
	@Override
	protected void createItems(GenerationConstraints constraints) {

		dropInRoom(new IronKey(), Heap.Type.CHEST, anteroom, new Random(), new Predicate<Integer>() {
			@Override
			public boolean test(Integer pos) {
				return isPassable(pos);
			}
		});
		
		Item item = Bones.get();
		if (item != null) {
			dropInRoom(item, Heap.Type.SKELETON, roomEntrance, new Random(), new Predicate<Integer>() {
				@Override
				public boolean test(Integer pos) {
					return pos != entrance && getCell(pos) != Terrain.SIGN;
				}
			});
		}
	}

	@Override
	public void press(final int cell, Char ch ) {
		
		super.press( cell, ch );
		
		if (ch == Dungeon.hero && !enteredArena && roomExit.inside( new Point(cell % width(), cell / width()) )) {
			
			enteredArena = true;

			Optional<Integer> pos = roomExit.random(new Predicate<Integer>() {
				@Override
				public boolean test(Integer pos) {
					return (pos == cell || Actor.findChar( pos ) != null);
				}
			}, this, new Random());
			if (!pos.isPresent()) {
				DLog.error("CreateItems", "Could not drop skeleton key!");
				// TODO Throw exception and regenerate level?!
			}
			
			Mob boss = Bestiary.mob( Dungeon.depth );
			boss.state = boss.HUNTING;
			boss.pos = pos.get();
			GameScene.add( boss );
			boss.notice();
			
			mobPress( boss );
			
			set( arenaDoor, Terrain.LOCKED_DOOR );
			GameScene.updateMap( arenaDoor );
			Dungeon.observe();
		}
	}
	
	@Override
	public Heap drop( Item item, int cell ) {
		
		if (!keyDropped && item instanceof SkeletonKey) {
			
			keyDropped = true;
			
			set( arenaDoor, Terrain.DOOR );
			GameScene.updateMap( arenaDoor );
			Dungeon.observe();
		}
		
		return super.drop( item, cell );
	}
	
	@Override
	public int randomRespawnCell() {
		return -1;
	}

	@Override
	public void addVisuals( Scene scene ) {
		PrisonLevel.addVisuals( this, scene );
	}
}
