/*
 * Pixel Dungeon
 * Copyright (C) 2012-2014  Oleg Dolya
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.watabou.pixeldungeon.levels;

import com.badlogic.gdx.utils.IntMap;
import com.watabou.pixeldungeon.actors.Char;
import com.watabou.pixeldungeon.levels.features.*;
import com.watabou.pixeldungeon.levels.features.traps.*;
import com.watabou.pixeldungeon.utils.GLog;

/**
 * Declares the different terrains and its properties a tile in a level can have.
 */
public enum Terrain {
	EMPTY (1, F.PASSABLE),
	EMPTY_WELL (3, EMPTY),
	ENTRANCE (7, EMPTY),
	EXIT (8, EMPTY),
	EMBERS (9, EMPTY),
	EMPTY_DECO (24, EMPTY),
	UNLOCKED_EXIT (26, EMPTY),
	ALCHEMY (42, EMPTY, new AlchemyPot()),
	INACTIVE_TRAP (23, EMPTY),

	GRASS (2, F.PASSABLE | F.FLAMMABLE),
	WALL (4, F.LOS_BLOCKING | F.SOLID | F.UNSTITCHABLE),
	WALL_DECO (12, WALL),
	DOOR (5, F.PASSABLE | F.LOS_BLOCKING | F.FLAMMABLE | F.SOLID | F.UNSTITCHABLE, new Door()),
	OPEN_DOOR (6, F.PASSABLE | F.FLAMMABLE | F.UNSTITCHABLE),
	LOCKED_DOOR (10, F.LOS_BLOCKING | F.SOLID | F.UNSTITCHABLE),
	PEDESTAL (11, F.PASSABLE | F.UNSTITCHABLE),
	BARRICADE (13, F.FLAMMABLE | F.SOLID | F.LOS_BLOCKING),
	EMPTY_SP (14, EMPTY.getFlags() | F.UNSTITCHABLE),
	HIGH_GRASS (15, F.PASSABLE | F.LOS_BLOCKING | F.FLAMMABLE, new HighGrass()),
	LOCKED_EXIT (25, F.SOLID),
	SIGN (29, F.PASSABLE | F.FLAMMABLE),
	WELL (34, F.AVOID, new Well()),
	STATUE (35, F.SOLID),
	STATUE_SP (36, STATUE.getFlags() | F.UNSTITCHABLE),
	BOOKSHELF (41, BARRICADE.getFlags() | F.UNSTITCHABLE),
	SECRET_DOOR (16, WALL.getFlags() | F.SECRET | F.UNSTITCHABLE, DOOR),

	CHASM (0, F.AVOID | F.PIT | F.UNSTITCHABLE, new Chasm()),
	CHASM_FLOOR (43, CHASM, new Chasm()),
	CHASM_FLOOR_SP (44, CHASM, new Chasm()),
	CHASM_WALL (45, CHASM, new Chasm()),
	CHASM_WATER (46, CHASM, new Chasm()),

	/** Helper enum for better readability of the flags (Do not use outside of this class) */
	_TRAP(-1, F.AVOID),

	TOXIC_TRAP (17, _TRAP, new ToxicTrap()),
	FIRE_TRAP (19, _TRAP, new FireTrap()),
	PARALYTIC_TRAP (21, _TRAP, new ParalyticTrap()),
	POISON_TRAP (27, _TRAP, new PoisonTrap()),
	ALARM_TRAP (30, _TRAP, new AlarmTrap()),
	LIGHTNING_TRAP (32, _TRAP, new LightningTrap()),
	GRIPPING_TRAP (37, _TRAP, new GrippingTrap()),
	SUMMONING_TRAP (39, _TRAP, new SummoningTrap()),

	/** Helper enum for better readability of the flags (Do not use outside of this class) */
	_SECRET_TRAP(-1, EMPTY.getFlags() | F.SECRET),

	SECRET_TOXIC_TRAP (18, _SECRET_TRAP, TOXIC_TRAP),
	SECRET_FIRE_TRAP (20, _SECRET_TRAP, FIRE_TRAP),
	SECRET_PARALYTIC_TRAP (22, _SECRET_TRAP, PARALYTIC_TRAP),
	SECRET_POISON_TRAP (28, _SECRET_TRAP, POISON_TRAP),
	SECRET_ALARM_TRAP (31, _SECRET_TRAP, ALARM_TRAP),
	SECRET_LIGHTNING_TRAP (33, _SECRET_TRAP, LIGHTNING_TRAP),
	SECRET_GRIPPING_TRAP (38, _SECRET_TRAP, GRIPPING_TRAP),
	SECRET_SUMMONING_TRAP (40, _SECRET_TRAP, SUMMONING_TRAP),

	/**
	 * The different water tiles that get selected depending on the surrounding PASSABLE tiles
	 * (see Level#getWaterTile(int)) to make them blend in
	 */
	WATER (63, F.PASSABLE | F.LIQUID | F.UNSTITCHABLE),
	WATER_TILE_0(48, WATER),
	WATER_TILE_1(49, WATER),
	WATER_TILE_2(50, WATER),
	WATER_TILE_3(51, WATER),
	WATER_TILE_4(52, WATER),
	WATER_TILE_5(53, WATER),
	WATER_TILE_6(54, WATER),
	WATER_TILE_7(55, WATER),
	WATER_TILE_8(56, WATER),
	WATER_TILE_9(57, WATER),
	WATER_TILE_10(58, WATER),
	WATER_TILE_11(59, WATER),
	WATER_TILE_12(60, WATER),
	WATER_TILE_13(61, WATER),
	WATER_TILE_14(62, WATER);

	private final int value;
	private final int flags;
	private final Terrain discovered;
	private final TerrainFeature feature;

	Terrain( int value, int flags, Terrain discovered, TerrainFeature feature) {
		this.value = value;
		this.flags = flags;
		this.discovered = discovered;
		this.feature = feature;
	}

	// this constructor cannot telescope the available constructor because of the use of "this"
	// before initialization
	Terrain( int value, int flags, TerrainFeature feature) {
		this.value = value;
		this.flags = flags;
		this.discovered = this;
		this.feature = feature;
	}

	Terrain( int value, int flags, Terrain discovered) {
		this(value, flags, discovered, new None());
	}

	Terrain( int value, int flags) {
		this(value, flags, new None());
	}

	/**
	 * For conciseness purposes so that the created Terrain inherits the flags (and therefore some
	 * of its properties) of the given one.
	 * @param value The index/value the Terrain is identified by (on the {@link Level#getRawMap()})
	 * @param terrain The Terrain that the flags get inherited from
	 */
	Terrain( int value, Terrain terrain) {
		this(value, terrain.getFlags());
	}

	Terrain( int value, Terrain terrain, TerrainFeature feature) {
		this(value, terrain.getFlags(), feature);
	}

	/**
	 * @param parent The parent Terrain that this Terrain inherits its "discovered"- and "feature"-
	 *               fields from (mostly used by secret tiles that inherit from its non-secret
	 *               versions)
	 */
	Terrain( int value, Terrain terrain, Terrain parent) {
		this(value, terrain.getFlags(), parent.discovered, parent.feature);
	}

	private static final IntMap<Terrain> map = new IntMap<>();
	static {
		for (Terrain terrain : Terrain.values()) {
			// drop negative values which could only be helper enums that should not be used
			// outside the class and therefore should not be able to be found by value.
			if (terrain.value >= 0) {
				map.put(terrain.value, terrain);
			}
		}
	}

	public static Terrain valueOf(int terrain) {
		return map.get(terrain);
	}

	public int getValue() {
		return value;
	}

	public int getFlags() {
		return flags;
	}

	/**
	 * @return The Terrain that it becomes on discovery (e. g. SECRET_DOOR -> DOOR). By default
	 * this just returns the Terrain itself (since most Terrains are not SECRET)
	 */
	public Terrain discover() {
		return discovered;
	}

	private static final String TXT_HIDDEN_PLATE_CLICKS = "A hidden pressure plate clicks!";

	/**
	 * Handle the entering (the movement onto a tile) of a cell by a character which is delegated
	 * to the TerrainFeature instance of the feature field.
	 * @return If the entered Terrain is a Trap
	 */
	public boolean press(Level level, int cell, Char character) {
		boolean isTrapFeature = feature instanceof Trap;
		if (isTrapFeature && isSecret()) {
			GLog.i( TXT_HIDDEN_PLATE_CLICKS );
		}
		feature.enter(level, cell, character);
		return isTrapFeature;
	}

	/**
	 * F(lags) Helper class to make the usage of the flags less tedious (e. g. by having to write
	 * out F.PASSABLE everytime) since java enums do not allow to define those constants
	 * before initializing all enums (because enum init must always come first by definition).
	 */
	private class F {
		public static final int PASSABLE = 0x01;
		public static final int LOS_BLOCKING = 0x02;
		public static final int FLAMMABLE = 0x04;
		public static final int SECRET = 0x08;
		public static final int SOLID = 0x10;
		public static final int AVOID = 0x20;
		public static final int LIQUID = 0x40;
		public static final int PIT = 0x80;

		public static final int UNSTITCHABLE = 0x100;
	}

	public boolean isPassable() {
		return (flags & F.PASSABLE) != 0;
	}

	public boolean isLosBlocking() {
		return (flags & F.LOS_BLOCKING) != 0;
	}

	public boolean isFlammable() {
		return (flags & F.FLAMMABLE) != 0;
	}

	public boolean isSecret() {
		return (flags & F.SECRET) != 0;
	}

	public boolean isSolid() {
		return (flags & F.SOLID) != 0;
	}

	public boolean isAvoided() {
		return (flags & F.AVOID) != 0;
	}

	public boolean isLiquid() {
		return (flags & F.LIQUID) != 0;
	}

	public boolean isPit() {
		return (flags & F.PIT) != 0;
	}

	public boolean isUnstitchable() {
		return (flags & F.UNSTITCHABLE) != 0;
	}
}
