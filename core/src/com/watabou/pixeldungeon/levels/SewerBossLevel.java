/*
 * Pixel Dungeon
 * Copyright (C) 2012-2014  Oleg Dolya
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.watabou.pixeldungeon.levels;

import java.util.ArrayList;
import java.util.List;

import com.watabou.noosa.Scene;
import com.watabou.pixeldungeon.Bones;
import com.watabou.pixeldungeon.actors.Actor;
import com.watabou.pixeldungeon.actors.mobs.Bestiary;
import com.watabou.pixeldungeon.actors.mobs.Mob;
import com.watabou.pixeldungeon.items.Heap;
import com.watabou.pixeldungeon.items.Item;
import com.watabou.pixeldungeon.levels.Room.Type;
import com.watabou.pixeldungeon.levels.terraintypes.SewerTerrain;
import com.watabou.pixeldungeon.scenes.GameScene;
import com.watabou.utils.Bundle;
import com.watabou.utils.Graph;
import com.watabou.utils.Random;
import com.watabou.utils.Predicate;

public class SewerBossLevel extends RegularLevel {

	{
		terrain = new SewerTerrain();
	}
	
	private int stairs = 0;
	
	@Override
	protected boolean build(GenerationConstraints constraints) {
		
		initRooms(constraints.random);
	
		int distance;
		int retry = 0;
		int minDistance = (int)Math.sqrt( rooms.size() );
		do {
			int innerRetry = 0;
			do {
				if (innerRetry++ > 10) {
					return false;
				}
				roomEntrance = constraints.random.nextElement( rooms );
			} while (roomEntrance.width() < 4 || roomEntrance.height() < 4);
			
			innerRetry = 0;
			do {
				if (innerRetry++ > 10) {
					return false;
				}
				roomExit = constraints.random.nextElement( rooms );
			} while (roomExit == roomEntrance || roomExit.width() < 6 || roomExit.height() < 6 || roomExit.top == 0);
	
			Graph.buildDistanceMap( rooms, roomExit );
			distance = roomEntrance.distance();
			
			if (retry++ > 10) {
				return false;
			}
			
		} while (distance < minDistance);
		
		roomEntrance.type = Type.ENTRANCE;
		roomExit.type = Type.BOSS_EXIT;
		
		Graph.buildDistanceMap( rooms, roomExit );
		List<Room> path = Graph.buildPath( rooms, roomEntrance, roomExit );
		
		Graph.setPrice( path, roomEntrance.distance );
		
		Graph.buildDistanceMap( rooms, roomExit );
		path = Graph.buildPath( rooms, roomEntrance, roomExit );
		
		Room room = roomEntrance;
		for (Room next : path) {
			room.connect( next );
			room = next;
		}
		
		room = (Room)roomExit.connected.keySet().toArray()[0];
		if (roomExit.top == room.bottom) {
			return false;
		}
		
		for (Room r : rooms) {
			if (r.type == Type.NULL && r.connected.size() > 0) {
				r.type = Type.TUNNEL; 
			}
		}
		
		ArrayList<Room> candidates = new ArrayList<Room>();
		for (Room r : roomExit.neigbours) {
			if (!roomExit.connected.containsKey( r ) &&
				(roomExit.left == r.right || roomExit.right == r.left || roomExit.bottom == r.top)) {
				candidates.add( r );
			}
		}
		if (candidates.size() > 0) {
			Room kingsRoom = constraints.random.nextElement( candidates );
			kingsRoom.connect( roomExit );
			kingsRoom.type = Room.Type.RAT_KING;
		}
		
		paint(constraints);
		
		paintWater(constraints.random);
		paintGrass(constraints.random);
		
		placeTraps(constraints);
		
		return true;
	}
		
	protected boolean[] water(Random random) {
		return Patch.generate( 0.5f, 5, random);
	}
	
	protected boolean[] grass(Random random) {
		return Patch.generate( 0.40f, 4, random);
	}
	
	@Override
	protected void decorate(GenerationConstraints constraints) {
		int start = roomExit.top * WIDTH + roomExit.left + 1;
		int end = start + roomExit.width() - 1;
		for (int i=start; i < end; i++) {
			if (i != exit) {
				setCell(i, Terrain.WALL_DECO);
				setCell(i + WIDTH, Terrain.WATER);
			} else {
				setCell(i + WIDTH, Terrain.EMPTY);
			}
		}
		
		placeSign(constraints.random);
	}
	
	@Override
	public void addVisuals( Scene scene ) {
		SewerLevel.addVisuals( this, scene );
	}
	
	
	@Override
	protected void createMobs(GenerationConstraints constraints) {
		Mob mob = Bestiary.mob( constraints.depth );
		mob.pos = roomExit.random(this, new Random());
		addMob( mob );
	}
	
	public Actor respawner() {
		return null;
	}
	
	@Override
	protected void createItems(GenerationConstraints constraints) {
		Item item = Bones.get();
		if (item != null) {
			dropInRoom(item, Heap.Type.SKELETON, roomEntrance, new Random(), new Predicate<Integer>() {
				@Override
				public boolean test(Integer pos) {
					return (pos != entrance && getCell(pos) != Terrain.SIGN);
				}
			});
		}
	}
	
	public void seal() {
		if (entrance != 0) {
			
			set( entrance, Terrain.WATER_TILE_0);
			GameScene.updateMap( entrance );
			GameScene.ripple( entrance );
			
			stairs = entrance;
			entrance = 0;
		}
	}
	
	public void unseal() {
		if (stairs != 0) {
			
			entrance = stairs;
			stairs = 0;
			
			set( entrance, Terrain.ENTRANCE );
			GameScene.updateMap( entrance );
		}
	}
	
	private static final String STAIRS	= "stairs";
	
	@Override
	public void storeInBundle( Bundle bundle ) {
		super.storeInBundle( bundle );
		bundle.put( STAIRS, stairs );
	}
	
	@Override
	public void restoreFromBundle( Bundle bundle ) {
		super.restoreFromBundle( bundle );
		stairs = bundle.getInt( STAIRS );
	}
}
