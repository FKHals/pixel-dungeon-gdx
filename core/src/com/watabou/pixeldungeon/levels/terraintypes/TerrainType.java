package com.watabou.pixeldungeon.levels.terraintypes;

import com.watabou.pixeldungeon.levels.Terrain;

/**
 * This class contains the properties that are specific for a terrain.
 */
public interface TerrainType {

    /**
     * @return The texture to be used by this terrain type (for everything but the water)
     */
    String tilesTexture();

    /**
     * @return The texture to be used for the water of this terrain type
     */
    String waterTexture();

    /**
     * @return The color that leaves could have in this terrain
     * (expected format is hexadecimal: 0x...)
     */
    int leafColor();

    /**
     * @return The alternative (second) color that leaves could have in this terrain
     * (expected format is hexadecimal: 0x...)
     */
    int leafColorAlternative();

    /**
     * @param tile The examined cell
     * @return The name of the terrain of that examined cell
     */
    String tileName( Terrain tile );

    /**
     * @param tile The examined cell
     * @return The description of the terrain and it's properties of that examined cell
     */
    String tileDesc( Terrain tile );
}
