package com.watabou.pixeldungeon.levels.terraintypes;

import com.watabou.pixeldungeon.levels.Terrain;

/**
 * A default terrain type that holds some default values which get used by most of the other
 * terrain types
 */
public class DefaultTerrainValues {

    public static int leafColor() {
        return 0x004400;
    }

    public static int leafColorAlternative() {
        return 0x88CC44;
    }

    public static String tileName(Terrain tile) {

        if (tile.getValue() >= Terrain.WATER_TILE_0.getValue()) {
            return tileName( Terrain.WATER );
        }

        if (tile != Terrain.CHASM && (tile.isPit())) {
            return tileName( Terrain.CHASM );
        }

        switch (tile) {
            case CHASM:
                return "Chasm";
            case EMPTY:
            case EMPTY_SP:
            case EMPTY_DECO:
            case SECRET_TOXIC_TRAP:
            case SECRET_FIRE_TRAP:
            case SECRET_PARALYTIC_TRAP:
            case SECRET_POISON_TRAP:
            case SECRET_ALARM_TRAP:
            case SECRET_LIGHTNING_TRAP:
                return "Floor";
            case GRASS:
                return "Grass";
            case WATER:
                return "Water";
            case WALL:
            case WALL_DECO:
            case SECRET_DOOR:
                return "Wall";
            case DOOR:
                return "Closed door";
            case OPEN_DOOR:
                return "Open door";
            case ENTRANCE:
                return "Depth entrance";
            case EXIT:
                return "Depth exit";
            case EMBERS:
                return "Embers";
            case LOCKED_DOOR:
                return "Locked door";
            case PEDESTAL:
                return "Pedestal";
            case BARRICADE:
                return "Barricade";
            case HIGH_GRASS:
                return "High grass";
            case LOCKED_EXIT:
                return "Locked depth exit";
            case UNLOCKED_EXIT:
                return "Unlocked depth exit";
            case SIGN:
                return "Sign";
            case WELL:
                return "Well";
            case EMPTY_WELL:
                return "Empty well";
            case STATUE:
            case STATUE_SP:
                return "Statue";
            case TOXIC_TRAP:
                return "Toxic gas trap";
            case FIRE_TRAP:
                return "Fire trap";
            case PARALYTIC_TRAP:
                return "Paralytic gas trap";
            case POISON_TRAP:
                return "Poison dart trap";
            case ALARM_TRAP:
                return "Alarm trap";
            case LIGHTNING_TRAP:
                return "Lightning trap";
            case GRIPPING_TRAP:
                return "Gripping trap";
            case SUMMONING_TRAP:
                return "Summoning trap";
            case INACTIVE_TRAP:
                return "Triggered trap";
            case BOOKSHELF:
                return "Bookshelf";
            case ALCHEMY:
                return "Alchemy pot";
            default:
                return "???";
        }
    }

    public static String tileDesc(Terrain tile) {

        switch (tile) {
            case CHASM:
                return "You can't see the bottom.";
            case WATER:
                return "In case of burning step into the water to extinguish the fire.";
            case ENTRANCE:
                return "Stairs lead up to the upper depth.";
            case EXIT:
            case UNLOCKED_EXIT:
                return "Stairs lead down to the lower depth.";
            case EMBERS:
                return "Embers cover the floor.";
            case HIGH_GRASS:
                return "Dense vegetation blocks the view.";
            case LOCKED_DOOR:
                return "This door is locked, you need a matching key to unlock it.";
            case LOCKED_EXIT:
                return "Heavy bars block the stairs leading down.";
            case BARRICADE:
                return "The wooden barricade is firmly set but has dried over the years. Might it burn?";
            case SIGN:
                return "You can't read the text from here.";
            case TOXIC_TRAP:
            case FIRE_TRAP:
            case PARALYTIC_TRAP:
            case POISON_TRAP:
            case ALARM_TRAP:
            case LIGHTNING_TRAP:
            case GRIPPING_TRAP:
            case SUMMONING_TRAP:
                return "Stepping onto a hidden pressure plate will activate the trap.";
            case INACTIVE_TRAP:
                return "The trap has been triggered before and it's not dangerous anymore.";
            case STATUE:
            case STATUE_SP:
                return "Someone wanted to adorn this place, but failed, obviously.";
            case ALCHEMY:
                return "Drop some seeds here to cook a potion.";
            case EMPTY_WELL:
                return "The well has run dry.";
            default:
                if (tile.getValue() >= Terrain.WATER_TILE_0.getValue()) {
                    return tileDesc( Terrain.WATER );
                }
                if (tile.isPit()) {
                    return tileDesc( Terrain.CHASM );
                }
                return "";
        }
    }
}
