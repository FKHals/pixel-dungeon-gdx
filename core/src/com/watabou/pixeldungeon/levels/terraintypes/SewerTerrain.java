package com.watabou.pixeldungeon.levels.terraintypes;

import com.watabou.pixeldungeon.Assets;
import com.watabou.pixeldungeon.levels.Terrain;

public class SewerTerrain implements TerrainType {

    @Override
    public String tilesTexture() {
        return Assets.TILES_SEWERS;
    }

    @Override
    public String waterTexture() {
        return Assets.WATER_SEWERS;
    }

    @Override
    public int leafColor() {
        return 0x48763c;
    }

    @Override
    public int leafColorAlternative() {
        return 0x59994a;
    }

    @Override
    public String tileName( Terrain tile ) {
        if (tile == Terrain.WATER) {
            return "Murky water";
        }
        return DefaultTerrainValues.tileName(tile);
    }

    @Override
    public String tileDesc(Terrain tile) {
        switch (tile) {
            case EMPTY_DECO:
                return "Wet yellowish moss covers the floor.";
            case BOOKSHELF:
                return "The bookshelf is packed with cheap useless books. Might it burn?";
            default:
                return DefaultTerrainValues.tileDesc( tile );
        }
    }
}
