package com.watabou.pixeldungeon.levels.terraintypes;

import com.watabou.pixeldungeon.Assets;
import com.watabou.pixeldungeon.levels.Terrain;

public class CavesTerrain implements TerrainType {

    @Override
    public String tilesTexture() {
        return Assets.TILES_CAVES;
    }

    @Override
    public String waterTexture() {
        return Assets.WATER_CAVES;
    }

    @Override
    public int leafColor() {
        return 0x534f3e;
    }

    @Override
    public int leafColorAlternative() {
        return 0xb9d661;
    }

    @Override
    public String tileName( Terrain tile ) {
        switch (tile) {
            case GRASS:
                return "Fluorescent moss";
            case HIGH_GRASS:
                return "Fluorescent mushrooms";
            case WATER:
                return "Freezing cold water.";
            default:
                return DefaultTerrainValues.tileName( tile );
        }
    }

    @Override
    public String tileDesc( Terrain tile ) {
        switch (tile) {
            case ENTRANCE:
                return "The ladder leads up to the upper depth.";
            case EXIT:
                return "The ladder leads down to the lower depth.";
            case HIGH_GRASS:
                return "Huge mushrooms block the view.";
            case WALL_DECO:
                return "A vein of some ore is visible on the wall. Gold?";
            case BOOKSHELF:
                return "Who would need a bookshelf in a cave?";
            default:
                return DefaultTerrainValues.tileDesc( tile );
        }
    }
}
