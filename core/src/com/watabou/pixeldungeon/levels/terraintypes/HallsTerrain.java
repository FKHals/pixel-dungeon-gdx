package com.watabou.pixeldungeon.levels.terraintypes;

import com.watabou.pixeldungeon.Assets;
import com.watabou.pixeldungeon.levels.Terrain;

public class HallsTerrain implements TerrainType {

    @Override
    public String tilesTexture() {
        return Assets.TILES_HALLS;
    }

    @Override
    public String waterTexture() {
        return Assets.WATER_HALLS;
    }

    @Override
    public int leafColor() {
        return 0x801500;
    }

    @Override
    public int leafColorAlternative() {
        return 0xa68521;
    }

    @Override
    public String tileName( Terrain tile ) {
        switch (tile) {
            case WATER:
                return "Cold lava";
            case GRASS:
                return "Embermoss";
            case HIGH_GRASS:
                return "Emberfungi";
            case STATUE:
            case STATUE_SP:
                return "Pillar";
            default:
                return DefaultTerrainValues.tileName( tile );
        }
    }

    @Override
    public String tileDesc(Terrain tile) {
        switch (tile) {
            case WATER:
                return "It looks like lava, but it's cold and probably safe to touch.";
            case STATUE:
            case STATUE_SP:
                return "The pillar is made of real humanoid skulls. Awesome.";
            case BOOKSHELF:
                return "Books in ancient languages smoulder in the bookshelf.";
            default:
                return DefaultTerrainValues.tileDesc( tile );
        }
    }
}
