package com.watabou.pixeldungeon.levels.terraintypes;

import com.watabou.pixeldungeon.Assets;
import com.watabou.pixeldungeon.levels.Terrain;

public class PrisonTerrain implements TerrainType {

    @Override
    public String tilesTexture() {
        return Assets.TILES_PRISON;
    }

    @Override
    public String waterTexture() {
        return Assets.WATER_PRISON;
    }

    @Override
    public int leafColor() {
        return 0x6a723d;
    }

    @Override
    public int leafColorAlternative() {
        return 0x88924c;
    }

    @Override
    public String tileName( Terrain tile ) {
        if (tile == Terrain.WATER) {
            return "Dark cold water.";
        }
        return DefaultTerrainValues.tileName(tile);
    }

    @Override
    public String tileDesc(Terrain tile) {
        switch (tile) {
            case EMPTY_DECO:
                return "There are old blood stains on the floor.";
            case BOOKSHELF:
                return "This is probably a vestige of a prison library. Might it burn?";
            default:
                return DefaultTerrainValues.tileDesc( tile );
        }
    }
}
