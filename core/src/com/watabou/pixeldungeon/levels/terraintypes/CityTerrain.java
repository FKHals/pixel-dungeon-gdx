package com.watabou.pixeldungeon.levels.terraintypes;

import com.watabou.pixeldungeon.Assets;
import com.watabou.pixeldungeon.levels.Terrain;

public class CityTerrain implements TerrainType {

    @Override
    public String tilesTexture() {
        return Assets.TILES_CITY;
    }

    @Override
    public String waterTexture() {
        return Assets.WATER_CITY;
    }

    @Override
    public int leafColor() {
        return 0x4b6636;
    }

    @Override
    public int leafColorAlternative() {
        return 0xf2f2f2;
    }

    @Override
    public String tileName( Terrain tile ) {
        switch (tile) {
            case WATER:
                return "Suspiciously colored water";
            case HIGH_GRASS:
                return "High blooming flowers";
            default:
                return DefaultTerrainValues.tileName( tile );
        }
    }

    @Override
    public String tileDesc(Terrain tile) {
        switch (tile) {
            case ENTRANCE:
                return "A ramp leads up to the upper depth.";
            case EXIT:
                return "A ramp leads down to the lower depth.";
            case WALL_DECO:
            case EMPTY_DECO:
                return "Several tiles are missing here.";
            case EMPTY_SP:
                return "Thick carpet covers the floor.";
            case STATUE:
            case STATUE_SP:
                return "The statue depicts some dwarf standing in a heroic stance.";
            case BOOKSHELF:
                return "The rows of books on different disciplines fill the bookshelf.";
            default:
                return DefaultTerrainValues.tileDesc( tile );
        }
    }
}
