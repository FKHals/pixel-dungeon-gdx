/*
 * Pixel Dungeon
 * Copyright (C) 2012-2014  Oleg Dolya
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.watabou.pixeldungeon.levels;

import com.watabou.noosa.Scene;
import com.watabou.noosa.particles.Emitter;
import com.watabou.pixeldungeon.Dungeon;
import com.watabou.pixeldungeon.DungeonTilemap;
import com.watabou.pixeldungeon.effects.Halo;
import com.watabou.pixeldungeon.effects.particles.FlameParticle;
import com.watabou.pixeldungeon.levels.Room.Type;
import com.watabou.pixeldungeon.levels.terraintypes.PrisonTerrain;
import com.watabou.utils.PointF;
import com.watabou.utils.Random;

public class PrisonLevel extends RegularLevel {

	{
		terrain = new PrisonTerrain();
	}
	
	protected boolean[] water(Random random) {
		return Patch.generate( feeling == Feeling.WATER ? 0.65f : 0.45f, 4, random);
	}
	
	protected boolean[] grass(Random random) {
		return Patch.generate( feeling == Feeling.GRASS ? 0.60f : 0.40f, 3, random);
	}
	
	@Override
	protected boolean assignRoomType(GenerationConstraints constraints) {
		boolean roomsAssigned = super.assignRoomType(constraints);

		if (super.assignRoomType(constraints)) {
			for (Room r : rooms) {
				if (r.type == Type.TUNNEL) {
					r.type = Type.PASSAGE;
				}
			}
		}
		return roomsAssigned;
	}

	@Override
	protected void decorate(GenerationConstraints constraints) {
		
		for (int i=WIDTH + 1; i < LENGTH - WIDTH - 1; i++) {
			if (getCell(i) == Terrain.EMPTY) {
				
				float c = 0.05f;
				if (getCell(i + 1) == Terrain.WALL && getCell(i + WIDTH) == Terrain.WALL) {
					c += 0.2f;
				}
				if (getCell(i - 1) == Terrain.WALL && getCell(i + WIDTH) == Terrain.WALL) {
					c += 0.2f;
				}
				if (getCell(i + 1) == Terrain.WALL && getCell(i - WIDTH) == Terrain.WALL) {
					c += 0.2f;
				}
				if (getCell(i - 1) == Terrain.WALL && getCell(i - WIDTH) == Terrain.WALL) {
					c += 0.2f;
				}
				
				if (constraints.random.nextFloat() < c) {
					setCell(i, Terrain.EMPTY_DECO);
				}
			}
		}
		
		for (int i=0; i < WIDTH; i++) {
			if (getCell(i) == Terrain.WALL &&
				(getCell(i + WIDTH) == Terrain.EMPTY || getCell(i + WIDTH) == Terrain.EMPTY_SP) &&
				constraints.random.nextInt( 6 ) == 0) {
				
				setCell(i, Terrain.WALL_DECO);
			}
		}
		
		for (int i=WIDTH; i < LENGTH - WIDTH; i++) {
			if (getCell(i) == Terrain.WALL &&
				getCell(i - WIDTH) == Terrain.WALL &&
				(getCell(i + WIDTH) == Terrain.EMPTY || getCell(i + WIDTH) == Terrain.EMPTY_SP) &&
				constraints.random.nextInt( 3 ) == 0) {
				
				setCell(i, Terrain.WALL_DECO);
			}
		}
		
		placeSign(constraints.random);
	}
	
	@Override
	public void addVisuals( Scene scene ) {
		super.addVisuals( scene );
		addVisuals( this, scene );
	}
	
	public static void addVisuals( Level level, Scene scene ) {
		for (int i=0; i < LENGTH; i++) {
			if (level.getCell(i) == Terrain.WALL_DECO) {
				scene.add( new Torch( i ) );
			}
		}
	}
	
	private static class Torch extends Emitter {
		
		private int pos;
		
		public Torch( int pos ) {
			super();
			
			this.pos = pos;
			
			PointF p = DungeonTilemap.tileCenterToWorld( pos );
			pos( p.x - 1, p.y + 3, 2, 0 );
			
			pour( FlameParticle.FACTORY, 0.15f );
			
			add( new Halo( 16, 0xFFFFCC, 0.2f ).point( p.x, p.y ) );
		}
		
		@Override
		public void update() {
			if (visible = Dungeon.visible[pos]) {
				super.update();
			}
		}
	}
}