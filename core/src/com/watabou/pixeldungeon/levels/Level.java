/*
 * Pixel Dungeon
 * Copyright (C) 2012-2014  Oleg Dolya
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.watabou.pixeldungeon.levels;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;

import com.watabou.noosa.Scene;
import com.watabou.noosa.audio.Sample;
import com.watabou.pixeldungeon.Assets;
import com.watabou.pixeldungeon.Challenges;
import com.watabou.pixeldungeon.Dungeon;
import com.watabou.pixeldungeon.Statistics;
import com.watabou.pixeldungeon.actors.Actor;
import com.watabou.pixeldungeon.actors.Char;
import com.watabou.pixeldungeon.actors.blobs.Blob;
import com.watabou.pixeldungeon.actors.buffs.Awareness;
import com.watabou.pixeldungeon.actors.buffs.Blindness;
import com.watabou.pixeldungeon.actors.buffs.Buff;
import com.watabou.pixeldungeon.actors.buffs.MindVision;
import com.watabou.pixeldungeon.actors.buffs.Shadows;
import com.watabou.pixeldungeon.actors.hero.Hero;
import com.watabou.pixeldungeon.actors.hero.HeroClass;
import com.watabou.pixeldungeon.actors.mobs.Bestiary;
import com.watabou.pixeldungeon.actors.mobs.Mob;
import com.watabou.pixeldungeon.effects.particles.FlowParticle;
import com.watabou.pixeldungeon.effects.particles.WindParticle;
import com.watabou.pixeldungeon.items.Generator;
import com.watabou.pixeldungeon.items.Gold;
import com.watabou.pixeldungeon.items.Heap;
import com.watabou.pixeldungeon.items.Item;
import com.watabou.pixeldungeon.items.armor.Armor;
import com.watabou.pixeldungeon.items.bags.ScrollHolder;
import com.watabou.pixeldungeon.items.bags.SeedPouch;
import com.watabou.pixeldungeon.items.food.Food;
import com.watabou.pixeldungeon.items.potions.PotionOfHealing;
import com.watabou.pixeldungeon.items.potions.PotionOfStrength;
import com.watabou.pixeldungeon.items.scrolls.Scroll;
import com.watabou.pixeldungeon.items.scrolls.ScrollOfEnchantment;
import com.watabou.pixeldungeon.items.scrolls.ScrollOfUpgrade;
import com.watabou.pixeldungeon.levels.painters.Painter;
import com.watabou.pixeldungeon.levels.terraintypes.TerrainType;
import com.watabou.pixeldungeon.mechanics.ShadowCaster;
import com.watabou.pixeldungeon.plants.Plant;
import com.watabou.pixeldungeon.quests.QuestEvent;
import com.watabou.pixeldungeon.quests.QuestManager;
import com.watabou.pixeldungeon.scenes.GameScene;
import com.watabou.utils.*;

public abstract class Level implements Bundlable, Field<Terrain> {

	public static enum Feeling {
		NONE,
		CHASM,
		WATER,
		GRASS
	};
	
	public static final int WIDTH = 32;
	public static final int HEIGHT = 32;
	public static final int LENGTH = WIDTH * HEIGHT;
	
	public static final int[] NEIGHBOURS4 = {-WIDTH, +1, +WIDTH, -1}; 
	public static final int[] NEIGHBOURS8 = {+1, -1, +WIDTH, -WIDTH, +1+WIDTH, +1-WIDTH, -1+WIDTH, -1-WIDTH};
	public static final int[] NEIGHBOURS9 = {0, +1, -1, +WIDTH, -WIDTH, +1+WIDTH, +1-WIDTH, -1+WIDTH, -1-WIDTH};
	
	protected static final float TIME_TO_RESPAWN	= 50;
	
	public static boolean resizingNeeded;
	// This one can be different from resizingNeeded if the level
	// was created in the older version of the game
	public static int loadedMapSize;
	
	private int[] map;
	public boolean[] visited;
	public boolean[] mapped;
	
	public int viewDistance = 8;
	
	public static boolean[] fieldOfView = new boolean[LENGTH];

	private static boolean[] passable = new boolean[LENGTH];
	private static boolean[] losBlocking = new boolean[LENGTH];
	private static boolean[] flamable = new boolean[LENGTH];
	private static boolean[] secret = new boolean[LENGTH];
	private static boolean[] solid = new boolean[LENGTH];
	private static boolean[] avoid = new boolean[LENGTH];
	private static boolean[] water = new boolean[LENGTH];
	private static boolean[] pit = new boolean[LENGTH];

	public static boolean[] discoverable = new boolean[LENGTH];
	
	public Feeling feeling = Feeling.NONE;
	
	public int entrance;
	public int exit;
	
	private MobSet mobs;
	public SparseArray<Heap> heaps;
	public HashMap<Class<? extends Blob>,Blob> blobs;
	private PlantMap plants;

	protected ArrayList<Item> itemsToSpawn = new ArrayList<Item>();

	public TerrainType terrain = null;
	
	protected static boolean pitRoomNeeded = false;
	protected static boolean weakFloorCreated = false;
	
	private static final String MAP			= "map";
	private static final String VISITED		= "visited";
	private static final String MAPPED		= "mapped";
	private static final String ENTRANCE	= "entrance";
	private static final String EXIT		= "exit";
	private static final String HEAPS		= "heaps";
	private static final String PLANTS		= "plants";
	private static final String MOBS		= "mobs";
	private static final String BLOBS		= "blobs";
	private static final String VIEWDISTANCE = "viewdistance";

	/**
	 * @return Internal integer representation of the map (should be allowed to be used by as few
	 * classes as possible since this breaks encapsulation)
	 */
	public int[] getRawMap() {
		return map;
	}

	public Terrain getCell(int cell) {
		return Terrain.valueOf(map[cell]);
	}

	public Terrain getCell(Point cell) {
		return Terrain.valueOf(map[toCell(cell)]);
	}

	protected void fillMap(Terrain value) {
		Arrays.fill(map, value.getValue());
	}

	public void fillMap(int fromIndex, int toIndex, Terrain value) {
		Arrays.fill(map, fromIndex, toIndex, value.getValue());
	}

	public void setCell(int cell, Terrain value) {
		map[cell] = value.getValue();
	}

	public void setCell(Point point, Terrain value) {
		map[toCell(point)] = value.getValue();
	}

	@Override
	public int width() {
		return WIDTH;
	}

	@Override
	public int height() {
		return HEIGHT;
	}

	@Override
	public int size() {
		return LENGTH;
	}

	public void create(GenerationConstraints constraints,
					   QuestManager questManager) {
		create(constraints, questManager, true);
	}

	public void create(GenerationConstraints constraints,
					   QuestManager questManager,
					   boolean createMobsAndItems) {

		resizingNeeded = false;
		
		map = new int[LENGTH];
		visited = new boolean[LENGTH];
		Arrays.fill( visited, false );
		mapped = new boolean[LENGTH];
		Arrays.fill( mapped, false );
		
		mobs = new MobSet();
		heaps = new SparseArray<Heap>();
		blobs = new HashMap<Class<? extends Blob>,Blob>();
		plants = new PlantMap();
		
		if (!constraints.isBossLevel) {
			addItemToSpawn( Generator.random( Generator.Category.FOOD ) );
			if (constraints.isPotionOfStrengthNeeded) {
				addItemToSpawn( new PotionOfStrength() );
			}
			if (constraints.isScrollsOfUpgradeNeeded) {
				addItemToSpawn( new ScrollOfUpgrade() );
			}
			if (constraints.isScrollsOfEnchantmentNeeded) {
				addItemToSpawn( new ScrollOfEnchantment() );
			}
			
			if (constraints.depth > 1) {
				switch (constraints.random.nextInt( 10 )) {
				case 0:
					if (!constraints.isNextLevelBossLevel) {
						feeling = Feeling.CHASM;
					}
					break;
				case 1:
					feeling = Feeling.WATER;
					break;
				case 2:
					feeling = Feeling.GRASS;
					break;
				}
			}
		}
		
		boolean pitNeeded = constraints.depth > 1 && weakFloorCreated;

		do {
			fillMap( feeling == Feeling.CHASM ? Terrain.CHASM : Terrain.WALL );
			
			pitRoomNeeded = pitNeeded;
			weakFloorCreated = false;

		} while (!build(constraints));
		decorate(constraints);
		
		buildFlagMaps();
		cleanWalls();

		// TODO Remove the createMobsAndItems-argument/flag and instead inject something like MobCreator- and
		//  ItemCreator-Objects that implement that behaviour
		if (createMobsAndItems) {
			createMobs(constraints);
			createItems(constraints);
		}
	}
	
	public void reset(GenerationConstraints constraints) {
		
		mobs.reset();
		createMobs(constraints);
	}
	
	@Override
	public void restoreFromBundle( Bundle bundle ) {

		mobs = (MobSet) bundle.get( MOBS );
		heaps = new SparseArray<Heap>();
		blobs = new HashMap<Class<? extends Blob>, Blob>();
		plants = (PlantMap) bundle.get( PLANTS );

		map = bundle.getIntArray( MAP );

		visited	= bundle.getBooleanArray( VISITED );
		mapped	= bundle.getBooleanArray( MAPPED );
		
		entrance	= bundle.getInt( ENTRANCE );
		exit		= bundle.getInt( EXIT );
		
		weakFloorCreated = false;
		
		adjustMapSize();
		
		Collection<Bundlable> collection = bundle.getCollection( HEAPS );
		for (Bundlable h : collection) {
			Heap heap = (Heap)h;
			if (resizingNeeded) {
				heap.pos = adjustPos( heap.pos );
			}
			heaps.put( heap.pos, heap );
		}
		
		collection = bundle.getCollection( BLOBS );
		for (Bundlable b : collection) {
			Blob blob = (Blob)b;
			blobs.put( blob.getClass(), blob );
		}

		if (resizingNeeded) {
			mobs.adjustPos( this );
			plants.adjustPos( this );
		}

		viewDistance = bundle.getInt(VIEWDISTANCE);
		
		buildFlagMaps();
		cleanWalls();
	}
	
	@Override
	public void storeInBundle( Bundle bundle ) {
		bundle.put( MAP, map );
		bundle.put( VISITED, visited );
		bundle.put( MAPPED, mapped );
		bundle.put( ENTRANCE, entrance );
		bundle.put( EXIT, exit );
		bundle.put( HEAPS, heaps.valuesAsList() );
		bundle.put( PLANTS, plants );
		bundle.put( MOBS, mobs );
		bundle.put( BLOBS, blobs.values() );
		bundle.put( VIEWDISTANCE, viewDistance );
	}

	public void setViewDistance(int viewDistance) {
		this.viewDistance = viewDistance;
	}

	public static boolean[] getPassable() {
		return passable;
	}

	public static boolean isPassable(int cell) {
		return passable[cell];
	}

	public static boolean[] getLosBlocking() {
		return losBlocking;
	}

	public static boolean isLosBlocking(int cell) {
		return losBlocking[cell];
	}

	public static boolean[] getFlamable() {
		return flamable;
	}

	public static boolean isFlamable(int cell) {
		return flamable[cell];
	}

	public static boolean[] getSecret() {
		return secret;
	}

	public static boolean isSecret(int cell) {
		return secret[cell];
	}

	public static boolean[] getSolid() {
		return solid;
	}

	public static boolean isSolid(int cell) {
		return solid[cell];
	}

	public static boolean[] getAvoided() {
		return avoid;
	}

	public static boolean isAvoided(int cell) {
		return avoid[cell];
	}

	public static boolean[] getWater() {
		return water;
	}

	public static boolean isWater(int cell) {
		return water[cell];
	}

	public static boolean[] getPit() {
		return pit;
	}

	public static boolean isPit(int cell) {
		return pit[cell];
	}

	public Collection<Mob> getMobs() {
		return mobs.getSet();
	}

	public void addMob(Mob mob) {
		mobs.add(mob);
	}

	public void removeMob(Mob mob) {
		mobs.remove(mob);
	}

	public int getMobCount() {
		return mobs.getCount();
	}

	public QuestEvent<Mob> getMobKillEvent() {
		return mobs.getKillEvent();
	}

	public Collection<Plant> getPlants() {
		return plants.getPlants();
	}

	public Plant getPlant( int cell ) {
		return plants.get(cell);
	}

	public void plant( Plant.Seed seed, int pos ) {
		plants.add(seed, pos, this);
	}

	public void removePlant(int pos ) {
		plants.remove( pos );
	}
	
	public Terrain tunnelTile() {
		return feeling == Feeling.CHASM ? Terrain.EMPTY_SP : Terrain.EMPTY;
	}
	
	private void adjustMapSize() {
		// For levels saved before 1.6.3
		if (map.length < LENGTH) {
			
			resizingNeeded = true;
			loadedMapSize = (int)Math.sqrt( map.length );

			int[] map = new int[LENGTH];
			Arrays.fill( map, Terrain.WALL.getValue() );
			
			boolean[] visited = new boolean[LENGTH];
			Arrays.fill( visited, false );
			
			boolean[] mapped = new boolean[LENGTH];
			Arrays.fill( mapped, false );
			
			for (int i=0; i < loadedMapSize; i++) {
				System.arraycopy( this.map, i * loadedMapSize, map, i * WIDTH, loadedMapSize );
				System.arraycopy( this.visited, i * loadedMapSize, visited, i * WIDTH, loadedMapSize );
				System.arraycopy( this.mapped, i * loadedMapSize, mapped, i * WIDTH, loadedMapSize );
			}
			
			this.map = map;
			this.visited = visited;
			this.mapped = mapped;
			
			entrance = adjustPos( entrance );
			exit = adjustPos( exit ); 
		} else {
			resizingNeeded = false;
		}
	}
	
	public int adjustPos( int pos ) {
		return (pos / loadedMapSize) * WIDTH + (pos % loadedMapSize);
	}
	
	public String tilesTex() {
		return terrain.tilesTexture();
	}
	
	public String waterTex() {
		return terrain.waterTexture();
	}

	public int leafColor() {
		return terrain.leafColor();
	}

	public int leafColorAlternative() {
		return terrain.leafColorAlternative();
	}
	
	abstract protected boolean build(GenerationConstraints constraints);
	abstract protected void decorate(GenerationConstraints constraints);
	abstract protected void createMobs(GenerationConstraints constraints);
	abstract protected void createItems(GenerationConstraints constraints);
	
	public void addVisuals( Scene scene ) {
		for (int i=0; i < LENGTH; i++) {
			if (pit[i]) {
				scene.add( new WindParticle.Wind( i ) );
				if (i >= WIDTH && water[i-WIDTH]) {
					scene.add( new FlowParticle.Flow( i - WIDTH ) );
				}
			}
		}
	}
	
	public int nMobs(int depth) {
		return 0;
	}
	
	public Actor respawner() {
		return new Actor() {	
			@Override
			protected boolean act() {
				if (mobs.getCount() < nMobs(Dungeon.depth)) {

					Mob mob = Bestiary.mutable( Dungeon.depth );
					mob.state = mob.WANDERING;
					mob.pos = randomRespawnCell();
					if (Dungeon.hero.isAlive() && mob.pos != -1) {
						GameScene.add( mob );
						if (Statistics.amuletObtained) {
							mob.beckon( Dungeon.hero.pos );
						}
					}
				}
				spend( Dungeon.nightMode || Statistics.amuletObtained ? TIME_TO_RESPAWN / 2 : TIME_TO_RESPAWN );
				return true;
			}
		};
	}
	
	public int randomRespawnCell() {
		int cell;
		do {
			cell = Random.Int( LENGTH );
		} while (!passable[cell] || Dungeon.visible[cell] || Actor.findChar( cell ) != null);
		return cell;
	}
	
	public int randomDestination() {
		int cell;
		do {
			cell = Random.Int( LENGTH );
		} while (!passable[cell]);
		return cell;
	}
	
	public void addItemToSpawn( Item item ) {
		if (item != null) {
			itemsToSpawn.add( item );
		}
	}
	
	public Item itemToSpanAsPrize() {
		if (Random.Int( itemsToSpawn.size() + 1 ) > 0) {
			Item item = Random.element( itemsToSpawn );
			itemsToSpawn.remove( item );
			return item;
		} else {
			return null;
		}
	}

	private void updateFlags(int cell) {
		Terrain t = getCell(cell);
		passable[cell] = t.isPassable();
		losBlocking[cell] = t.isLosBlocking();
		flamable[cell] = t.isFlammable();
		secret[cell] = t.isSecret();
		solid[cell] = t.isSolid();
		avoid[cell] = t.isAvoided();
		water[cell] = t.isLiquid();
		pit[cell] = t.isPit();
	}
	
	void buildFlagMaps() {
		
		for (int i=0; i < LENGTH; i++) {
			updateFlags(i);
		}
		
		int lastRow = LENGTH - WIDTH;
		for (int i=0; i < WIDTH; i++) {
			passable[i] = avoid[i] = false;
			passable[lastRow + i] = avoid[lastRow + i] = false;
		}
		for (int i=WIDTH; i < lastRow; i += WIDTH) {
			passable[i] = avoid[i] = false;
			passable[i + WIDTH-1] = avoid[i + WIDTH-1] = false;
		}
		 
		for (int i=WIDTH; i < LENGTH - WIDTH; i++) {
			
			if (water[i]) {
				setCell(i, getWaterTile( i ));
			}
			
			if (pit[i]) {
				if (!pit[i - WIDTH]) {
					Terrain c = getCell(i - WIDTH);
					if (c == Terrain.EMPTY_SP || c == Terrain.STATUE_SP) {
						setCell(i, Terrain.CHASM_FLOOR_SP);
					} else if (water[i - WIDTH]) {
						setCell(i, Terrain.CHASM_WATER);
					} else if (c.isUnstitchable()) {
						setCell(i, Terrain.CHASM_WALL);
					} else {
						setCell(i, Terrain.CHASM_FLOOR);
					}
				}
			}
		}
	}

	private Terrain getWaterTile(int pos ) {
		int t = Terrain.WATER_TILE_0.getValue();
		for (int j=0; j < NEIGHBOURS4.length; j++) {
			if (getCell(pos + NEIGHBOURS4[j]).isUnstitchable()) {
				t += 1 << j;
			}
		}
		return Terrain.valueOf(t);
	}

	public void destroy( int pos ) {
		if (!getCell(pos).isUnstitchable()) {

			set( pos, Terrain.EMBERS );

		} else {
			boolean flood = false;
			for (int neighbourOffset: NEIGHBOURS4) {
				if (water[pos + neighbourOffset]) {
					flood = true;
					break;
				}
			}
			if (flood) {
				set( pos, getWaterTile( pos ) );
			} else {
				set( pos, Terrain.EMBERS );
			}
		}
	}

	void cleanWalls() {
		for (int cell=0; cell < LENGTH; cell++) {
			
			boolean isDiscoverable = false;
			
			for (int neighbourOffset: NEIGHBOURS9) {
				int neighbourCell = cell + neighbourOffset;
				if (neighbourCell >= 0
						&& neighbourCell < LENGTH
						&& getCell(neighbourCell) != Terrain.WALL
						&& getCell(neighbourCell) != Terrain.WALL_DECO) {
					isDiscoverable = true;
					break;
				}
			}
			
			if (isDiscoverable) {
				isDiscoverable = false;
				
				for (int neighbourOffset: NEIGHBOURS9) {
					int neighbourCell = cell + neighbourOffset;
					if (neighbourCell >= 0
							&& neighbourCell < LENGTH
							&& !pit[neighbourCell]) {
						isDiscoverable = true;
						break;
					}
				}
			}
			
			discoverable[cell] = isDiscoverable;
		}
	}
	
	public static void set( int cell, Terrain terrain ) {
		Painter.set( Dungeon.level, cell, terrain );
		Dungeon.level.updateFlags(cell);
	}
	
	public Heap drop( Item item, int cell ) {

		if (Dungeon.isChallenged( Challenges.NO_FOOD ) && item instanceof Food) {
			item = new Gold( item.price() );
		} else
		if (Dungeon.isChallenged( Challenges.NO_ARMOR ) && item instanceof Armor) {
			item = new Gold( item.price() );
		} else
		if (Dungeon.isChallenged( Challenges.NO_HEALING ) && item instanceof PotionOfHealing) {
			item = new Gold( item.price() );
		} else
		if (Dungeon.isChallenged( Challenges.NO_HERBALISM ) && item instanceof SeedPouch) {
			item = new Gold( item.price() );
		} else
		if (Dungeon.isChallenged( Challenges.NO_SCROLLS ) && (item instanceof Scroll || item instanceof ScrollHolder)) {
			if (item instanceof ScrollOfUpgrade) {
				// These scrolls still can be found
			} else {
				item = new Gold( item.price() );
			}
		}
		
		if ((getCell(cell) == Terrain.ALCHEMY) && !(item instanceof Plant.Seed)) {
			int n;
			do {
				n = cell + NEIGHBOURS8[Random.Int( 8 )];
			} while (getCell(n) != Terrain.EMPTY_SP);
			cell = n;
		}
		
		Heap heap = heaps.get( cell );
		if (heap == null) {
			
			heap = new Heap();
			heap.pos = cell;
			if (getCell(cell) == Terrain.CHASM || (Dungeon.level != null && pit[cell])) {
				Dungeon.dropToChasm( item );
				GameScene.discard( heap );
			} else {
				heaps.put( cell, heap );
				GameScene.add( heap );
			}
			
		} else if (heap.type == Heap.Type.LOCKED_CHEST || heap.type == Heap.Type.CRYSTAL_CHEST) {
			
			int n;
			do {
				n = cell + Level.NEIGHBOURS8[Random.Int( 8 )];
			} while (!Level.passable[n] && !Level.avoid[n]);
			return drop( item, n );
			
		}
		heap.drop( item );
		
		if (Dungeon.level != null) {
			press( cell, null );
		}
				
		return heap;
	}

	/**
	 * Drops a specific item in a specific room at a position with certain features.
	 * @param item That gets dropped into the room
	 * @param heapType Type of heap that the item should appear as
	 * @param room That the item gets dropped into
	 * @param random Random object that makes seeding possible
	 * @param positionRequirement Filters the possible positions that the item can be dropped at
	 */
	public void dropInRoom(
			Item item,
			Heap.Type heapType,
			Room room,
			Random random,
			Predicate<Integer> positionRequirement
	) {
		Optional<Integer> pos = room.random(positionRequirement, this, random);
		if (pos.isPresent()) {
			drop( item, pos.get() ).type = heapType;
		} else {
			DLog.error("Level", "Could not drop '" + item.name() + "' into " + room.type);
			// TODO Throw exception and regenerate level?!
		}
	}

	public void dropInRoom(
			Item item,
			Room room,
			Random random,
			Predicate<Integer> positionRequirement
	) {
		dropInRoom(item, Heap.Type.HEAP, room, random, positionRequirement);
	}

	/**
	 * Drops multiple items of a certain kind in a specific room at positions with certain features.
	 * It generally picks different positions for different objects as long as the number of
	 * possible positions is at least as large as the number of object to drop.
	 */
	public void dropMultipleInRoom(
			ArrayList<Item> items,
			ArrayList<Heap.Type> heapTypes,
			Room room,
			Random random,
			Predicate<Integer> positionRequirement
	) {
		Optional<ArrayList<Integer>> optPositions =
				room.multipleDistinctRandom( items.size(), positionRequirement, this, random);
		if (optPositions.isPresent() && !optPositions.get().isEmpty()) {
			ArrayList<Integer> positions = optPositions.get();
			for (int i=0; i < items.size(); i++) {
				// if less positions are available then there are items then drop multiple items on
				// the same position TODO Do we really want that?!
				drop(items.get(i), positions.get(i % positions.size()) ).type = heapTypes.get(i);
			}
		} else {
			DLog.error("Level", "Could not drop " + items.size() + " Items (" + items +
					") into " + room.type);
			// TODO Throw exception and regenerate level?!
		}
	}

	public void dropMultipleInRoom(
			ArrayList<Item> items,
			Room room,
			Random random,
			Predicate<Integer> positionRequirement
	) {
		ArrayList<Heap.Type> heapTypes = new ArrayList<>();
		for (int i=0; i < items.size(); i++) {
			heapTypes.add( Heap.Type.HEAP );
		}
		dropMultipleInRoom(items, heapTypes, room, random, positionRequirement);
	}

	/**
	 * Drop multiple items at the same spot (in the same heap) in the room
	 * @param heapType The type of heap that the items are stored in
	 */
	public void dropMultipleAtSameSpotInRoom(
			ArrayList<Item> items,
			Heap.Type heapType,
			Room room,
			Random random,
			Predicate<Integer> positionRequirement
	) {
		Optional<Integer> pos = room.random(positionRequirement, this, random);
		if (pos.isPresent()) {
			for (Item item: items) {
				drop( item, pos.get() ).type = heapType;
			}
		} else {
			DLog.error("Level", "Could not drop '" + items.toString() +
					"' into " + room.type);
			// TODO Throw exception and regenerate level?!
		}
	}

	public void placeInRoom(
			Mob mob,
			Room room,
			Random random,
			Predicate<Integer> positionRequirement
	) {
		Optional<Integer> pos = room.random(positionRequirement, this, random);
		// TODO Add requirement:
		//  Actor.findChar( cell ) != null && Level.isPassable(cell)
		if (pos.isPresent()) {
			mob.pos = pos.get();
			addMob( mob );
		} else {
			DLog.error("Level", "Could not place '" + mob.name + "' in " + room.type);
			// TODO Throw exception and regenerate level?!
		}
	}
	
	public int pitCell() {
		return randomRespawnCell();
	}
	
	public void press( int cell, Char ch ) {

		boolean isTrap = getCell(cell).press(this, cell, ch);

		if (isTrap) {
			// players should not hear the trap if it is triggered by a monster out of (visibility) range
			if (ch == Dungeon.hero || Dungeon.visible[cell]) {
				Sample.INSTANCE.play( Assets.SND_TRAP );
			}
			if (ch == Dungeon.hero) {
				Dungeon.hero.interrupt();
			}
			set( cell, Terrain.INACTIVE_TRAP );
			GameScene.updateMap( cell );
		}
		
		Plant plant = plants.get( cell );
		if (plant != null) {
			plant.activate( ch, this );
		}
	}
	
	public void mobPress( Mob mob ) {
		press(mob.pos, mob);
	}
	
	public boolean[] updateFieldOfView( Char c ) {
		
		int cx = c.pos % WIDTH;
		int cy = c.pos / WIDTH;
		
		boolean sighted = c.buff( Blindness.class ) == null && c.buff( Shadows.class ) == null && c.isAlive();
		if (sighted) {
			ShadowCaster.castShadow( cx, cy, fieldOfView, c.viewDistance );
		} else {
			Arrays.fill( fieldOfView, false );
		}
		
		int sense = 1;
		if (c.isAlive()) {
			for (Buff b : c.buffs( MindVision.class )) {
				sense = Math.max( ((MindVision)b).distance, sense );
			}
		}
		
		if ((sighted && sense > 1) || !sighted) {
			
			int ax = Math.max( 0, cx - sense );
			int bx = Math.min( cx + sense, WIDTH - 1 );
			int ay = Math.max( 0, cy - sense );
			int by = Math.min( cy + sense, HEIGHT - 1 );

			int len = bx - ax + 1;
			int pos = ax + ay * WIDTH;
			for (int y = ay; y <= by; y++, pos+=WIDTH) {
				Arrays.fill( fieldOfView, pos, pos + len, true );
			}
			
			for (int i=0; i < LENGTH; i++) {
				fieldOfView[i] &= discoverable[i];
			}
		}
		
		if (c.isAlive()) {
			if (c.buff( MindVision.class ) != null) {
				for (Mob mob : mobs.getSet()) {
					int p = mob.pos;
					for (int neighbourOffset: NEIGHBOURS9) {
						fieldOfView[p + neighbourOffset] = true;
					}
				}
			} else if (c == Dungeon.hero && ((Hero)c).heroClass == HeroClass.HUNTRESS) {
				for (Mob mob : mobs.getSet()) {
					int p = mob.pos;
					if (distance( c.pos, p) == 2) {
						for (int neighbourOffset: NEIGHBOURS9) {
							fieldOfView[p + neighbourOffset] = true;
						}
					}
				}
			}
			if (c.buff( Awareness.class ) != null) {
				for (Heap heap : heaps.values()) {
					int p = heap.pos;
					for (int neighbourOffset: NEIGHBOURS9) {
						fieldOfView[p + neighbourOffset] = true;
					}
				}
			}
		}
		
		return fieldOfView;
	}
	
	public static int distance( int a, int b ) {
		int ax = a % WIDTH;
		int ay = a / WIDTH;
		int bx = b % WIDTH;
		int by = b / WIDTH;
		return Math.max( Math.abs( ax - bx ), Math.abs( ay - by ) );
	}
	
	public static boolean adjacent( int a, int b ) {
		int diff = Math.abs( a - b );
		return diff == 1 || diff == WIDTH || diff == WIDTH + 1 || diff == WIDTH - 1;
	}
	
	public String tileName( Terrain tile ) {
		return terrain.tileName(tile);
	}
	
	public String tileDesc( Terrain tile ) {
		return terrain.tileDesc(tile);
	}

	public int toCell(Point p) {
		return p.x + p.y * WIDTH;
	}

	/**
	 * A data container that holds all the required properties and constraints relevant to
	 * generating levels
	 */
	public static class GenerationConstraints {
		public int depth;
		public boolean isBossLevel;
		public boolean isNextLevelBossLevel;
		public boolean isPotionOfStrengthNeeded;
		public boolean isScrollsOfUpgradeNeeded;
		public boolean isScrollsOfEnchantmentNeeded;
		public boolean shopOnLevel;
		public boolean isImpQuestCompleted;
		public LinkedList<Room.Type> roomsToSpawn;
		public Random random;
		ArrayList<Room.Type> specialRooms;

		public GenerationConstraints(int depth,
									 boolean isBossLevel,
									 boolean isNextLevelBossLevel,
									 boolean isPotionOfStrengthNeeded,
									 boolean isScrollsOfUpgradeNeeded,
									 boolean isScrollsOfEnchantmentNeeded,
									 boolean shopOnLevel,
									 boolean isImpQuestCompleted,
									 Random random,
									 ArrayList<Room.Type> specialRooms) {
			this.depth = depth;
			this.isBossLevel = isBossLevel;
			this.isNextLevelBossLevel = isNextLevelBossLevel;
			this.isPotionOfStrengthNeeded = isPotionOfStrengthNeeded;
			this.isScrollsOfUpgradeNeeded = isScrollsOfUpgradeNeeded;
			this.isScrollsOfEnchantmentNeeded = isScrollsOfEnchantmentNeeded;
			this.shopOnLevel = shopOnLevel;
			this.isImpQuestCompleted = isImpQuestCompleted;
			this.roomsToSpawn = new LinkedList<>();
			this.random = random;
			this.specialRooms = new ArrayList<>(specialRooms);
		}
	}
}
