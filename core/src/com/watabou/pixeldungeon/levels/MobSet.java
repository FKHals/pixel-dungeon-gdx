package com.watabou.pixeldungeon.levels;

import com.watabou.pixeldungeon.actors.mobs.Mob;
import com.watabou.pixeldungeon.quests.QuestEvent;
import com.watabou.utils.Bundlable;
import com.watabou.utils.Bundle;
import com.watabou.utils.DLog;

import java.util.Collection;
import java.util.HashSet;

public class MobSet implements Bundlable {

    private HashSet<Mob> mobs;
    private QuestEvent<Mob> killEvent;

    public MobSet() {
        mobs = new HashSet<>();
        killEvent = new QuestEvent<>();
    }

    private static final String MOBS = "mobs";
    private static final String KILLEVENT = "killevent";

    @Override
    public void restoreFromBundle( Bundle bundle ) {
        mobs = new HashSet<Mob>();
        Collection<Bundlable> collection = bundle.getCollection( MOBS );
        for (Bundlable m : collection) {
            Mob mob = (Mob)m;
            if (mob != null) {
                mobs.add( mob );
            }
        }
        killEvent = (QuestEvent<Mob>) bundle.get( KILLEVENT );
        DLog.debug("MOBSET", "The following mobs have been loaded: " + mobs);
    }

    @Override
    public void storeInBundle( Bundle bundle ) {
        bundle.put( MOBS, mobs );
        bundle.put( KILLEVENT, killEvent );
    }

    public Collection<Mob> getSet() {
        return mobs;
    }

    public void add(Mob mob) {
        mobs.add(mob);
    }

    public void remove(Mob mob) {
        DLog.debug("MOBSET", "Mob died: " + mob);
        mobs.remove(mob);
        killEvent.notifyObservers(mob);
    }

    public int getCount() {
        return mobs.size();
    }

    /** Adjusts the position of the mobs if the level dimensions have changed
     */
    public void adjustPos(Level level) {
        for (Mob mob : mobs) {
            if (mob != null) {
                mob.pos = level.adjustPos( mob.pos );
            }
        }
    }

    public void reset() {
        for (Mob mob : mobs) {
            if (!mob.reset()) {
                mobs.remove( mob );
            }
        }
    }

    public QuestEvent<Mob> getKillEvent() {
        return killEvent;
    }
}
