/*
 * Pixel Dungeon
 * Copyright (C) 2012-2014  Oleg Dolya
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.watabou.pixeldungeon.levels;

import java.util.Arrays;

import com.watabou.noosa.Scene;
import com.watabou.pixeldungeon.items.Amulet;
import com.watabou.pixeldungeon.levels.painters.Painter;
import com.watabou.pixeldungeon.levels.terraintypes.HallsTerrain;
import com.watabou.utils.Random;

public class LastLevel extends Level {

	private static final int SIZE = 7;
	
	{
		terrain = new HallsTerrain();
	}
	
	private int pedestal;

	@Override
	protected boolean build(GenerationConstraints constraints) {

		fillMap(Terrain.WALL );
		Painter.fill( this, 1, 1, SIZE, SIZE, Terrain.WATER );
		Painter.fill( this, 2, 2, SIZE-2, SIZE-2, Terrain.EMPTY );
		Painter.fill( this, SIZE/2, SIZE/2, 3, 3, Terrain.EMPTY_SP );
		
		entrance = SIZE * WIDTH + SIZE / 2 + 1;
		setCell(entrance, Terrain.ENTRANCE);
		
		exit = entrance - WIDTH * SIZE;
		setCell(exit, Terrain.LOCKED_EXIT);
		
		pedestal = (SIZE / 2 + 1) * (WIDTH + 1);
		setCell(pedestal, Terrain.PEDESTAL);
		setCell(pedestal-1, Terrain.STATUE_SP);
		setCell(pedestal+1, Terrain.STATUE_SP);
		
		feeling = Feeling.NONE;
		
		return true;
	}

	@Override
	protected void decorate(GenerationConstraints constraints) {
		for (int i=0; i < LENGTH; i++) {
			if (getCell(i) == Terrain.EMPTY && constraints.random.nextInt( 10 ) == 0) {
				setCell(i, Terrain.EMPTY_DECO);
			}
		}
	}

	@Override
	protected void createMobs(GenerationConstraints constraints) {
	}

	@Override
	protected void createItems(GenerationConstraints constraints) {
		drop( new Amulet(), pedestal );
	}
	
	@Override
	public int randomRespawnCell() {
		return -1;
	}

	@Override
	public void addVisuals( Scene scene ) {
		HallsLevel.addVisuals( this, scene );
	}
}
