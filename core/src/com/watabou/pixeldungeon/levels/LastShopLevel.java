/*
 * Pixel Dungeon
 * Copyright (C) 2012-2014  Oleg Dolya
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.watabou.pixeldungeon.levels;

import java.util.List;

import com.watabou.noosa.Scene;
import com.watabou.pixeldungeon.Bones;
import com.watabou.pixeldungeon.actors.Actor;
import com.watabou.pixeldungeon.items.Heap;
import com.watabou.pixeldungeon.items.Item;
import com.watabou.pixeldungeon.levels.Room.Type;
import com.watabou.pixeldungeon.levels.terraintypes.CityTerrain;
import com.watabou.utils.*;

public class LastShopLevel extends RegularLevel {
	
	{
		terrain = new CityTerrain();
	}
	
	@Override
	protected boolean build(GenerationConstraints constraints) {
		
		initRooms(constraints.random);
		
		int distance;
		int retry = 0;
		int minDistance = (int)Math.sqrt( rooms.size() );
		do {
			int innerRetry = 0;
			do {
				if (innerRetry++ > 10) {
					return false;
				}
				roomEntrance = constraints.random.nextElement( rooms );
			} while (roomEntrance.width() < 4 || roomEntrance.height() < 4);
			
			innerRetry = 0;
			do {
				if (innerRetry++ > 10) {
					return false;
				}
				roomExit = constraints.random.nextElement( rooms );
			} while (roomExit == roomEntrance || roomExit.width() < 6 || roomExit.height() < 6 || roomExit.top == 0);
	
			Graph.buildDistanceMap( rooms, roomExit );
			distance = Graph.buildPath( rooms, roomEntrance, roomExit ).size();
			
			if (retry++ > 10) {
				return false;
			}
			
		} while (distance < minDistance);
		
		roomEntrance.type = Type.ENTRANCE;
		roomExit.type = Type.EXIT;
		
		Graph.buildDistanceMap( rooms, roomExit );
		List<Room> path = Graph.buildPath( rooms, roomEntrance, roomExit );
		
		Graph.setPrice( path, roomEntrance.distance );
		
		Graph.buildDistanceMap( rooms, roomExit );
		path = Graph.buildPath( rooms, roomEntrance, roomExit );
		
		Room room = roomEntrance;
		for (Room next : path) {
			room.connect( next );
			room = next;
		}
		
		Room roomShop = null;
		int shopSquare = 0;
		for (Room r : rooms) {
			if (r.type == Type.NULL && r.connected.size() > 0) {
				r.type = Type.PASSAGE; 
				if (r.square() > shopSquare) {
					roomShop = r;
					shopSquare = r.square();
				}
			}
		}
		
		if (roomShop == null || shopSquare < 30) {
			return false;
		} else {
			roomShop.type = constraints.isImpQuestCompleted ? Room.Type.SHOP : Room.Type.STANDARD;
		}
		
		paint(constraints);
		
		paintWater(constraints.random);
		paintGrass(constraints.random);
		
		return true;
	}
	
	@Override
	protected void decorate(GenerationConstraints constraints) {
		
		for (int i=0; i < LENGTH; i++) {
			if (getCell(i) == Terrain.EMPTY && constraints.random.nextInt( 10 ) == 0) {
				
				setCell(i, Terrain.EMPTY_DECO);
				
			} else if (getCell(i) == Terrain.WALL && constraints.random.nextInt( 8 ) == 0) {
				
				setCell(i, Terrain.WALL_DECO);
				
			} else if (getCell(i) == Terrain.SECRET_DOOR) {
				
				setCell(i, Terrain.DOOR);
				
			}
		}
		
		if (constraints.isImpQuestCompleted) {
			placeSign(constraints.random);
		}
	}
	
	@Override
	protected void createMobs(GenerationConstraints constraints) {
	}
	
	public Actor respawner() {
		return null;
	}
	
	@Override
	protected void createItems(GenerationConstraints constraints) {
		Item item = Bones.get();
		if (item != null) {
			dropInRoom(item, Heap.Type.SKELETON, roomEntrance, new Random(), new Predicate<Integer>() {
				@Override
				public boolean test(Integer pos) {
					return (pos != entrance && getCell(pos) != Terrain.SIGN);
				}
			});
		}
	}
	
	@Override
	public int randomRespawnCell() {
		return -1;
	}

	@Override
	protected boolean[] water(Random random) {
		return Patch.generate( 0.35f, 4, random);
	}

	@Override
	protected boolean[] grass(Random random) {
		return Patch.generate( 0.30f, 3, random);
	}
	
	@Override
	public void addVisuals( Scene scene ) {
		CityLevel.addVisuals( this, scene );
	}
}
