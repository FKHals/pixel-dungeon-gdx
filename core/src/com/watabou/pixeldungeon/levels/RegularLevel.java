/*
 * Pixel Dungeon
 * Copyright (C) 2012-2014  Oleg Dolya
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.watabou.pixeldungeon.levels;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.LinkedList;
import java.util.LinkedHashSet;

import com.watabou.pixeldungeon.Bones;
import com.watabou.pixeldungeon.Dungeon;
import com.watabou.pixeldungeon.actors.Actor;
import com.watabou.pixeldungeon.actors.mobs.Bestiary;
import com.watabou.pixeldungeon.actors.mobs.Mob;
import com.watabou.pixeldungeon.items.Generator;
import com.watabou.pixeldungeon.items.Heap;
import com.watabou.pixeldungeon.items.Item;
import com.watabou.pixeldungeon.items.scrolls.ScrollOfUpgrade;
import com.watabou.pixeldungeon.levels.Room.Type;
import com.watabou.pixeldungeon.levels.painters.*;
import com.watabou.pixeldungeon.quests.QuestManager;
import com.watabou.utils.*;

public abstract class RegularLevel extends Level {

	// this must be an ordered data structure (instead of just a HashSet) so that randomized operations (e.g. sampling
	// elements) is deterministic according to the seed which is necessary for implementing dungeon seeded levels
	protected LinkedHashSet<Room> rooms;
	
	protected Room roomEntrance;
	protected Room roomExit;
	
	protected ArrayList<Room.Type> specials;
	public ArrayList<Room.Type> usedSpecialRooms = new ArrayList<>();
	
	public int secretDoors;

	public Room getEntranceRoom() {
		return roomEntrance;
	}

	@Override
	public void create(GenerationConstraints constraints,
					   QuestManager questManager) {

		questManager.initQuest(this, constraints.depth);
		constraints.roomsToSpawn.addAll(questManager.getRoomsToSpawn(constraints.depth));

		super.create(constraints, questManager);

		questManager.spawnQuests(this, constraints.depth);
	}

	/**
	 *  1. Split level recursively in rectangles (> 8) that become rooms
	 *  2. Take a random room (< 4x4) as entrance room
	 *  3. Take a random room (< 4x4) as exit room
	 *  4. Calculate the shortest path (over the room-graph) between them
	 *  5. go to 2. if path too short else
	 *  6. Add all rooms on the shortest way between entrance and exit to the list
	 *  7. Add a penalty to already added rooms
	 *  8. Find a second path between the entrance and exit that is should use mostly other rooms
	 *     than the first because of the penalty and add the new rooms on that path (which creates
	 *     a circle in most cases).
	 *  9. Choose a random room of the already added rooms and add a neighbour that has not yet been
	 *     added (repeat a arbitrary number of times)
	 * 10. Assign the required rooms (shop, quests,..) to one of the added rooms that meets the
	 *     requirements
	 * 11. see {@link #assignRoomType(GenerationConstraints)}
	 * ...
	 * @return
	 */
	@Override
	protected boolean build(GenerationConstraints constraints) {

		if (!initRooms(constraints.random)) {
			return false;
		}
	
		int distance;
		int retry = 0;
		int minDistance = (int)Math.sqrt( rooms.size() );
		do {
			do {
				// FIXME The problem is simply that the order of the elements in `rooms` is not guaranteed! Use LinkedHashSet instead!!!
				roomEntrance = constraints.random.nextElement( rooms );
			} while (roomEntrance.width() < 4 || roomEntrance.height() < 4);
			
			do {
				roomExit = constraints.random.nextElement( rooms );
			} while (roomExit == roomEntrance || roomExit.width() < 4 || roomExit.height() < 4);
	
			Graph.buildDistanceMap( rooms, roomExit );
			distance = roomEntrance.distance();
			
			if (retry++ > 10) {
				return false;
			}
			
		} while (distance < minDistance);
		
		roomEntrance.type = Type.ENTRANCE;
		roomExit.type = Type.EXIT;
		
		LinkedHashSet<Room> connected = new LinkedHashSet<Room>();
		connected.add( roomEntrance );
		
		Graph.buildDistanceMap( rooms, roomExit );
		List<Room> path = Graph.buildPath( rooms, roomEntrance, roomExit );
		
		Room room = roomEntrance;
		for (Room next : path) {
			room.connect( next );
			room = next;
			connected.add( room );
		}
		
		Graph.setPrice( path, roomEntrance.distance );
		
		Graph.buildDistanceMap( rooms, roomExit );
		path = Graph.buildPath( rooms, roomEntrance, roomExit );
		
		room = roomEntrance;
		for (Room next : path) {
			room.connect( next );
			room = next;
			connected.add( room );
		}
		
		int nConnected = (int)(rooms.size() * constraints.random.nextFloat( 0.5f, 0.7f ));
		while (connected.size() < nConnected) {

			Room cr = constraints.random.nextElement( connected );
			Room or = constraints.random.nextElement( cr.neigbours );
			if (!connected.contains( or )) {

				cr.connect( or );
				connected.add( or );
			}
		}
		
		specials = constraints.specialRooms;
		if (constraints.isNextLevelBossLevel) {
			specials.remove( Room.Type.WEAK_FLOOR );
		}
		if (!assignRoomType(constraints)) {
			return false;
		}
		
		paint(constraints);
		paintWater(constraints.random);
		paintGrass(constraints.random);
		
		placeTraps(constraints);
		
		return true;
	}

	/**
	 * Initializes the rooms by splitting the map into rooms and connecting them.
	 * @return If the rooms have properly been initialized (if enough rooms were created)
	 */
	protected boolean initRooms(Random random) {

		rooms = new LinkedHashSet<Room>();
		split( new Rect( 0, 0, WIDTH - 1, HEIGHT - 1 ), random);

		if (rooms.size() < 8) {
			return false;
		}

		Room[] ra = rooms.toArray( new Room[0] );
		for (int i=0; i < ra.length-1; i++) {
			for (int j=i+1; j < ra.length; j++) {
				ra[i].addNeighbour( ra[j] );
			}
		}
		
		return true;
	}

	/**
	 * Assigns all currently not assigned rooms a certain Room Type.
	 * Special rooms only get created with single connections (dead-end) while rooms with many
	 * connections become tunnels and the rest standard rooms.
	 * @return If the assignment matched all the requirements
	 */
	protected boolean assignRoomType(GenerationConstraints constraints) {

		// avoid using the roomsToSpawn in constraints since this method would modify that value
		// and thereby create unwanted side effects (since build() may be called multiple times)
		LinkedList<Type> roomsToSpawn = new LinkedList<>(constraints.roomsToSpawn);

		if (constraints.shopOnLevel) {
			roomsToSpawn.add(Room.Type.SHOP);
		}
		// first pass that generates all rooms that are required (in contrast to special rooms)
		for (Room.Type roomType: roomsToSpawn) {
			Room roomToSpawn = null;
			for (Room potentialRoom : rooms) {
				if (roomType.areRequirementsMetBy(potentialRoom, this)) {
					roomToSpawn = potentialRoom;
					break;
				}
			}
			if (roomToSpawn == null) {
				return false;
			} else {
				roomToSpawn.type = roomType;
			}
		}

		int specialRooms = 0;

		// Use some (or all) of the rooms with only one connection (only one door) to the other
		// rooms to (possibly) make them special rooms
		for (Room r : rooms) {
			if (r.type == Type.NULL && 
				r.connected.size() == 1) {

				if (specials.size() > 0 &&
					r.width() > 3 && r.height() > 3 &&
						constraints.random.nextInt( specialRooms * specialRooms + 2 ) == 0) {

					if (pitRoomNeeded) {

						r.type = Type.PIT;
						pitRoomNeeded = false;

						specials.remove( Type.ARMORY );
						specials.remove( Type.CRYPT );
						specials.remove( Type.LABORATORY );
						specials.remove( Type.LIBRARY );
						specials.remove( Type.STATUE );
						specials.remove( Type.TREASURY );
						specials.remove( Type.VAULT );
						specials.remove( Type.WEAK_FLOOR );
						
					} else if (constraints.depth % 5 == 2 && specials.contains( Type.LABORATORY )) {
						
						r.type = Type.LABORATORY;
						
					} else {
						
						int n = specials.size();
						r.type = specials.get( Math.min( constraints.random.nextInt( n ), constraints.random.nextInt( n ) ) );
						if (r.type == Type.WEAK_FLOOR) {
							weakFloorCreated = true;
						}

					}

					usedSpecialRooms.add( r.type );
					specials.remove( r.type );
					specialRooms++;
					
				} else if (constraints.random.nextInt( 2 ) == 0){

					LinkedHashSet<Room> neigbours = new LinkedHashSet<Room>();
					for (Room n : r.neigbours) {
						if (!r.connected.containsKey( n ) && 
							!constraints.specialRooms.contains( n.type ) &&
							n.type != Type.PIT) {
							
							neigbours.add( n );
						}
					}
					if (neigbours.size() > 1) {
						r.connect( constraints.random.nextElement( neigbours ) );
					}
				}
			}
		}

		// make all rooms with connections to other rooms either a tunnel or a standard room (the
		// higher the amount of connections, the higher the probability to create a tunnel)
		int count = 0;
		for (Room r : rooms) {
			if (r.type == Type.NULL) {
				int connections = r.connected.size();
				if (connections == 0) {
					// do nothing
				} else if (constraints.random.nextInt( connections * connections ) == 0) {
					r.type = Type.STANDARD;
					count++;
				} else {
					r.type = Type.TUNNEL; 
				}
			}
		}

		// if not enough standard rooms have been created then convert some tunnels to standard
		// rooms until at least a certain amount of standard rooms exists if possible
		for (int i = count; i <= 4; i++) {
			Optional<Room> r = randomRoomOfType( Type.TUNNEL, constraints.random);
			if (r.isPresent()) {
				r.get().type = Type.STANDARD;
			} else {
				// this warning is only necessary in context of the requirements of the vanilla
				// pixel dungeon
				System.err.println("WARNING: Could not find enough tunnels to turn into standard rooms!");
				break;
			}
		}
		return true;
	}
	
	protected void paintWater(Random random) {
		boolean[] lake = water(random);
		for (int i=0; i < LENGTH; i++) {
			if (getCell(i) == Terrain.EMPTY && lake[i]) {
				setCell(i, Terrain.WATER);
			}
		}
	}
	
	protected void paintGrass(Random random) {
		boolean[] grass = grass(random);
		
		if (feeling == Feeling.GRASS) {
			
			for (Room room : rooms) {
				if (room.type != Type.NULL && room.type != Type.PASSAGE && room.type != Type.TUNNEL) {
					grass[(room.left + 1) + (room.top + 1) * WIDTH] = true;
					grass[(room.right - 1) + (room.top + 1) * WIDTH] = true;
					grass[(room.left + 1) + (room.bottom - 1) * WIDTH] = true;
					grass[(room.right - 1) + (room.bottom - 1) * WIDTH] = true;
				}
			}
		}

		for (int i=WIDTH+1; i < LENGTH-WIDTH-1; i++) {
			if (getCell(i) == Terrain.EMPTY && grass[i]) {
				int count = 1;
				for (int n : NEIGHBOURS8) {
					if (grass[i + n]) {
						count++;
					}
				}
				setCell(i, (random.nextFloat() < count / 12f) ? Terrain.HIGH_GRASS : Terrain.GRASS);
			}
		}
	}
	
	protected abstract boolean[] water(Random random);
	protected abstract boolean[] grass(Random random);
	
	protected void placeTraps(GenerationConstraints constraints) {
		
		int nTraps = nTraps(constraints.depth, constraints.random);
		float[] trapChances = trapChances();
		
		for (int i=0; i < nTraps; i++) {
			
			int trapPos = constraints.random.nextInt( LENGTH );
			
			if (getCell(trapPos) == Terrain.EMPTY) {
				switch (constraints.random.nextChances( trapChances )) {
				case 0:
					setCell(trapPos, Terrain.SECRET_TOXIC_TRAP);
					break;
				case 1:
					setCell(trapPos, Terrain.SECRET_FIRE_TRAP);
					break;
				case 2:
					setCell(trapPos, Terrain.SECRET_PARALYTIC_TRAP);
					break;
				case 3:
					setCell(trapPos, Terrain.SECRET_POISON_TRAP);
					break;
				case 4:
					setCell(trapPos, Terrain.SECRET_ALARM_TRAP);
					break;
				case 5:
					setCell(trapPos, Terrain.SECRET_LIGHTNING_TRAP);
					break;
				case 6:
					setCell(trapPos, Terrain.SECRET_GRIPPING_TRAP);
					break;
				case 7:
					setCell(trapPos, Terrain.SECRET_SUMMONING_TRAP);
					break;
				}
			}
		}
	}
	
	protected int nTraps(int depth, Random random) {
		return depth <= 1 ? 0 : random.nextInt( 1, rooms.size() + depth );
	}
	
	protected float[] trapChances() {
		float[] chances = { 1, 1, 1, 1, 1, 1, 1, 1 };
		return chances;
	}
	
	protected int minRoomSize = 7;
	protected int maxRoomSize = 9;

	/**
	 * Splits a rectangle into smaller rectangles by recursively subdividing a given rectangle into
	 * two smaller rectangles (binary space partitioning)
	 *
	 * @param rect   A rectangle that will be split into smaller rectangles
	 */
	protected void split(Rect rect, Random random) {
		
		int width = rect.width();
		int height = rect.height();

		// Room is too wide => vertical split
		if (width > maxRoomSize && height < minRoomSize) {
			
			int vw = random.nextInt( rect.left + 3, rect.right - 3 );
			split( new Rect( rect.left, rect.top, vw, rect.bottom ), random);
			split( new Rect( vw, rect.top, rect.right, rect.bottom ), random);
			
		} else
		// room is too high => horizontal split
		if (height > maxRoomSize && width < minRoomSize) {

			int vh = random.nextInt( rect.top + 3, rect.bottom - 3 );
			split( new Rect( rect.left, rect.top, rect.right, vh ), random);
			split( new Rect( rect.left, vh, rect.right, rect.bottom ), random);
			
		} else 	
		if ((random.nextDouble() <= ((double)(minRoomSize * minRoomSize) / rect.square()) && width <= maxRoomSize && height <= maxRoomSize) || width < minRoomSize || height < minRoomSize) {

			rooms.add( (Room)new Room().set( rect ) );
			
		} else {
			
			if (random.nextFloat() < (float)(width - 2) / (width + height - 4)) {
				int vw = random.nextInt( rect.left + 3, rect.right - 3 );
				split( new Rect( rect.left, rect.top, vw, rect.bottom ), random);
				split( new Rect( vw, rect.top, rect.right, rect.bottom ), random);
			} else {
				int vh = random.nextInt( rect.top + 3, rect.bottom - 3 );
				split( new Rect( rect.left, rect.top, rect.right, vh ), random);
				split( new Rect( rect.left, vh, rect.right, rect.bottom ), random);
			}
			
		}
	}
	
	protected void paint(GenerationConstraints constraints) {
		
		for (Room r : rooms) {
			if (r.type != Type.NULL) {
				placeDoors( r, constraints.random);
				r.type.paint( this, r, constraints );
			} else {
				if (feeling == Feeling.CHASM && constraints.random.nextInt( 2 ) == 0) {
					Painter.fill( this, r, Terrain.WALL );
				}
			}
		}
		
		for (Room r : rooms) {
			paintDoors( r, constraints.depth, constraints.random);
		}
	}
	
	private void placeDoors(Room r, Random random) {
		for (Room n : r.connected.keySet()) {
			Room.Door door = r.connected.get( n );
			if (door == null) {
				
				Rect i = r.intersect( n );
				if (i.width() == 0) {
					door = new Room.Door( 
						i.left, 
						random.nextInt( i.top + 1, i.bottom ) );
				} else {
					door = new Room.Door( 
						random.nextInt( i.left + 1, i.right ),
						i.top);
				}

				r.connected.put( n, door );
				n.connected.put( r, door );
			}
		}
	}
	
	protected void paintDoors(Room r, int depth, Random random) {
		for (Room n : r.connected.keySet()) {

			if (joinRooms( r, n )) {
				continue;
			}
			
			Room.Door d = r.connected.get( n );
			int door = d.x + d.y * WIDTH;
			
			switch (d.type) {
			case EMPTY:
				setCell(door, Terrain.EMPTY);
				break;
			case TUNNEL:
				setCell(door, tunnelTile());
				break;
			case REGULAR:
				if (depth <= 1) {
					setCell(door, Terrain.DOOR);
				} else {
					boolean secret = (depth < 6 ? random.nextInt( 12 - depth ) : random.nextInt( 6 )) == 0;
					setCell(door, secret ? Terrain.SECRET_DOOR : Terrain.DOOR);
					if (secret) {
						secretDoors++;
					}
				}
				break;
			case UNLOCKED:
				setCell(door, Terrain.DOOR);
				break;
			case HIDDEN:
				setCell(door, Terrain.SECRET_DOOR);
				secretDoors++;
				break;
			case BARRICADE:
				setCell(door, random.nextInt( 3 ) == 0 ? Terrain.BOOKSHELF : Terrain.BARRICADE);
				break;
			case LOCKED:
				setCell(door, Terrain.LOCKED_DOOR);
				break;
			}
		}
	}
	
	protected boolean joinRooms( Room r, Room n ) {
		
		if (r.type != Room.Type.STANDARD || n.type != Room.Type.STANDARD) {
			return false;
		}
		
		Rect w = r.intersect( n );
		if (w.left == w.right) {
			
			if (w.bottom - w.top < 3) {
				return false;
			}
			
			if (w.height() == Math.max( r.height(), n.height() )) {
				return false;
			}
			
			if (r.width() + n.width() > maxRoomSize) {
				return false;
			}
			
			w.top += 1;
			w.bottom -= 0;
			
			w.right++;
			
			Painter.fill( this, w.left, w.top, 1, w.height(), Terrain.EMPTY );
			
		} else {
			
			if (w.right - w.left < 3) {
				return false;
			}
			
			if (w.width() == Math.max( r.width(), n.width() )) {
				return false;
			}
			
			if (r.height() + n.height() > maxRoomSize) {
				return false;
			}
			
			w.left += 1;
			w.right -= 0;
			
			w.bottom++;
			
			Painter.fill( this, w.left, w.top, w.width(), 1, Terrain.EMPTY );
		}
		
		return true;
	}

	protected void placeSign(Random random) {
		Optional<Integer> pos = roomEntrance.random(new Predicate<Integer>() {
			@Override
			public boolean test(Integer pos) {
				return pos != entrance;
			}
		}, this, random);
		if (pos.isPresent()) {
			setCell(pos.get(), Terrain.SIGN);
		} else {
			DLog.error("RegularLevel", "Could not place sign!");
			// TODO Throw exception and regenerate level?!
		}
	}

	@Override
	public int nMobs(int depth) {
		return 2 + depth % 5 + new Random().nextInt( 3 );
	}

	@Override
	protected void createMobs(GenerationConstraints constraints) {
		for (Room r : rooms) {
			r.type.addActors( this, r, constraints );
		}

		int nMobs = nMobs(constraints.depth);
		for (int i=0; i < nMobs; i++) {
			Mob mob = Bestiary.mob( constraints.depth );
			do {
				mob.pos = randomRespawnCell();
			} while (mob.pos == -1);
			addMob( mob );
			Actor.occupyCell( mob );
		}
	}

	@Override
	public int randomRespawnCell() {
		int count = 0;
		int cell = -1;

		while (true) {

			if (++count > 10) {
				return -1;
			}

			Optional<Room> room = randomRoomOfType( Type.STANDARD, new Random());
			if (!room.isPresent()) {
				continue;
			}

			cell = room.get().random(this, new Random());
			if (!Dungeon.visible[cell] && Actor.findChar( cell ) == null && Level.isPassable(cell)) {
				return cell;
			}

		}
	}

	@Override
	public int randomDestination() {

		int cell = -1;

		// TODO This could be replaced by getting all rooms shuffled and then iterating it.
		while (true) {

			Room room = Random.element( rooms );
			if (room == null) {
				continue;
			}

			cell = room.random(this, new Random());
			if (Level.isPassable(cell)) {
				return cell;
			}

		}
	}

	@Override
	protected void createItems(GenerationConstraints constraints) {
		for (Room r : rooms) {
			r.type.addItems( this, r, constraints );
		}

		int nItems = 3;
		while (Random.Float() < 0.4f) {
			nItems++;
		}

		for (int i=0; i < nItems; i++) {
			Heap.Type type = null;
			switch (Random.Int( 20 )) {
			case 0:
				type = Heap.Type.SKELETON;
				break;
			case 1:
			case 2:
			case 3:
			case 4:
				type = Heap.Type.CHEST;
				break;
			case 5:
				type = Dungeon.depth > 1 ? Heap.Type.MIMIC : Heap.Type.CHEST;
				break;
			default:
				type = Heap.Type.HEAP;
			}
			drop( Generator.random(), randomDropCell() ).type = type;
		}

		for (Item item : itemsToSpawn) {
			int cell = randomDropCell();
			if (item instanceof ScrollOfUpgrade) {

				while (getCell(cell) == Terrain.FIRE_TRAP || getCell(cell) == Terrain.SECRET_FIRE_TRAP) {
					cell = randomDropCell();
				}
			}

			drop( item, cell ).type = Heap.Type.HEAP;
		}

		Item item = Bones.get();
		if (item != null) {
			drop( item, randomDropCell() ).type = Heap.Type.SKELETON;
		}
	}

	/**
	 * @param type The type of rooms that should be returned
	 * @return The set of all rooms of a chosen type
	 */
	private LinkedHashSet<Room> getRoomSetOfType(Room.Type type) {
		LinkedHashSet<Room> roomsOfType = new LinkedHashSet<>();
		for (Room room: rooms) {
			if (room.type == type) {
				roomsOfType.add(room);
			}
		}
		return roomsOfType;
	}

	/**
	 * @param type   Type of the random room to find
	 * @param random
	 * @return A random room of a certain type or null if no room of that type exists
	 */
	public Optional<Room> randomRoomOfType(Type type, Random random) {
		// this must be an ordered data structure (instead of just a HashSet) so that the random element sampling is
		// deterministic according to the seed
		LinkedHashSet<Room> roomsOfType = getRoomSetOfType(type);
		if (!roomsOfType.isEmpty()) {
			return Optional.of(random.nextElement(roomsOfType));
		} else {
			return Optional.empty();
		}
	}

	public Room room( int pos ) {
		for (Room room : rooms) {
			if (room.type != Type.NULL && room.inside( new Point(pos % width(), pos / width()) )) {
				return room;
			}
		}

		return null;
	}

	protected int randomDropCell() {
		ArrayList<Room> standardRooms = new ArrayList<>(getRoomSetOfType(Type.STANDARD));
		if (standardRooms.isEmpty()) {
			DLog.error("RegularLevel", "Could not find Standard Room as drop cell");
			// TODO Throw exception and regenerate level?!
		}
		ArrayList<Room> shuffledStandardRooms = new Random().nextShuffle(standardRooms);
		for (Room room: shuffledStandardRooms) {
			Optional<Integer> passablePosition = room.random(new Predicate<Integer>() {
				@Override
				public boolean test(Integer pos) {
					return isPassable(pos);
				}
			}, this, new Random());
			if (passablePosition.isPresent()) {
				return passablePosition.get();
			}
		}
		DLog.error("Level", "Could not find any passable position in any standard room!");
		// TODO Throw exception and regenerate level?!
		while (true) {
			Optional<Room> room = randomRoomOfType( Type.STANDARD, new Random());
			if (room.isPresent()) {
				int pos = room.get().random(this, new Random());
				if (isPassable(pos)) {
					return pos;
				}
			}
		}
	}

	@Override
	public int pitCell() {
		for (Room room : rooms) {
			if (room.type == Type.PIT) {
				return room.random(this, new Random());
			}
		}
		
		return super.pitCell();
	}

	@Override
	public void storeInBundle( Bundle bundle ) {
		super.storeInBundle( bundle );
		bundle.put( "rooms", rooms );
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void restoreFromBundle( Bundle bundle ) {
		super.restoreFromBundle( bundle );

		rooms = new LinkedHashSet<Room>( (Collection<Room>) ((Collection<?>) bundle.getCollection( "rooms" )) );
		for (Room r : rooms) {
			if (r.type == Type.WEAK_FLOOR) {
				weakFloorCreated = true;
				break;
			}
		}
		// This is necessary to be able to add already spawned quests as event observers to already
		// generated levels. Otherwise only newly created levels could be observed by quests.
		Dungeon.spawnQuests( this );
	}
	
}
