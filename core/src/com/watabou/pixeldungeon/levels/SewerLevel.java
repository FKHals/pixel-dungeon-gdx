/*
 * Pixel Dungeon
 * Copyright (C) 2012-2014  Oleg Dolya
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.watabou.pixeldungeon.levels;

import com.watabou.noosa.Game;
import com.watabou.noosa.Scene;
import com.watabou.noosa.particles.Emitter;
import com.watabou.noosa.particles.PixelParticle;
import com.watabou.pixeldungeon.Dungeon;
import com.watabou.pixeldungeon.DungeonTilemap;
import com.watabou.pixeldungeon.items.DewVial;
import com.watabou.pixeldungeon.levels.terraintypes.SewerTerrain;
import com.watabou.pixeldungeon.scenes.GameScene;
import com.watabou.utils.ColorMath;
import com.watabou.utils.PointF;
import com.watabou.utils.Random;

public class SewerLevel extends RegularLevel {

	{
		terrain = new SewerTerrain();
	}
	
	protected boolean[] water(Random random) {
		return Patch.generate( feeling == Feeling.WATER ? 0.60f : 0.45f, 5, random);
	}
	
	protected boolean[] grass(Random random) {
		return Patch.generate( feeling == Feeling.GRASS ? 0.60f : 0.40f, 4, random);
	}
	
	@Override
	protected void decorate(GenerationConstraints constraints) {
		
		for (int i=0; i < WIDTH; i++) {
			if (getCell(i) == Terrain.WALL &&
				getCell(i + WIDTH) == Terrain.WATER &&
				constraints.random.nextInt( 4 ) == 0) {
				
				setCell(i, Terrain.WALL_DECO);
			}
		}
		
		for (int i=WIDTH; i < LENGTH - WIDTH; i++) {
			if (getCell(i) == Terrain.WALL &&
				getCell(i - WIDTH) == Terrain.WALL &&
				getCell(i + WIDTH) == Terrain.WATER &&
				constraints.random.nextInt( 2 ) == 0) {
				
				setCell(i, Terrain.WALL_DECO);
			}
		}
		
		for (int i=WIDTH + 1; i < LENGTH - WIDTH - 1; i++) {
			if (getCell(i) == Terrain.EMPTY) {
				
				int count = 
					(getCell(i + 1) == Terrain.WALL ? 1 : 0) +
					(getCell(i - 1) == Terrain.WALL ? 1 : 0) +
					(getCell(i + WIDTH) == Terrain.WALL ? 1 : 0) +
					(getCell(i - WIDTH) == Terrain.WALL ? 1 : 0);
				
				if (constraints.random.nextInt( 16 ) < count * count) {
					setCell(i, Terrain.EMPTY_DECO);
				}
			}
		}
		
		placeSign(constraints.random);
	}

	@Override
	protected void createItems(GenerationConstraints constraints) {
		if (Dungeon.dewVial && Random.Int( 4 - constraints.depth ) == 0) {
			addItemToSpawn( new DewVial() );
			Dungeon.dewVial = false;
		}
		
		super.createItems(constraints);
	}
	
	@Override
	public void addVisuals( Scene scene ) {
		super.addVisuals( scene );
		addVisuals( this, scene );
	}
	
	public static void addVisuals( Level level, Scene scene ) {
		for (int i=0; i < LENGTH; i++) {
			if (level.getCell(i) == Terrain.WALL_DECO) {
				scene.add( new Sink( i ) );
			}
		}
	}

	private static class Sink extends Emitter {
		
		private int pos;
		private float rippleDelay = 0;
		
		private static final Emitter.Factory factory = new Factory() {
			
			@Override
			public void emit( Emitter emitter, int index, float x, float y ) {
				WaterParticle p = (WaterParticle)emitter.recycle( WaterParticle.class );
				p.reset( x, y );
			}
		};
		
		public Sink( int pos ) {
			super();
			
			this.pos = pos;
			
			PointF p = DungeonTilemap.tileCenterToWorld( pos );
			pos( p.x - 2, p.y + 1, 4, 0 );
			
			pour( factory, 0.05f );
		}
		
		@Override
		public void update() {
			if (visible = Dungeon.visible[pos]) {
				
				super.update();
				
				if ((rippleDelay -= Game.elapsed) <= 0) {
					GameScene.ripple( pos + WIDTH ).y -= DungeonTilemap.SIZE / 2;
					rippleDelay = Random.Float( 0.2f, 0.3f );
				}
			}
		}
	}
	
	public static final class WaterParticle extends PixelParticle {
		
		public WaterParticle() {
			super();
			
			acc.y = 50;
			am = 0.5f;
			
			color( ColorMath.random( 0xb6ccc2, 0x3b6653 ) );
			size( 2 );
		}
		
		public void reset( float x, float y ) {
			revive();
			
			this.x = x;
			this.y = y;
			
			speed.set( Random.Float( -2, +2 ), 0 );
			
			left = lifespan = 0.5f;
		}
	}
}
