/*
 * Pixel Dungeon
 * Copyright (C) 2012-2014  Oleg Dolya
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.watabou.pixeldungeon.levels;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.watabou.noosa.Game;
import com.watabou.noosa.Group;
import com.watabou.noosa.Scene;
import com.watabou.noosa.particles.PixelParticle;
import com.watabou.pixeldungeon.Dungeon;
import com.watabou.pixeldungeon.DungeonTilemap;
import com.watabou.pixeldungeon.items.Torch;
import com.watabou.pixeldungeon.levels.terraintypes.HallsTerrain;
import com.watabou.pixeldungeon.quests.QuestManager;
import com.watabou.utils.PointF;
import com.watabou.utils.Random;

public class HallsLevel extends RegularLevel {

	{
		minRoomSize = 6;
		
		viewDistance = 3; // just a default value that actually gets initiated in create()

		terrain = new HallsTerrain();
	}
	
	@Override
	public void create(GenerationConstraints constraints, QuestManager questManager) {
		viewDistance = Math.max( 25 - constraints.depth, 1 );
		addItemToSpawn( new Torch() );
		super.create(constraints, questManager);
	}

	protected boolean[] water(Random random) {
		return Patch.generate( feeling == Feeling.WATER ? 0.55f : 0.40f, 6, random);
	}
	
	protected boolean[] grass(Random random) {
		return Patch.generate( feeling == Feeling.GRASS ? 0.55f : 0.30f, 3, random);
	}
	
	@Override
	protected void decorate(GenerationConstraints constraints) {
		
		for (int i=WIDTH + 1; i < LENGTH - WIDTH - 1; i++) {
			if (getCell(i) == Terrain.EMPTY) {
				
				int count = 0;
				for (int neighbour : NEIGHBOURS8) {
					if (getCell(i + neighbour).isPassable()) {
						count++;
					}
				}
				
				if (constraints.random.nextInt( 80 ) < count) {
					setCell(i, Terrain.EMPTY_DECO);
				}
				
			} else
			if (getCell(i) == Terrain.WALL &&
					getCell(i-1) != Terrain.WALL_DECO && getCell(i-WIDTH) != Terrain.WALL_DECO &&
					constraints.random.nextInt( 20 ) == 0) {

				setCell(i, Terrain.WALL_DECO);

			}
		}
		
		placeSign(constraints.random);
	}
	
	@Override
	public void addVisuals( Scene scene ) {
		super.addVisuals( scene );
		addVisuals( this, scene );
	}
	
	public static void addVisuals( Level level, Scene scene ) {
		for (int i=0; i < LENGTH; i++) {
			if (level.getCell(i) == Terrain.WATER) {
				scene.add( new Stream( i ) );
			}
		}
	}
	
	private static class Stream extends Group {
		
		private int pos;
		
		private float delay;
		
		public Stream( int pos ) {
			super();
			
			this.pos = pos;
			
			delay = Random.Float( 2 );
		}
		
		@Override
		public void update() {
			
			if (visible = Dungeon.visible[pos]) {
				
				super.update();
				
				if ((delay -= Game.elapsed) <= 0) {
					
					delay = Random.Float( 2 );
					
					PointF p = DungeonTilemap.tileToWorld( pos );
					((FireParticle)recycle( FireParticle.class )).reset( 
						p.x + Random.Float( DungeonTilemap.SIZE ), 
						p.y + Random.Float( DungeonTilemap.SIZE ) );
				}
			}
		}
		
		@Override
		public void draw() {
			Gdx.gl.glBlendFunc( GL20.GL_SRC_ALPHA, GL20.GL_ONE );
			super.draw();
			Gdx.gl.glBlendFunc( GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA );
		}
	}
	
	public static class FireParticle extends PixelParticle.Shrinking {
		
		public FireParticle() {
			super();
			
			color( 0xEE7722 );
			lifespan = 1f;
			
			acc.set( 0, +80 );
		}
		
		public void reset( float x, float y ) {
			revive();
			
			this.x = x;
			this.y = y;
			
			left = lifespan;
			
			speed.set( 0, -40 );
			size = 4;
		}
		
		@Override
		public void update() {
			super.update();
			float p = left / lifespan;
			am = p > 0.8f ? (1 - p) * 5 : 1;
		}
	}
}
