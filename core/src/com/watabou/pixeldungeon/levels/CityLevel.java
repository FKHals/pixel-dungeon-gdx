/*
 * Pixel Dungeon
 * Copyright (C) 2012-2014  Oleg Dolya
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.watabou.pixeldungeon.levels;

import com.watabou.noosa.Scene;
import com.watabou.noosa.particles.Emitter;
import com.watabou.noosa.particles.PixelParticle;
import com.watabou.pixeldungeon.Dungeon;
import com.watabou.pixeldungeon.DungeonTilemap;
import com.watabou.pixeldungeon.levels.Room.Type;
import com.watabou.pixeldungeon.levels.terraintypes.CityTerrain;
import com.watabou.utils.PointF;
import com.watabou.utils.Random;

public class CityLevel extends RegularLevel {

	{
		terrain = new CityTerrain();
	}
	
	protected boolean[] water(Random random) {
		return Patch.generate( feeling == Feeling.WATER ? 0.65f : 0.45f, 4, random);
	}
	
	protected boolean[] grass(Random random) {
		return Patch.generate( feeling == Feeling.GRASS ? 0.60f : 0.40f, 3, random);
	}
	
	@Override
	protected boolean assignRoomType(GenerationConstraints constraints) {
		boolean roomsAssigned = super.assignRoomType(constraints);

		if (super.assignRoomType(constraints)) {
			for (Room r : rooms) {
				if (r.type == Type.TUNNEL) {
					r.type = Type.PASSAGE;
				}
			}
		}
		return roomsAssigned;
	}
	
	@Override
	protected void decorate(GenerationConstraints constraints) {
		
		for (int i=0; i < LENGTH; i++) {
			if (getCell(i) == Terrain.EMPTY && constraints.random.nextInt( 10 ) == 0) {
				setCell(i, Terrain.EMPTY_DECO);
			} else if (getCell(i) == Terrain.WALL && constraints.random.nextInt( 8 ) == 0) {
				setCell(i, Terrain.WALL_DECO);
			}
		}
		
		placeSign(constraints.random);
	}

	@Override
	public void addVisuals( Scene scene ) {
		super.addVisuals( scene );
		addVisuals( this, scene );
	}
	
	public static void addVisuals( Level level, Scene scene ) {
		for (int i=0; i < LENGTH; i++) {
			if (level.getCell(i) == Terrain.WALL_DECO) {
				scene.add( new Smoke( i ) );
			}
		}
	}
	
	private static class Smoke extends Emitter {
		
		private int pos;
		
		private static final Emitter.Factory factory = new Factory() {
			
			@Override
			public void emit( Emitter emitter, int index, float x, float y ) {
				SmokeParticle p = (SmokeParticle)emitter.recycle( SmokeParticle.class );
				p.reset( x, y );
			}
		};
		
		public Smoke( int pos ) {
			super();
			
			this.pos = pos;
			
			PointF p = DungeonTilemap.tileCenterToWorld( pos );
			pos( p.x - 4, p.y - 2, 4, 0 );
			
			pour( factory, 0.2f );
		}
		
		@Override
		public void update() {
			if (visible = Dungeon.visible[pos]) {
				super.update();
			}
		}
	}
	
	public static final class SmokeParticle extends PixelParticle {
		
		public SmokeParticle() {
			super();
			
			color( 0x000000 );
			speed.set( Random.Float( 8 ), -Random.Float( 8 ) );
		}
		
		public void reset( float x, float y ) {
			revive();
			
			this.x = x;
			this.y = y;
			
			left = lifespan = 2f;
		}
		
		@Override
		public void update() {
			super.update();
			float p = left / lifespan;
			am = p > 0.8f ? 1 - p : p * 0.25f;
			size( 8 - p * 4 );
		}
	}
}