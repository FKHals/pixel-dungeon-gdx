package com.watabou.pixeldungeon.levels;

import com.watabou.pixeldungeon.plants.Plant;
import com.watabou.pixeldungeon.scenes.GameScene;
import com.watabou.utils.Bundlable;
import com.watabou.utils.Bundle;
import com.watabou.utils.SparseArray;

import java.util.Collection;

public class PlantMap implements Bundlable {

    private SparseArray<Plant> plants;

    public PlantMap() {
        plants = new SparseArray<Plant>();
    }

    private static final String PLANTS = "plants";

    @Override
    public void restoreFromBundle( Bundle bundle ) {
        Collection<Bundlable> collection = bundle.getCollection( PLANTS );
        for (Bundlable p : collection) {
            Plant plant = (Plant)p;
            plants.put( plant.pos, plant );
        }
    }

    @Override
    public void storeInBundle( Bundle bundle ) {
        bundle.put( PLANTS, plants.valuesAsList() );
    }

    public void adjustPos(Level level) {
        for (Plant plant : plants.valuesAsList()) {
            if (plant != null) {
                plant.pos = level.adjustPos( plant.pos );
            }
        }
    }

    public Collection<Plant> getPlants() {
        return plants.valuesAsList();
    }

    public Plant get( int cell ) {
        return plants.get(cell);
    }

    public Plant add(Plant.Seed seed, int pos, Level level) {

        Plant plant = plants.get( pos );
        if (plant != null) {
            plant.wither(level);
        }

        plant = seed.couch( pos );
        plants.put( pos, plant );

        GameScene.add( plant );

        return plant;
    }

    public void remove( int pos ) {
        plants.remove( pos );
    }
}
