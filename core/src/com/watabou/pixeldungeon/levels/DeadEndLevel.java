/*
 * Pixel Dungeon
 * Copyright (C) 2012-2014  Oleg Dolya
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.watabou.pixeldungeon.levels;

import java.util.Arrays;

import com.watabou.pixeldungeon.Assets;
import com.watabou.utils.Random;

public class DeadEndLevel extends Level {

	private static final int SIZE = 5;
	
	@Override
	public String tilesTex() {
		return Assets.TILES_CAVES;
	}
	
	@Override
	public String waterTex() {
		return Assets.WATER_HALLS;
	}

	@Override
	public int leafColor() {
		return 0x534f3e;
	}

	@Override
	public int leafColorAlternative() {
		return 0xb9d661;
	}
	
	@Override
	protected boolean build(GenerationConstraints constraints) {

		fillMap(Terrain.WALL );
		
		for (int i=2; i < SIZE; i++) {
			for (int j=2; j < SIZE; j++) {
				setCell(i * WIDTH + j, Terrain.EMPTY);
			}
		}
		
		for (int i=1; i <= SIZE; i++) {
			setCell(WIDTH + i, Terrain.WATER);
			setCell(WIDTH * SIZE + i, Terrain.WATER);
			setCell(WIDTH * i + 1, Terrain.WATER);
			setCell(WIDTH * i + SIZE, Terrain.WATER);
		}
		
		entrance = SIZE * WIDTH + SIZE / 2 + 1;
		setCell(entrance, Terrain.ENTRANCE);
		
		exit = -1;
		
		setCell((SIZE / 2 + 1) * (WIDTH + 1), Terrain.SIGN);
		
		return true;
	}

	@Override
	protected void decorate(GenerationConstraints constraints) {
		for (int i=0; i < LENGTH; i++) {
			if (getCell(i) == Terrain.EMPTY && constraints.random.nextInt( 10 ) == 0) {
				setCell(i, Terrain.EMPTY_DECO);
			} else if (getCell(i) == Terrain.WALL && constraints.random.nextInt( 8 ) == 0) {
				setCell(i, Terrain.WALL_DECO);
			}
		}
	}

	@Override
	protected void createMobs(GenerationConstraints constraints) {
	}

	@Override
	protected void createItems(GenerationConstraints constraints) {
	}
	
	@Override
	public int randomRespawnCell() {
		return -1;
	}

}
