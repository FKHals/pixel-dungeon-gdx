/*
 * Pixel Dungeon
 * Copyright (C) 2012-2014  Oleg Dolya
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.watabou.pixeldungeon.actors.mobs.npcs;

import com.watabou.pixeldungeon.Dungeon;
import com.watabou.pixeldungeon.actors.mobs.Mob;
import com.watabou.pixeldungeon.items.Heap;
import com.watabou.pixeldungeon.levels.Level;
import com.watabou.pixeldungeon.quests.Quest;
import com.watabou.utils.Bundle;
import com.watabou.utils.Random;

public abstract class NPC extends Mob {

	public Quest<?> quest;
	
	{
		HP = HT = 1;
		EXP = 0;
	
		hostile = false;
		state = PASSIVE;
	}

	private static final String QUEST = "quest";

	@Override
	public void storeInBundle( Bundle bundle ) {
		super.storeInBundle( bundle );
		if (quest != null) {
			bundle.put(QUEST, quest.getId());
		}
	}

	@Override
	public void restoreFromBundle( Bundle bundle ) {
		super.restoreFromBundle( bundle );
		quest = Dungeon.questManager.findById(bundle.getInt( QUEST ));
	}

	public void setQuest(Quest<?> quest) {
		this.quest = quest;
	}

	public void removeQuest() {
		this.quest = null;
	}
	
	protected void throwItem() {
		Heap heap = Dungeon.level.heaps.get( pos );
		if (heap != null) {
			int n;
			do {
				n = pos + Level.NEIGHBOURS8[Random.Int( 8 )];
			} while (!Level.isPassable(n) && !Level.isAvoided(n));
			Dungeon.level.drop( heap.pickUp(), n ).sprite.drop( pos );
		}
	}
	
	@Override
	public void beckon( int cell ) {
	}
	
	abstract public void interact();
}
