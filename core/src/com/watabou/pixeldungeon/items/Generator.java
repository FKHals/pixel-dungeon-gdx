/*
 * Pixel Dungeon
 * Copyright (C) 2012-2015 Oleg Dolya
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.watabou.pixeldungeon.items;

import java.util.HashMap;

import com.badlogic.gdx.utils.reflect.ClassReflection;
import com.watabou.pixeldungeon.Dungeon;
import com.watabou.pixeldungeon.actors.hero.Hero;
import com.watabou.pixeldungeon.items.armor.*;
import com.watabou.pixeldungeon.items.bags.Bag;
import com.watabou.pixeldungeon.items.food.Food;
import com.watabou.pixeldungeon.items.food.MysteryMeat;
import com.watabou.pixeldungeon.items.food.Pasty;
import com.watabou.pixeldungeon.items.potions.*;
import com.watabou.pixeldungeon.items.rings.*;
import com.watabou.pixeldungeon.items.scrolls.*;
import com.watabou.pixeldungeon.items.wands.*;
import com.watabou.pixeldungeon.items.weapon.*;
import com.watabou.pixeldungeon.items.weapon.melee.*;
import com.watabou.pixeldungeon.items.weapon.missiles.*;
import com.watabou.pixeldungeon.plants.*;
import com.watabou.utils.DLog;
import com.watabou.utils.Random;

public class Generator {

	public static enum Category {
		WEAPON	( 15,	Weapon.class ),
		ARMOR	( 10,	Armor.class ),
		POTION	( 50,	Potion.class ),
		SCROLL	( 40,	Scroll.class ),
		WAND	( 4,	Wand.class ),
		RING	( 2,	Ring.class ),
		SEED	( 5,	Plant.Seed.class ),
		FOOD	( 0,	Food.class ),
		GOLD	( 50,	Gold.class ),
		MISC	( 5,	Item.class );
		
		public Class<?>[] classes;
		public float[] probs;
		
		public float prob;
		public Class<? extends Item> superClass;
		
		private Category( float prob, Class<? extends Item> superClass ) {
			this.prob = prob;
			this.superClass = superClass;
		}
		
		public static int order( Item item ) {
			for (int i=0; i < values().length; i++) {
				if (values()[i].superClass.isInstance( item )) {
					return i;
				}
			}
			
			return item instanceof Bag ? Integer.MAX_VALUE : Integer.MAX_VALUE - 1;
		}
	};
	
	private static HashMap<Category,Float> categoryProbs = new HashMap<Generator.Category, Float>();
	
	static {
		
		Category.GOLD.classes = new Class<?>[]{ 
			Gold.class };
		Category.GOLD.probs = new float[]{ 1 };

		Category.SCROLL.classes = new Class<?>[]{
			ScrollOfIdentify.class,
			ScrollOfTeleportation.class,
			ScrollOfRemoveCurse.class,
			ScrollOfRecharging.class,
			ScrollOfMagicMapping.class,
			ScrollOfChallenge.class,
			ScrollOfTerror.class,
			ScrollOfLullaby.class,
			ScrollOfPsionicBlast.class,
			ScrollOfMirrorImage.class,
			ScrollOfUpgrade.class,
			ScrollOfEnchantment.class };
		Category.SCROLL.probs = new float[]{ 30, 10, 15, 10, 15, 12, 8, 8, 4, 6, 0, 1 };
		
		Category.POTION.classes = new Class<?>[]{ 
			PotionOfHealing.class, 
			PotionOfExperience.class,
			PotionOfToxicGas.class, 
			PotionOfParalyticGas.class,
			PotionOfLiquidFlame.class,
			PotionOfLevitation.class,
			PotionOfStrength.class,
			PotionOfMindVision.class,
			PotionOfPurity.class,
			PotionOfInvisibility.class,
			PotionOfMight.class,
			PotionOfFrost.class };
		Category.POTION.probs = new float[]{ 45, 4, 15, 10, 15, 10, 0, 20, 12, 10, 0, 10 };

		Category.WAND.classes = new Class<?>[]{
			WandOfTeleportation.class,
			WandOfSlowness.class,
			WandOfFirebolt.class,
			WandOfRegrowth.class,
			WandOfPoison.class,
			WandOfBlink.class,
			WandOfLightning.class,
			WandOfAmok.class,
			WandOfReach.class,
			WandOfFlock.class,
			WandOfMagicMissile.class,
			WandOfDisintegration.class,
			WandOfAvalanche.class };
		Category.WAND.probs = new float[]{ 10, 10, 15, 6, 10, 11, 15, 10, 6, 10, 0, 5, 5 };
		
		Category.WEAPON.classes = new Class<?>[]{ 
			Dagger.class, 
			Knuckles.class,
			Quarterstaff.class, 
			Spear.class, 
			Mace.class, 
			Sword.class, 
			Longsword.class,
			BattleAxe.class,
			WarHammer.class, 
			Glaive.class,
			ShortSword.class,
			Dart.class,
			Javelin.class,
			IncendiaryDart.class,
			CurareDart.class,
			Shuriken.class,
			Boomerang.class,
			Tamahawk.class };
		Category.WEAPON.probs = new float[]{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1 };
		
		Category.ARMOR.classes = new Class<?>[]{ 
			ClothArmor.class, 
			LeatherArmor.class, 
			MailArmor.class, 
			ScaleArmor.class, 
			PlateArmor.class };
		Category.ARMOR.probs = new float[]{ 1, 1, 1, 1, 1 };
		
		Category.FOOD.classes = new Class<?>[]{ 
			Food.class, 
			Pasty.class,
			MysteryMeat.class };
		Category.FOOD.probs = new float[]{ 4, 1, 0 };
			
		Category.RING.classes = new Class<?>[]{ 
			RingOfMending.class,
			RingOfDetection.class,
			RingOfShadows.class,
			RingOfPower.class,
			RingOfHerbalism.class,
			RingOfAccuracy.class,
			RingOfEvasion.class,
			RingOfSatiety.class,
			RingOfHaste.class,
			RingOfElements.class,
			RingOfHaggler.class,
			RingOfThorns.class };
		Category.RING.probs = new float[]{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0 };

		Category.SEED.classes = new Class<?>[]{
			Firebloom.Seed.class,
			Icecap.Seed.class,
			Sorrowmoss.Seed.class,
			Dreamweed.Seed.class,
			Sungrass.Seed.class,
			Earthroot.Seed.class,
			Fadeleaf.Seed.class,
			Rotberry.Seed.class };
		Category.SEED.probs = new float[]{ 1, 1, 1, 1, 1, 1, 1, 0 };

		Category.MISC.classes = new Class<?>[]{
			Bomb.class,
			Honeypot.class};
		Category.MISC.probs = new float[]{ 2, 1 };
	}
	
	public static void reset() {
		for (Category cat : Category.values()) {
			categoryProbs.put( cat, cat.prob );
		}
	}
	
	public static Item random() {
		return random( Random.chances( categoryProbs ) );
	}

	/**
	 * Returns a random item for the current (hero) strength (The potential strength that the hero
	 * could have considering the starting health and the number of available potions of strength)
	 */
	public static Item random( Category cat ) {
		int currentStrength = Hero.STARTING_STR + Dungeon.potionOfStrength;
		return random( cat, currentStrength );
	}

	/**
	 * Returns a random item of a specific category for a specific (hero) strength.
	 * Also decreases the spawning probability for items of that category.
	 * @param cat The category of the generated item
	 * @param heroStrength The current strength of the hero that the random item will be generated
	 *                     for (which does NOT mean that the item always has the same level)
	 * @return The item of a specific category considering a specific hero strength
	 */
	public static Item random( Category cat, int heroStrength ) {
		try {
			// decrease the spawning probability for items of the same category
			categoryProbs.put( cat, categoryProbs.get( cat ) / 2 );
			
			switch (cat) {
			case ARMOR:
				return randomArmor(heroStrength);
			case WEAPON:
				return randomWeapon(heroStrength);
			default:
				return ((Item)cat.classes[Random.chances( cat.probs )].newInstance()).random();
			}
			
		} catch (Exception e) {
			DLog.error("Generic", "generic error?", e);
			return null;
			
		}
	}
	
	public static Item random( Class<? extends Item> cl ) {
		try {
			return ((Item) ClassReflection.newInstance(cl)).random();

		} catch (Exception e) {
			DLog.error("Generic", "generic error?", e);
			return null;
		}
	}

	//FIXME DRY: randomArmor() and randomWeapon() are the same except for the item category.
	//		The Problem is that randomItem() is not possible without changing the class
	//      hierarchy of the armor and weapon items in general since only both of these have
	//      a STR (strength) variable and they do not share the same (direct) parent-class.
	//      Otherwise one could just create a new subclass (e.g. EquipableFightingItem) but that
	//      would cause other items in the actual superclass KindOfWeapon to all get a STR
	//      variable which would be wrong.
	/**
	 * Works like {@link Generator#randomWeapon(int)} except that the generated item is an armor.
	 * @see Generator#randomWeapon(int)
	 */
	public static Armor randomArmor( int heroStrength ) throws Exception {

		Category cat = Category.ARMOR;
		
		Armor a1 = (Armor)cat.classes[Random.chances( cat.probs )].getDeclaredConstructor().newInstance();
		Armor a2 = (Armor)cat.classes[Random.chances( cat.probs )].getDeclaredConstructor().newInstance();
		
		a1.random();
		a2.random();
		
		return Math.abs( heroStrength - a1.STR ) < Math.abs( heroStrength - a2.STR ) ? a1 : a2;
	}

	/**
	 * Returns a random weapon for a specific (hero) strength.
	 * Two completely random weapons are generated and the one that has the STR attribute that is
	 * closer to the (potential, see {@link Generator#random(Category)}) player strength is
	 * actually returned.
	 * @param heroStrength The current strength of the hero that the random weapon will be generated
	 *                     for (which does NOT mean that the weapon always has the same level)
	 * @return The generated weapon considering a specific hero strength
	 */
	public static Weapon randomWeapon( int heroStrength ) throws Exception {

		Category cat = Category.WEAPON;
		
		Weapon w1 = (Weapon)cat.classes[Random.chances( cat.probs )].getDeclaredConstructor().newInstance();
		Weapon w2 = (Weapon)cat.classes[Random.chances( cat.probs )].getDeclaredConstructor().newInstance();
		
		w1.random();
		w2.random();

		return Math.abs( heroStrength - w1.STR ) < Math.abs( heroStrength - w2.STR ) ? w1 : w2;
	}
}
