package com.watabou.pixeldungeon.quests.spawnconfigs;

import com.watabou.pixeldungeon.levels.Level;
import com.watabou.pixeldungeon.quests.Quest;
import com.watabou.pixeldungeon.quests.QuestManager;
import com.watabou.utils.Bundlable;

/**
 * Manages which quests should be spawned in which condition or context.
 */
public interface QuestSpawnConfiguration extends Bundlable {

    /**
     * @return A quest selected by considering the arguments (Type of Level, etc.)
     */
    Quest<?> createQuest(int questID, int depth, Level level, QuestManager questManager);

    boolean isCityQuestSpawned();
}
