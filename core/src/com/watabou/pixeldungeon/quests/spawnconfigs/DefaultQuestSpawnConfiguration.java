package com.watabou.pixeldungeon.quests.spawnconfigs;

import com.watabou.pixeldungeon.Dungeon;
import com.watabou.pixeldungeon.levels.*;
import com.watabou.pixeldungeon.quests.*;
import com.watabou.pixeldungeon.quests.blacksmith.BloodQuest;
import com.watabou.pixeldungeon.quests.blacksmith.GoldQuest;
import com.watabou.pixeldungeon.quests.ghost.CurseQuest;
import com.watabou.pixeldungeon.quests.ghost.RatQuest;
import com.watabou.pixeldungeon.quests.ghost.RoseQuest;
import com.watabou.pixeldungeon.quests.imp.GolemQuest;
import com.watabou.pixeldungeon.quests.imp.MonkQuest;
import com.watabou.pixeldungeon.quests.wandmaker.BerryQuest;
import com.watabou.pixeldungeon.quests.wandmaker.DustQuest;
import com.watabou.pixeldungeon.quests.wandmaker.FishQuest;
import com.watabou.utils.Bundle;
import com.watabou.utils.Random;

/**
 * This quest spawn configuration should implement the quest spawning behaviour equal to the
 * default Pixel Dungeon experience
 */
public class DefaultQuestSpawnConfiguration implements QuestSpawnConfiguration {

    private boolean sewerQuestSpawned = false;
    private boolean prisonQuestSpawned = false;
    private boolean caveQuestSpawned = false;
    private boolean cityQuestSpawned = false;

    private static final String SEWER_QUEST_SPAWNED = "sewerquestspawned";
    private static final String PRISON_QUEST_SPAWNED = "prisonquestspawned";
    private static final String CAVE_QUEST_SPAWNED = "cavequestspawned";
    private static final String CITY_QUEST_SPAWNED = "cityquestspawned";

    @Override
    public void restoreFromBundle( Bundle bundle ) {
        sewerQuestSpawned = bundle.getBoolean( SEWER_QUEST_SPAWNED );
        prisonQuestSpawned = bundle.getBoolean( PRISON_QUEST_SPAWNED );
        caveQuestSpawned = bundle.getBoolean( CAVE_QUEST_SPAWNED );
        cityQuestSpawned = bundle.getBoolean( CITY_QUEST_SPAWNED );
    }

    @Override
    public void storeInBundle( Bundle bundle ) {
        bundle.put( SEWER_QUEST_SPAWNED, sewerQuestSpawned);
        bundle.put( PRISON_QUEST_SPAWNED, prisonQuestSpawned);
        bundle.put( CAVE_QUEST_SPAWNED, caveQuestSpawned);
        bundle.put( CITY_QUEST_SPAWNED, cityQuestSpawned);
    }

    public boolean isCityQuestSpawned() {
        return cityQuestSpawned;
    }

    /**
     * Decides which quest gets spawned at a particular level. The actual spawning happens later to
     * make the preparation of certain requirements (e.g. Rooms, etc.) possible
     * @param level The level that the quest gets spawned into
     * @return The created quest corresponding to the level, otherwise null if none should be spawned
     */
    @Override
    public Quest<?> createQuest(int questID, int depth, Level level, QuestManager questManager) {
        Quest<?> quest = null;
        if (level instanceof SewerLevel) {
            if (!sewerQuestSpawned && Dungeon.depth > 1 && Random.Int( 5 - Dungeon.depth ) == 0) {
                switch (Random.Int(3)) {
                    case 1:
                        quest = new RatQuest(questID, depth, questManager);
                        break;
                    case 2:
                        quest = new CurseQuest(questID, depth, questManager);
                        break;
                    default:  // case 0
                        quest = new RoseQuest(questID, depth, questManager);
                }
                sewerQuestSpawned = true;
            }
        } else if (level instanceof PrisonLevel) {
            if (!prisonQuestSpawned && Dungeon.depth > 6 && Random.Int( 10 - Dungeon.depth ) == 0) {
                switch (Random.Int(3)) {
                    case 1:
                        quest = new BerryQuest(questID, depth, questManager);
                        break;
                    case 2:
                        quest = new DustQuest(questID, depth, questManager);
                        break;
                    default:  // case 0
                        quest = new FishQuest(questID, depth, questManager);
                        // if the level has too much water terrain then choose one of the other
                        // quests to avoid wasting too much of the players time on searching the
                        // fish
                        int water = 0;
                        for (int i = 0; i < Level.LENGTH; i++) {
                            if (Level.isWater(i)) {
                                if (++water > Level.LENGTH / 16) {
                                    quest = Random.Int( 2 ) == 0 ?
                                            new BerryQuest(questID, depth, questManager) :
                                            new DustQuest(questID, depth, questManager);
                                    break;
                                }
                            }
                        }
                }
                prisonQuestSpawned = true;
            }
        } else if (level instanceof CavesLevel) {
            if (!caveQuestSpawned && Dungeon.depth > 11 && Random.Int( 15 - Dungeon.depth ) == 0) {
                if (Random.Int(2) == 1) {
                    quest = new GoldQuest(questID, depth, questManager);
                } else {
                    quest = new BloodQuest(questID, depth, questManager);
                }
                caveQuestSpawned = true;
            }
        } else if (level instanceof CityLevel) {
            if (!cityQuestSpawned && Dungeon.depth > 16 && Random.Int( 20 - Dungeon.depth ) == 0) {
                if (Random.Int(2) == 1) {
                    quest = new MonkQuest(questID, depth, questManager);
                } else {
                    quest = new GolemQuest(questID, depth, questManager);
                }
                cityQuestSpawned = true;
            }
        }
        return quest;
    }
}
