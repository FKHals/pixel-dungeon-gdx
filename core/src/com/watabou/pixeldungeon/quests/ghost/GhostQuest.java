package com.watabou.pixeldungeon.quests.ghost;

import com.watabou.pixeldungeon.Challenges;
import com.watabou.pixeldungeon.Dungeon;
import com.watabou.pixeldungeon.Journal;
import com.watabou.pixeldungeon.actors.Actor;
import com.watabou.pixeldungeon.actors.hero.Hero;
import com.watabou.pixeldungeon.actors.mobs.Crab;
import com.watabou.pixeldungeon.actors.mobs.Gnoll;
import com.watabou.pixeldungeon.actors.mobs.Mob;
import com.watabou.pixeldungeon.actors.mobs.Rat;
import com.watabou.pixeldungeon.actors.mobs.npcs.Ghost;
import com.watabou.pixeldungeon.actors.mobs.npcs.NPC;
import com.watabou.pixeldungeon.items.Generator;
import com.watabou.pixeldungeon.items.Item;
import com.watabou.pixeldungeon.items.armor.Armor;
import com.watabou.pixeldungeon.items.armor.ClothArmor;
import com.watabou.pixeldungeon.items.weapon.Weapon;
import com.watabou.pixeldungeon.items.weapon.missiles.MissileWeapon;
import com.watabou.pixeldungeon.levels.RegularLevel;
import com.watabou.pixeldungeon.quests.Quest;
import com.watabou.pixeldungeon.quests.QuestManager;
import com.watabou.pixeldungeon.scenes.GameScene;
import com.watabou.pixeldungeon.utils.GLog;
import com.watabou.pixeldungeon.windows.WndQuest;
import com.watabou.utils.Bundle;
import com.watabou.utils.Callback;
import com.watabou.utils.DLog;
import com.watabou.utils.EventSubject;

/**
 * This contains the base for the three Ghost-quests
 */
public abstract class GhostQuest extends Quest<Mob> {

    // This is needed because the reward should account for the potential strength
    // (Hero.STARTING_STR + Dungeon.potionOfStrength) that the hero had when STARTING (and not
    // when finishing) the quest because otherwise the player could just get a better reward by
    // getting the reward later than he actually did the quest.
    int curPotentialHeroStrength = Hero.STARTING_STR + Dungeon.potionOfStrength;

    public GhostQuest() {
        super();
    }

    public GhostQuest(int id, int depth, QuestManager questManager) {
        super(id, depth, questManager);
    }

    private static final String CURSTRENGTH	= "curstrength";

    @Override
    public void storeInBundle( Bundle bundle ) {
        super.storeInBundle( bundle );
        bundle.put( CURSTRENGTH, curPotentialHeroStrength );
    }

    @Override
    public void restoreFromBundle( Bundle bundle ) {
        super.restoreFromBundle( bundle );
        curPotentialHeroStrength = bundle.getInt( CURSTRENGTH );
    }

    @Override
    public NPC onSpawn( RegularLevel level ) {
        Ghost ghost = new Ghost();
        do {
            ghost.pos = level.randomRespawnCell();
        } while (ghost.pos == -1);
        level.addMob(ghost);
        Actor.occupyCell(ghost);
        ghost.setQuest(this);
        addToLevelKillEvent(level);
        return ghost;
    }

    abstract public boolean onGive(NPC giver);

    @Override
    public boolean give(NPC giver) {
        Journal.add( Journal.Feature.GHOST );
        return onGive(giver);
    }

    public Callback repeatText(final NPC giver, final String dialogueText) {
        return new Callback() {
            @Override
            public void call() {
                GameScene.show( new WndQuest( giver, dialogueText ) );
                try {
                    ((Ghost) giver).relocate();
                } catch(ClassCastException e) {
                    DLog.debug("QUEST", "Could not relocate ghost in quest " + id + " ("
                            + this.getClass().getName() + ")");
                }
            }
        };
    }

    private static final String TXT_WEAPON	= "Ghost's weapon";
    private static final String TXT_ARMOR	= "Ghost's armor";

    protected abstract void onReward();

    public void reward(final NPC questGiver, final String rewardText) {
        GameScene.show(new WndQuest(questGiver, rewardText, TXT_WEAPON, TXT_ARMOR) {
            @Override
            protected void onSelect( int index ) {
                onReward();

                Item reward = index == 0 ? getWeaponReward() : getArmorReward();
                if (reward.doPickUp( Dungeon.hero )) {
                    GLog.i( Hero.TXT_YOU_NOW_HAVE, reward.name() );
                } else {
                    Dungeon.level.drop( reward, questGiver.pos ).sprite.drop();
                }

                questGiver.yell( "Farewell, adventurer!" );
                questGiver.die( null );

                // Since this method triggers only by TouchEvents, it is not possible for the quest to wait for the
                // answer if the reward has been taken or not. Instead the next/REWARDED event must be triggered here.
                stage = Stage.REWARDED;
                interact(questGiver);
            }
        });
    }

    @Override
    public void onNotify(Mob mob, EventSubject<Mob> event) {
        if (mob instanceof Rat || mob instanceof Crab || mob instanceof Gnoll) {
            if (stage == Stage.GIVEN) {
                processSewersKill(mob.pos, event);
            }
        }
    }

    protected void processSewersKill(int pos, EventSubject<Mob> event) {
        // Do nothing, just for (optional) overriding by subclasses
    }

    //TODO The methods could be moved to the the Generator class for better encapsulation. For example
    //     something like:
    //     randomWeaponBestOfExcluding(int heroStrength, int numOfTries, Generator.Category excludedWeaponCategory)
    protected Weapon getWeaponReward() {
        Weapon weapon = null;
        for (int i=0; i < 4; i++) {
            Item another;
            do {
                another = Generator.random( Generator.Category.WEAPON, curPotentialHeroStrength );
            } while (another instanceof MissileWeapon);

            if (weapon == null || another.level() > weapon.level()) {
                weapon = (Weapon)another;
            }
        }
        weapon.identify();
        return weapon;
    }

    protected Armor getArmorReward() {
        Armor armor;
        if (Dungeon.isChallenged( Challenges.NO_ARMOR )) {
            armor = (Armor)new ClothArmor().degrade();
        } else {
            armor = (Armor)Generator.random( Generator.Category.ARMOR, curPotentialHeroStrength );
            for (int i=0; i < 3; i++) {
                Item another = Generator.random( Generator.Category.ARMOR, curPotentialHeroStrength );
                if (another.level() > armor.level()) {
                    armor = (Armor)another;
                }
            }
        }
        armor.identify();
        return armor;
    }

    @Override
    public void finish() {
        Journal.remove( Journal.Feature.GHOST );
    }
}
