/*
 * Pixel Dungeon
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.watabou.pixeldungeon.quests.ghost;

import com.watabou.pixeldungeon.Dungeon;
import com.watabou.pixeldungeon.actors.mobs.*;
import com.watabou.pixeldungeon.actors.mobs.npcs.NPC;
import com.watabou.pixeldungeon.items.Item;
import com.watabou.pixeldungeon.items.quest.DriedRose;
import com.watabou.pixeldungeon.levels.RegularLevel;
import com.watabou.pixeldungeon.quests.QuestGoal;
import com.watabou.pixeldungeon.quests.QuestManager;
import com.watabou.pixeldungeon.scenes.GameScene;
import com.watabou.pixeldungeon.windows.WndQuest;
import com.watabou.utils.Bundle;
import com.watabou.utils.Callback;
import com.watabou.utils.EventSubject;
import com.watabou.utils.Random;

import java.util.Arrays;
import java.util.LinkedList;

public class RoseQuest extends GhostQuest {

    int left2kill = -1;

    public RoseQuest() {
        super();
    }

    public RoseQuest(int id, int depth, QuestManager questManager) {
        super(id, depth, questManager);
    }

    private static final String TXT_ROSE1	=
            "Hello adventurer... Once I was like you - strong and confident... " +
                    "And now I'm dead... But I can't leave this place... Not until I have my _dried rose_... " +
                    "It's very important to me... Some monster stole it from my body...";

    private static final String TXT_ROSE2	=
            "Please... Help me... _Find the rose_...";

    private static final String TXT_ROSE3	=
            "Yes! Yes!!! This is it! Please give it to me! " +
                    "And you can take one of these items, maybe they " +
                    "will be useful to you in your journey...";

    private static final String LEFT2KILL	= "left2kill";

    @Override
    public void storeInBundle( Bundle bundle ) {
        super.storeInBundle( bundle );
        bundle.put( LEFT2KILL, left2kill );
    }

    @Override
    public void restoreFromBundle( Bundle bundle ) {
        super.restoreFromBundle( bundle );
        left2kill = bundle.getInt( LEFT2KILL );
    }

    @Override
    protected LinkedList<QuestGoal> createQuestGoals() {
        QuestGoal roseDropped = new QuestGoal() {
            @Override
            public boolean isReached(NPC giver) {
                return left2kill == 0;
            }
        };
        QuestGoal roseInInventory = new QuestGoal() {
            @Override
            public boolean isReached(NPC giver) {
                Item driedRose = Dungeon.hero.belongings.getItem(DriedRose.class);
                return (driedRose != null);
            }
        };
        return new LinkedList<>(Arrays.asList(roseDropped, roseInInventory));
    }

    @Override
    public NPC onSpawn( RegularLevel level ) {
        NPC ghost = super.onSpawn(level);
        left2kill = 8;
        return ghost;
    }

    @Override
    public boolean onGive(NPC giver) {
        GameScene.show( new WndQuest( giver, TXT_ROSE1 ) );
        return true;
    }

    //FIXME the final NPC giver seems to cause a problem where only one ghost will actually vanish and the second one
    // just shoes the window but does not teleport for some reason
    @Override
    public Callback repeat(final NPC giver) {
        return repeatText(giver, TXT_ROSE2);
    }

    @Override
    protected void onReward() {
        Item questItem = Dungeon.hero.belongings.getItem(DriedRose.class);
        if (questItem != null) {
            questItem.detach( Dungeon.hero.belongings.backpack );
        }
    }

    @Override
    public void reward(final NPC questGiver) {
        super.reward(questGiver, TXT_ROSE3);
    }

    /**
     * Counts the number of killed mobs (of a specific depth) and drops the quest objective after
     * the target number of kills has been reached.
     * @param pos The position of the killed mob
     * @param event The event that triggered this method
     */
    public void processSewersKill( int pos, EventSubject<Mob> event) {
        if (Random.Int( left2kill ) == 0) {
            Dungeon.level.drop( new DriedRose(), pos ).sprite.drop();
            event.removeObserver(this);
            // this assures that the quest is only finished if the rose has been spawned so
            // that a rose gets spawned in any case (to avoid cross-quest-problems)
            left2kill = 0;
        } else {
            left2kill--;
        }
    }
}
