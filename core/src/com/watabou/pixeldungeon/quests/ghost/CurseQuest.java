package com.watabou.pixeldungeon.quests.ghost;

import com.watabou.noosa.audio.Sample;
import com.watabou.pixeldungeon.Assets;
import com.watabou.pixeldungeon.Dungeon;
import com.watabou.pixeldungeon.actors.mobs.Mob;
import com.watabou.pixeldungeon.actors.mobs.CursePersonification;
import com.watabou.pixeldungeon.actors.mobs.npcs.Ghost;
import com.watabou.pixeldungeon.actors.mobs.npcs.NPC;
import com.watabou.pixeldungeon.effects.particles.ShadowParticle;
import com.watabou.pixeldungeon.quests.QuestGoal;
import com.watabou.pixeldungeon.quests.QuestManager;
import com.watabou.pixeldungeon.scenes.GameScene;
import com.watabou.pixeldungeon.utils.Utils;
import com.watabou.pixeldungeon.windows.WndQuest;
import com.watabou.utils.Bundle;
import com.watabou.utils.Callback;
import com.watabou.utils.EventSubject;

import java.util.Arrays;
import java.util.LinkedList;

public class CurseQuest extends GhostQuest {

    boolean isCurseKilled = false;

    public CurseQuest() {
        super();
    }

    public CurseQuest(int id, int depth, QuestManager questManager) {
        super(id, depth, questManager);
    }

    private static final String TXT_CURSE1 =
            "Hello adventurer... Once I was like you - strong and confident... " +
                    "And now I'm dead... But I can't leave this place, as I am bound by a horrid curse... " +
                    "Please... Help me... _Destroy the curse_...";
    private static final String TXT_CURSE2 =
            "Thank you, %s! The curse is broken and I can finally rest... " +
                    "Please take one of these items, maybe they " +
                    "will be useful to you in your journey...";

    private static final String TXT_YES	= "Yes, I will do it for you";
    private static final String TXT_NO	= "No, I can't help you";

    private static final String CURSEKILLED	= "cursekilled";

    @Override
    public void storeInBundle( Bundle bundle ) {
        super.storeInBundle( bundle );
        bundle.put( CURSEKILLED, isCurseKilled );
    }

    @Override
    public void restoreFromBundle( Bundle bundle ) {
        super.restoreFromBundle( bundle );
        isCurseKilled = bundle.getBoolean( CURSEKILLED );
    }

    @Override
    protected LinkedList<QuestGoal> createQuestGoals() {
        QuestGoal curseKilled = new QuestGoal() {
            @Override
            public boolean isReached(NPC giver) {
                return isCurseKilled;
            }
        };
        return new LinkedList<>(Arrays.asList(curseKilled));
    }

    @Override
    public boolean onGive(final NPC giver) {
        GameScene.show( new WndQuest( giver, TXT_CURSE1, TXT_YES, TXT_NO ) {
            protected void onSelect( int index ) {
                if (index == 0) {
                    stage = Stage.GIVEN;

                    CursePersonification d = new CursePersonification();
                    Ghost.replace( giver, d );

                    d.sprite.emitter().burst( ShadowParticle.CURSE, 5 );
                    Sample.INSTANCE.play( Assets.SND_GHOST );

                    Dungeon.hero.next();
                } else {
                    try {
                        ((Ghost) giver).relocate();
                    } catch(ClassCastException e) {
                        // do nothing
                        System.out.println("no ghost? " + giver);
                    }
                }
            }
        } );
        // if it progresses to the next stage is decided in the window in this case and not by the
        // returned boolean
        return false;
    }

    @Override
    public Callback repeat(final NPC giver) {
        // does not happen since the ghost is replaced by its CursePersonification in this stage
        return null;
    }

    /**
     * Processes the killing of the CursePersonification and replace it with the Ghost quest-giver
     */
    @Override
    public void onNotify(Mob mob, EventSubject<Mob> event) {
        if (mob instanceof CursePersonification) {
            if (stage == Stage.GIVEN) {
                isCurseKilled = true;
                Ghost ghost = new Ghost();
                ghost.setQuest(this);
                ghost.state = ghost.PASSIVE;
                // the observer removal must be called before Ghost.replace() because it also calls
                // destroy() on the CursePersonification which leads to a StackOverflowError
                event.removeObserver(this);
                Ghost.replace( mob, ghost );
            }
        }
    }

    @Override
    protected void onReward() {
        // do nothing
    }

    @Override
    public void reward(final NPC questGiver) {
        super.reward(questGiver, Utils.format( TXT_CURSE2, Dungeon.hero.className() ));
    }
}
