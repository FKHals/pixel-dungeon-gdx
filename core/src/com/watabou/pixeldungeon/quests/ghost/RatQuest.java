package com.watabou.pixeldungeon.quests.ghost;

import com.watabou.pixeldungeon.Dungeon;
import com.watabou.pixeldungeon.actors.mobs.Mob;
import com.watabou.pixeldungeon.actors.mobs.FetidRat;
import com.watabou.pixeldungeon.actors.mobs.npcs.NPC;
import com.watabou.pixeldungeon.items.Item;
import com.watabou.pixeldungeon.items.quest.RatSkull;
import com.watabou.pixeldungeon.quests.QuestGoal;
import com.watabou.pixeldungeon.quests.QuestManager;
import com.watabou.pixeldungeon.scenes.GameScene;
import com.watabou.pixeldungeon.windows.WndQuest;
import com.watabou.utils.Bundle;
import com.watabou.utils.Callback;
import com.watabou.utils.EventSubject;

import java.util.Arrays;
import java.util.LinkedList;

public class RatQuest extends GhostQuest {

    boolean isRatSpawned = false;

    private static final String TXT_RAT1	=
            "Hello adventurer... Once I was like you - strong and confident... " +
                    "And now I'm dead... But I can't leave this place... Not until I have my revenge... " +
                    "Slay the _fetid rat_, that has taken my life...";

    private static final String TXT_RAT2	=
            "Please... Help me... _Slay the abomination_...";

    private static final String TXT_RAT3	=
            "Yes! The ugly creature is slain and I can finally rest... " +
                    "Please take one of these items, maybe they " +
                    "will be useful to you in your journey...";

    public RatQuest() {
        super();
    }

    public RatQuest(int id, int depth, QuestManager questManager) {
        super(id, depth, questManager);
    }

    private static final String IS_RAT_SPAWNED	= "isratspawned";

    @Override
    public void storeInBundle( Bundle bundle ) {
        super.storeInBundle( bundle );
        bundle.put( IS_RAT_SPAWNED, isRatSpawned );
    }

    @Override
    public void restoreFromBundle( Bundle bundle ) {
        super.restoreFromBundle( bundle );
        isRatSpawned = bundle.getBoolean( IS_RAT_SPAWNED );
    }

    @Override
    protected LinkedList<QuestGoal> createQuestGoals() {
        QuestGoal ratSpawned = new QuestGoal() {
            @Override
            public boolean isReached(NPC giver) {
                return isRatSpawned;
            }
        };
        QuestGoal skullInInventory = new QuestGoal() {
            @Override
            public boolean isReached(NPC giver) {
                Item ratSkull = Dungeon.hero.belongings.getItem(RatSkull.class);
                return (ratSkull != null);
            }
        };
        return new LinkedList<>(Arrays.asList(ratSpawned, skullInInventory));
    }

    @Override
    public boolean onGive(NPC giver) {
        GameScene.show( new WndQuest( giver, TXT_RAT1 ) );
        return true;
    }

    @Override
    public Callback repeat(final NPC giver) {
        return repeatText(giver, TXT_RAT2);
    }

    @Override
    protected void onReward() {
        Item questItem = Dungeon.hero.belongings.getItem(RatSkull.class);
        if (questItem != null) {
            questItem.detach( Dungeon.hero.belongings.backpack );
        }
    }

    @Override
    public void reward(final NPC questGiver) {
        super.reward(questGiver, TXT_RAT3);
    }

    /**
     * Tries to spawn a FetidRat when another enemy of a specific depth has been killed.
     * @param pos The position of the killed mob
     * @param event The event that triggered this method
     */
    public void processSewersKill( int pos, EventSubject<Mob> event) {
        FetidRat rat = new FetidRat();
        rat.pos = Dungeon.level.randomRespawnCell();
        if (rat.pos != -1) {
            GameScene.add( rat );
            isRatSpawned = true;
            event.removeObserver(this);
        }
    }
}
