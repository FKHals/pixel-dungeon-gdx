package com.watabou.pixeldungeon.quests;

import com.watabou.pixeldungeon.Dungeon;
import com.watabou.utils.EventSubject;

public class QuestEvent<E> extends EventSubject<E> {

    @Override
    public int getId(Observer<E> observer) {
        return observer.getId();
    }

    /**
     * @param observerId The id of the quest which should be found
     * @return The quest corresponding to the given id, otherwise null
     */
    @Override
    public Observer<E> findById(int observerId) {
        return (Observer<E>) Dungeon.questManager.findById(observerId);
    }
}
