package com.watabou.pixeldungeon.quests;

import com.watabou.pixeldungeon.levels.Level;
import com.watabou.pixeldungeon.levels.RegularLevel;
import com.watabou.pixeldungeon.levels.Room;
import com.watabou.pixeldungeon.quests.imp.ImpQuest;
import com.watabou.pixeldungeon.quests.spawnconfigs.DefaultQuestSpawnConfiguration;
import com.watabou.pixeldungeon.quests.spawnconfigs.QuestSpawnConfiguration;
import com.watabou.utils.DLog;
import com.watabou.utils.Bundlable;
import com.watabou.utils.Bundle;
import com.watabou.utils.SparseArray;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;

/**
 * Stores and manages quests including spawning etc.
 */
public class QuestManager implements Bundlable {

    private HashSet<Quest<?>> all = new HashSet<>();
    private SparseArray<Quest<?>> ids = new SparseArray<>();
    // maps depths to the corresponding quest-ids
    private SparseArray<HashSet<Integer>> depths = new SparseArray<>();

    private static final String ALL = "all";
    private static final String SPAWN_CONFIG = "questspawnconfig";

    QuestSpawnConfiguration spawnConfiguration = new DefaultQuestSpawnConfiguration();

    @Override
    public void restoreFromBundle( Bundle bundle ) {
        all = new HashSet<>();
        ids = new SparseArray<>();
        depths = new SparseArray<>();
        Collection<Bundlable> collection = bundle.getCollection( ALL );
        for (Bundlable q : collection) {
            Quest<?> quest = (Quest<?>)q;
            if (quest != null) {
                quest.setQuestManager(this);
                add( quest );
            }
        }
        spawnConfiguration = (DefaultQuestSpawnConfiguration) bundle.get( SPAWN_CONFIG );
    }

    @Override
    public void storeInBundle( Bundle bundle ) {
        bundle.put( ALL, all);
        bundle.put( SPAWN_CONFIG, spawnConfiguration);
    }

    public void clear() {
        all.clear();
        ids.clear();
        depths.clear();
    }

    /**
     * Adds a quest to the corresponding depth-dependencies (so that it gets spawned or registered
     * as an Observer depending on the quest preferences)
     *
     * This method is idempotent because of the HashSet internally used by the `depths`-field.
     */
    private void addQuestToDepth(int questId, int depth) {
        HashSet<Integer> idsOfDepth = depths.get(depth, new HashSet<Integer>());
        idsOfDepth.add(questId);
        depths.put(depth, idsOfDepth);
        DLog.debug("QUEST", "Quest " + questId + " added to depth " + depth + ". "
                + "Current depth dependencies (depth -> quest-id): " + depths);
    }

    public void add( Quest<?> quest ) {
        if (quest != null) {
            all.add(quest);
            if (quest.id > 0) {
                ids.put(quest.id, quest);
                addQuestToDepth(quest.id, quest.depth);
                // add the Quest to all depths that it observes so that it can be added as a
                // listener to the corresponding level-events
                for (int depth: quest.getObservedDepths()) {
                    addQuestToDepth(quest.id, depth);
                }
            }
            DLog.debug("QUEST", "Quest " + quest.id + " added. Currently active quests (by ID): " + ids);
        }
    }

    /**
     * Removes a quest to the corresponding depth-dependencies.
     *
     * This does NOT DIRECTLY remove the quest from the list of observers of events at other levels
     * BUT since the events get restored when loading a level, the restoreFromBundle() function
     * filters out observers that are null which is exactly what the used {@link QuestManager#findById(int)}
     * returns if the quest was finished and removed from the Quest-list ({@link QuestManager#all}).
     * As a result an explicit removal of the quests from the level-event-observer-lists is not
     * necessary.
     *
     * This method is idempotent because of the HashSet internally used by the `depths`-field.
     */
    private void removeQuestFromDepth(int questId, int depth) {
        HashSet<Integer> idsOfDepth = depths.get(depth, new HashSet<Integer>());
        idsOfDepth.remove(questId);
        depths.put(depth, idsOfDepth);
        DLog.debug("QUEST", "Quest " + questId + " removed from depth " + depth + ". "
                + "Current depth dependencies (depth -> quest-id): " + depths);
    }

    public void remove( Quest<?> quest ) {
        if (quest != null) {
            all.remove( quest );
            if (quest.id > 0) {
                ids.remove( quest.id );
                removeQuestFromDepth(quest.id, quest.depth);
                for (int depth: quest.getObservedDepths()) {
                    removeQuestFromDepth(quest.id, depth);
                }
            }
            DLog.debug("QUEST", "Quest " + quest.id + " removed. Currently active quests (by ID): " + ids);
        }
    }

    /**
     * Decides which quest gets spawned at a particular level and initialize without actually
     * spawning them.
     * This is needed since the quest needs to exist to be checked for the required rooms in
     * {@link #getRoomsToSpawn(int)} before generating the level which should incorporate that
     * required rooms.
     * @see #spawnQuests(RegularLevel, int)
     * @param level The level that the quest gets spawned into
     */
    public void initQuest(Level level, int depth) {
        int questID = generateId();
        Quest<?> quest = spawnConfiguration.createQuest(questID, depth, level, this);
        add(quest);
        DLog.debug("QUEST", "Active quests (by ID): " + ids);
    }

    // FIXME This should be changed: Possibly by changing the signature of
    //  spawnQuests(RegularLevel) to spawnQuests(Level) which would require all Quests to be
    //  compatible with Level instead of RegularLevel (which currently would not work e.g. for
    //  quests.blacksmith.BlacksmithQuest.onSpawn(RegularLevel) which depends on knowledge of
    //  the rooms of a RegularLevel which is not available in Level) therefore Level would have
    //  to be changed accordingly.
    /**
     * Spawn the quests (or observers of the quests) for a particular level and depth.
     * @see Quest#spawn(RegularLevel)
     */
    public void spawnQuests(RegularLevel level, int depth) {
        for (Quest<?> quest: findByDepth(depth)) {
            quest.spawn(level);
        }
    }

    /**
     * Generates a unique ID to identify quests.
     * The IDs are only unique in the same Level/depth and not across Levels.
     * @return Unique quest ID
     */
    public int generateId() {
        int max = 0;
        for (Quest<?> q : all) {
            if (q.id > max) {
                max = q.id;
            }
        }
        return max + 1;
    }

    /**
     * Returns a quest found by it's ID
     * @param id The unique quest-ID to identify which quest to return
     * @return The quest with the corresponding ID otherwise null if no quest of that particular ID
     * is found.
     */
    public Quest<?> findById( int id ) {
        return ids.get( id );
    }

    /**
     * @param depth The depth that the quests are spawned in
     * @return A list of the quests that belong to a given depth
     */
    public HashSet<Quest<?>> findByDepth( int depth ) {
        HashSet<Quest<?>> questsOfDepth = new HashSet<>();
        for (int questId: depths.get( depth, new HashSet<Integer>() )) {
            questsOfDepth.add(findById(questId));
        }
        return questsOfDepth;
    }

    public HashSet<Quest<?>> all() {
        return all;
    }

    public LinkedList<Room.Type> getRoomsToSpawn(int depth) {
        LinkedList<Room.Type> roomsToSpawn = new LinkedList<>();
        for (Quest<?> quest: findByDepth(depth)) {
            roomsToSpawn.addAll(quest.getRequiredRooms());
        }
        return roomsToSpawn;
    }

    /**
     * @return If the Imp quest (in a CityLevel) has been completed (which is the case if it has
     * been spawned and already got deleted by finishing it)
     */
    public boolean isImpQuestCompleted() {
        boolean isImpQuestActive = false;
        for (Quest<?> quest : all) {
            if (quest instanceof ImpQuest) {
                isImpQuestActive = true;
                break;
            }
        }
        return spawnConfiguration.isCityQuestSpawned() && !isImpQuestActive;
    }
}
