package com.watabou.pixeldungeon.quests.imp;

import com.watabou.pixeldungeon.actors.mobs.Mob;
import com.watabou.pixeldungeon.actors.mobs.Monk;
import com.watabou.pixeldungeon.quests.QuestManager;

public class MonkQuest extends ImpQuest {

    public MonkQuest() {
        super();
    }

    public MonkQuest(int id, int depth, QuestManager questManager) {
        super(id, depth, questManager);
    }

    @Override
    protected int getQuestItemQuantity() {
        return 8;
    }

    protected Class<? extends Mob> getTargetMobClass() {
        return Monk.class;
    }

    @Override
    protected String getGiveText() {
        return "In my case this is _monks_ who need to be killed. You see, I'm going to start a "
                + "little business here, but these lunatics don't buy anything themselves and "
                + "will scare away other customers. "
                + "So please, kill... let's say _" + getQuestItemQuantity() + " of them_ and a "
                + "reward is yours.";
    }

    @Override
    protected String getRepeatText() {
        return "Oh, you are still alive! I knew that your kung-fu is stronger ;) "
                + "Just don't forget to grab these monks' tokens.";
    }
}
