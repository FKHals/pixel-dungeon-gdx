package com.watabou.pixeldungeon.quests.imp;

import com.watabou.pixeldungeon.actors.mobs.Golem;
import com.watabou.pixeldungeon.actors.mobs.Mob;
import com.watabou.pixeldungeon.quests.QuestManager;

public class GolemQuest extends ImpQuest {

    public GolemQuest() {
        super();
    }

    public GolemQuest(int id, int depth, QuestManager questManager) {
        super(id, depth, questManager);
    }

    @Override
    protected int getQuestItemQuantity() {
        return 6;
    }

    protected Class<? extends Mob> getTargetMobClass() {
        return Golem.class;
    }

    @Override
    protected String getGiveText() {
        return "In my case this is _golems_ who need to be killed. You see, I'm going to start a "
                + "little business here, but these stupid golems are bad for business! "
                + "It's very hard to negotiate with wandering lumps of granite, damn them! "
                + "So please, kill... let's say _" + getQuestItemQuantity() + " of them_ and a "
                + "reward is yours.";
    }

    @Override
    protected String getRepeatText() {
        return "How is your golem safari going?";
    }
}
