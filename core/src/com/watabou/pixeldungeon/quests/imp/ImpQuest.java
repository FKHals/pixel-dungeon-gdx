package com.watabou.pixeldungeon.quests.imp;

import com.watabou.noosa.BitmapTextMultiline;
import com.watabou.pixeldungeon.Dungeon;
import com.watabou.pixeldungeon.Journal;
import com.watabou.pixeldungeon.actors.Actor;
import com.watabou.pixeldungeon.actors.hero.Hero;
import com.watabou.pixeldungeon.actors.mobs.Mob;
import com.watabou.pixeldungeon.actors.mobs.npcs.Imp;
import com.watabou.pixeldungeon.actors.mobs.npcs.NPC;
import com.watabou.pixeldungeon.items.Generator;
import com.watabou.pixeldungeon.items.Item;
import com.watabou.pixeldungeon.items.quest.DwarfToken;
import com.watabou.pixeldungeon.items.rings.Ring;
import com.watabou.pixeldungeon.levels.RegularLevel;
import com.watabou.pixeldungeon.quests.Quest;
import com.watabou.pixeldungeon.quests.QuestGoal;
import com.watabou.pixeldungeon.quests.QuestManager;
import com.watabou.pixeldungeon.scenes.GameScene;
import com.watabou.pixeldungeon.scenes.PixelScene;
import com.watabou.pixeldungeon.sprites.ItemSprite;
import com.watabou.pixeldungeon.ui.RedButton;
import com.watabou.pixeldungeon.ui.Window;
import com.watabou.pixeldungeon.utils.GLog;
import com.watabou.pixeldungeon.utils.Utils;
import com.watabou.pixeldungeon.windows.IconTitle;
import com.watabou.pixeldungeon.windows.WndQuest;
import com.watabou.utils.Callback;
import com.watabou.utils.EventSubject;

import java.util.Arrays;
import java.util.LinkedList;

public abstract class ImpQuest extends Quest<Mob> {

    public ImpQuest() {
        super();
    }

    public ImpQuest(int id, int depth, QuestManager questManager) {
        super(id, depth, questManager);
    }

    /**
     * @return The number of quest items that at least must be in the players posession to satisfy
     * the quest goal
     */
    abstract protected int getQuestItemQuantity();

    @Override
    protected LinkedList<QuestGoal> createQuestGoals() {
        QuestGoal itemInInventory = new QuestGoal() {
            @Override
            public boolean isReached(NPC giver) {
                Item questItem = Dungeon.hero.belongings.getItem(DwarfToken.class);
                return (questItem != null && questItem.quantity() >= getQuestItemQuantity());
            }
        };
        return new LinkedList<>(Arrays.asList(itemInInventory));
    }

    @Override
    public NPC onSpawn( RegularLevel level ) {
        Imp npc = new Imp();
        do {
            npc.pos = level.randomRespawnCell();
        } while (npc.pos == -1 || level.heaps.get( npc.pos ) != null);
        level.addMob( npc );
        Actor.occupyCell( npc );
        npc.setQuest(this);
        addToLevelKillEvent(level);
        return npc;
    }

    private void tell( NPC giver, String format, Object...args ) {
        GameScene.show( new WndQuest( giver, Utils.format( format, args ) ) );
    }

    abstract protected String getGiveText();

    private static final String TXT_GIVE_REASON =
            "Are you an adventurer? I love adventurers! You can always rely on them if something "
            + "needs to be killed. Am I right? For a bounty, of course ;)\n";

    @Override
    public boolean give(NPC giver) {
        Journal.add( Journal.Feature.IMP );
        tell( giver, TXT_GIVE_REASON + getGiveText() );
        return true;
    }

    abstract protected String getRepeatText();

    public Callback repeat(final NPC giver) {
        return new Callback() {
            @Override
            public void call() {
                tell( giver, getRepeatText(), Dungeon.hero.className() );
            }
        };
    }

    /**
     * @return The depths of the levels that should be observed by this quest
     */
    @Override
    public LinkedList<Integer> getObservedDepths() {
        return new LinkedList<>(Arrays.asList(16, 17, 18, 19));
    }

    abstract Class<? extends Mob> getTargetMobClass();

    @Override
    public void onNotify(Mob mob, EventSubject<Mob> event) {
        if (stage == Stage.GIVEN) {
            if (getTargetMobClass().isInstance(mob)) {
                Dungeon.level.drop( new DwarfToken(), mob.pos ).sprite.drop();
            }
        }
    }

    @Override
    public void reward(final NPC questGiver) {
        Dungeon.level.getMobKillEvent().removeObserver(this);
        DwarfToken questItem = Dungeon.hero.belongings.getItem(DwarfToken.class);
        GameScene.show(new WndImpReward(questGiver, questItem));
    }

    protected static Item getRingReward() {
        Ring reward;
        do {
            reward = (Ring) Generator.random( Generator.Category.RING );
        } while (reward.cursed);
        reward.upgrade( 2 );
        reward.cursed = true;
        return reward;
    }

    @Override
    public void finish() {
        Journal.remove( Journal.Feature.IMP );
    }

    private class WndImpReward extends Window {

        private static final String TXT_MESSAGE = "Oh yes! You are my hero!\n"
            + "Regarding your reward, I don't have cash with me right now, but I have something "
            + "better for you. "
            + "This is my family heirloom ring: my granddad took it off a dead paladin's finger.";
        private static final String TXT_REWARD = "Take the ring";

        private static final int WIDTH = 120;
        private static final int BTN_HEIGHT = 20;
        private static final int GAP = 2;

        public WndImpReward( final NPC questGiver, final DwarfToken tokens) {

            super();

            IconTitle titlebar = new IconTitle();
            titlebar.icon( new ItemSprite( tokens.image(), null ) );
            titlebar.label( Utils.capitalize( tokens.name() ) );
            titlebar.setRect( 0, 0, WIDTH, 0 );
            add( titlebar );

            BitmapTextMultiline message = PixelScene.createMultiline( TXT_MESSAGE, 6 );
            message.maxWidth = WIDTH;
            message.measure();
            message.y = titlebar.bottom() + GAP;
            add( message );

            RedButton btnReward = new RedButton( TXT_REWARD ) {
                @Override
                protected void onClick() {
                    takeReward( questGiver, tokens );
                }
            };
            btnReward.setRect( 0, message.y + message.height() + GAP, WIDTH, BTN_HEIGHT );
            add( btnReward );

            resize( WIDTH, (int)btnReward.bottom() );
        }

        private void takeReward( NPC questGiver, DwarfToken tokens ) {

            hide();

            tokens.detachAll( Dungeon.hero.belongings.backpack );

            Item reward = ImpQuest.getRingReward();
            reward.identify();
            if (reward.doPickUp( Dungeon.hero )) {
                GLog.i( Hero.TXT_YOU_NOW_HAVE, reward.name() );
            } else {
                Dungeon.level.drop( reward, questGiver.pos ).sprite.drop();
            }

            ((Imp) questGiver).flee();

            // Since this method triggers only by TouchEvents, it is not possible for the quest to wait for the
            // answer if the reward has been taken or not. Instead the next/REWARDED event must be triggered here.
            stage = Stage.REWARDED;
            interact(questGiver);
        }
    }
}
