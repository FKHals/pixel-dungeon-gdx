package com.watabou.pixeldungeon.quests.blacksmith;

import com.watabou.pixeldungeon.Dungeon;
import com.watabou.pixeldungeon.Journal;
import com.watabou.pixeldungeon.actors.Actor;
import com.watabou.pixeldungeon.actors.hero.Hero;
import com.watabou.pixeldungeon.actors.mobs.npcs.Blacksmith;
import com.watabou.pixeldungeon.actors.mobs.npcs.NPC;
import com.watabou.pixeldungeon.items.quest.Pickaxe;
import com.watabou.pixeldungeon.levels.RegularLevel;
import com.watabou.pixeldungeon.levels.Room;
import com.watabou.pixeldungeon.quests.Quest;
import com.watabou.pixeldungeon.quests.QuestManager;
import com.watabou.pixeldungeon.scenes.GameScene;
import com.watabou.pixeldungeon.utils.GLog;
import com.watabou.pixeldungeon.windows.WndReforge;
import com.watabou.pixeldungeon.windows.WndQuest;
import com.watabou.utils.Bundle;
import com.watabou.utils.Callback;
import com.watabou.utils.Optional;
import com.watabou.utils.Random;

import java.util.Arrays;
import java.util.LinkedList;

public abstract class BlacksmithQuest extends Quest<Object> {

    protected static final String TXT_MISSING_PICKAXE =
            "Are you kiddin' me? Where is my pickaxe?!";

    // TODO: This could also be refactored into a general DONE Quest.Stage.
    // If the reforging reward is available (if the given task ist completed)
    public boolean isReforgingAvailable = false;

    public BlacksmithQuest() {
        super();
    }

    public BlacksmithQuest(int id, int depth, QuestManager questManager) {
        super(id, depth, questManager);
    }

    private static final String HAS_REFORGED = "hasreforged";

    @Override
    public void storeInBundle( Bundle bundle ) {
        super.storeInBundle( bundle );
        bundle.put( HAS_REFORGED, isReforgingAvailable);
    }

    @Override
    public void restoreFromBundle( Bundle bundle ) {
        super.restoreFromBundle( bundle );
        isReforgingAvailable = bundle.getBoolean( HAS_REFORGED );
    }

    @Override
    protected LinkedList<Room.Type> createRequiredRooms() {
        return new LinkedList<>(Arrays.asList(Room.Type.BLACKSMITH));
    }

    @Override
    public NPC onSpawn(RegularLevel level ) {
        Optional<Room> spawnRoom = level.randomRoomOfType(Room.Type.BLACKSMITH, new Random());
        if (!spawnRoom.isPresent()) {
            throw new AssertionError("Blacksmith spawn room is null which should never happen since"
                    + "all the required rooms should be spawned due to createPrerequisiteRooms()");
        }
        Blacksmith npc = new Blacksmith();
        do {
            npc.pos = spawnRoom.get().random( 1, level, new Random());
        } while (level.heaps.get( npc.pos ) != null);
        level.addMob( npc );
        Actor.occupyCell( npc );
        npc.setQuest(this);
        return npc;
    }

    private void tell( NPC giver, String text ) {
        GameScene.show( new WndQuest( giver, text ) );
    }

    abstract protected String getGiveText();

    private static final String TXT_GIVE_END =
            " What do you mean, how am I gonna pay? You greedy...\n"
            + "Ok, ok, I don't have money to pay, but I can do some smithin' for you. Consider "
            + "yourself lucky, I'm the only blacksmith around.";

    @Override
    public boolean give(NPC giver) {
        tell( giver, getGiveText() + TXT_GIVE_END );
        Pickaxe pick = new Pickaxe();
        if (pick.doPickUp( Dungeon.hero )) {
            GLog.i( Hero.TXT_YOU_NOW_HAVE, pick.name() );
        } else {
            Dungeon.level.drop( pick, Dungeon.hero.pos ).sprite.drop();
        }
        Journal.add( Journal.Feature.TROLL );
        return true;
    }

    abstract protected String getRepeatText();

    public Callback repeat(final NPC giver) {
        return new Callback() {
            @Override
            public void call() {
                tell( giver, getRepeatText() );
            }
        };
    }

    // FIXME: Overwriting this seems like a hack. Maybe better create a new optional DONE Stage in Quest
    @Override
    public boolean isGoalReached(NPC questGiver) {
        if (!isReforgingAvailable) {
            return super.isGoalReached(questGiver);
        } else {
            return true;
        }
    }

    protected void onReward() {
        Pickaxe pick = Dungeon.hero.belongings.getItem( Pickaxe.class );
        if (pick.isEquipped( Dungeon.hero )) {
            pick.doUnequip( Dungeon.hero, false );
        }
        pick.detach( Dungeon.hero.belongings.backpack );
    }

    @Override
    public void reward(final NPC questGiver) {
        if ( !isReforgingAvailable ) {
            tell( questGiver, "Oh, you have returned... Better late dan never." );
            onReward();
            isReforgingAvailable = true;
        } else {
            GameScene.show( new WndReforge( (Blacksmith) questGiver, Dungeon.hero ) {
                @Override
                public void onUpgrade() {
                    stage = Stage.REWARDED;
                    isReforgingAvailable = false;  // FIXME: redundant because of the Stage change?
                    questGiver.removeQuest();
                    interact(questGiver);
                }
            } );
        }
    }

    @Override
    public void finish() {
        Journal.remove( Journal.Feature.TROLL );
    }
}
