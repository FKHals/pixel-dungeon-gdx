package com.watabou.pixeldungeon.quests.blacksmith;

import com.watabou.pixeldungeon.Dungeon;
import com.watabou.pixeldungeon.actors.mobs.npcs.NPC;
import com.watabou.pixeldungeon.items.Item;
import com.watabou.pixeldungeon.items.quest.Pickaxe;
import com.watabou.pixeldungeon.quests.Quest;
import com.watabou.pixeldungeon.quests.QuestGoal;
import com.watabou.pixeldungeon.quests.QuestManager;
import com.watabou.pixeldungeon.scenes.GameScene;
import com.watabou.pixeldungeon.windows.WndQuest;
import com.watabou.utils.Callback;

import java.util.Arrays;
import java.util.LinkedList;

public class BloodQuest extends BlacksmithQuest {

    public BloodQuest() {
        super();
    }

    public BloodQuest(int id, int depth, QuestManager questManager) {
        super(id, depth, questManager);
    }

    @Override
    protected LinkedList<QuestGoal> createQuestGoals() {
        QuestGoal pickInInventory = new QuestGoal() {
            @Override
            public boolean isReached(NPC giver) {
                Item pick = Dungeon.hero.belongings.getItem(Pickaxe.class);
                return (pick != null);
            }
            @Override
            protected Callback request(Quest<?> quest, final NPC questGiver) {
                return new Callback() {
                    @Override
                    public void call() {
                        GameScene.show( new WndQuest( questGiver, TXT_MISSING_PICKAXE ) );
                    }
                };
            }
        };
        QuestGoal bloodOnPickaxe = new QuestGoal() {
            @Override
            public boolean isReached(NPC giver) {
                Pickaxe pick = Dungeon.hero.belongings.getItem(Pickaxe.class);
                return ((pick != null) && pick.bloodStained);
            }
        };
        return new LinkedList<>(Arrays.asList(bloodOnPickaxe, pickInInventory));
    }

    @Override
    protected String getGiveText() {
        return "Hey human! Wanna be useful, eh? Take dis pickaxe and _kill a bat_ wit' it, I need "
                + "its blood on the head.";
    }

    @Override
    protected String getRepeatText() {
        return "I said I need bat blood on the pickaxe. Chop chop!";
    }
}
