package com.watabou.pixeldungeon.quests.blacksmith;

import com.watabou.pixeldungeon.Dungeon;
import com.watabou.pixeldungeon.actors.mobs.npcs.NPC;
import com.watabou.pixeldungeon.items.Item;
import com.watabou.pixeldungeon.items.quest.DarkGold;
import com.watabou.pixeldungeon.items.quest.Pickaxe;
import com.watabou.pixeldungeon.quests.Quest;
import com.watabou.pixeldungeon.quests.QuestGoal;
import com.watabou.pixeldungeon.quests.QuestManager;
import com.watabou.pixeldungeon.scenes.GameScene;
import com.watabou.pixeldungeon.windows.WndQuest;
import com.watabou.utils.Callback;

import java.util.Arrays;
import java.util.LinkedList;

public class GoldQuest extends BlacksmithQuest {

    // number of gold pieces/nuggets the player has to mine to complete the quest
    private static final int NUM_GOLD_PIECES = 15;

    public GoldQuest() {
        super();
    }

    public GoldQuest(int id, int depth, QuestManager questManager) {
        super(id, depth, questManager);
    }

    @Override
    protected LinkedList<QuestGoal> createQuestGoals() {
        QuestGoal pickInInventory = new QuestGoal() {
            @Override
            public boolean isReached(NPC giver) {
                Item pick = Dungeon.hero.belongings.getItem(Pickaxe.class);
                return (pick != null);
            }
            @Override
            protected Callback request(Quest<?> quest, final NPC questGiver) {
                return new Callback() {
                    @Override
                    public void call() {
                        GameScene.show( new WndQuest( questGiver, TXT_MISSING_PICKAXE ) );
                    }
                };
            }
        };
        QuestGoal goldInInventory = new QuestGoal() {
            @Override
            public boolean isReached(NPC giver) {
                Item gold = Dungeon.hero.belongings.getItem( DarkGold.class );
                return (gold != null && gold.quantity() >= NUM_GOLD_PIECES);
            }
        };
        return new LinkedList<>(Arrays.asList(goldInInventory, pickInInventory));
    }

    @Override
    protected String getGiveText() {
        return "Hey human! Wanna be useful, eh? Take dis pickaxe and mine me some "
                + "_dark gold ore_, _" + NUM_GOLD_PIECES + " pieces_ should be enough.";
    }

    @Override
    protected String getRepeatText() {
        return "Dark gold ore. " + NUM_GOLD_PIECES + " pieces. Seriously, is it dat hard?";
    }

    @Override
    protected void onReward() {
        super.onReward();
        Item gold = Dungeon.hero.belongings.getItem( DarkGold.class );
        if (gold != null) {
            gold.detachAll( Dungeon.hero.belongings.backpack );
        }
    }
}
