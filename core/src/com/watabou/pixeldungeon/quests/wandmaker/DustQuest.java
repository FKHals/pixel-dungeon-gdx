package com.watabou.pixeldungeon.quests.wandmaker;

import com.watabou.pixeldungeon.Dungeon;
import com.watabou.pixeldungeon.items.Heap;
import com.watabou.pixeldungeon.items.Item;
import com.watabou.pixeldungeon.items.quest.CorpseDust;
import com.watabou.pixeldungeon.quests.QuestManager;
import com.watabou.utils.Random;

import java.util.ArrayList;

public class DustQuest extends WandmakerQuest {

    public DustQuest() {
        super();
    }

    public DustQuest(int id, int depth, QuestManager questManager) {
        super(id, depth, questManager);
    }

    @Override
    protected Class<? extends Item> getQuestItemClass() {
        return CorpseDust.class;
    }

    @Override
    protected String getGiveText() {
        return "Oh, what a pleasant surprise to meet a decent person in such place! I came here "
                + "for a rare ingredient: _corpse dust_. It can be gathered from skeletal remains "
                + "and there is an ample number of them in the dungeon.";
    }

    @Override
    protected void placeItem() {
        ArrayList<Heap> candidates = new ArrayList<Heap>();
        for (Heap heap : Dungeon.level.heaps.values()) {
            if (heap.type == Heap.Type.SKELETON && !Dungeon.visible[heap.pos]) {
                candidates.add( heap );
            }
        }
        if (candidates.size() > 0) {
            Random.element( candidates ).drop( new CorpseDust() );
        } else {
            int pos = Dungeon.level.randomRespawnCell();
            while (Dungeon.level.heaps.get( pos ) != null) {
                pos = Dungeon.level.randomRespawnCell();
            }
            Heap heap = Dungeon.level.drop( new CorpseDust(), pos );
            heap.type = Heap.Type.SKELETON;
            heap.sprite.link();
        }
    }

    @Override
    protected String getRepeatText() {
        return "Any luck with _corpse dust_, %s? Bone piles are the most obvious places to look.";
    }
}

