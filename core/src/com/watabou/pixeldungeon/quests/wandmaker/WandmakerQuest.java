package com.watabou.pixeldungeon.quests.wandmaker;

import com.watabou.pixeldungeon.Dungeon;
import com.watabou.pixeldungeon.Journal;
import com.watabou.pixeldungeon.actors.Actor;
import com.watabou.pixeldungeon.actors.hero.Hero;
import com.watabou.pixeldungeon.actors.mobs.npcs.NPC;
import com.watabou.pixeldungeon.actors.mobs.npcs.Wandmaker;
import com.watabou.pixeldungeon.items.Item;
import com.watabou.pixeldungeon.items.wands.*;
import com.watabou.pixeldungeon.levels.RegularLevel;
import com.watabou.pixeldungeon.levels.Terrain;
import com.watabou.pixeldungeon.quests.Quest;
import com.watabou.pixeldungeon.quests.QuestGoal;
import com.watabou.pixeldungeon.quests.QuestManager;
import com.watabou.pixeldungeon.scenes.GameScene;
import com.watabou.pixeldungeon.utils.GLog;
import com.watabou.pixeldungeon.utils.Utils;
import com.watabou.pixeldungeon.windows.WndQuest;
import com.watabou.utils.Callback;
import com.watabou.utils.Predicate;
import com.watabou.utils.Random;

import java.util.Arrays;
import java.util.LinkedList;

// The Object in Quest<Object> means in this case that the class does not need an Observer because
// otherwise there
/**
 * This is the base class for the three Wandmaker-quests.
 */
public abstract class WandmakerQuest extends Quest<Object> {

    public WandmakerQuest() {
        super();
    }

    public WandmakerQuest(int id, int depth, QuestManager questManager) {
        super(id, depth, questManager);
    }

    abstract protected Class<? extends Item> getQuestItemClass();

    @Override
    protected LinkedList<QuestGoal> createQuestGoals() {
        QuestGoal itemInInventory = new QuestGoal() {
            @Override
            public boolean isReached(NPC giver) {
                Item questItem = Dungeon.hero.belongings.getItem(getQuestItemClass());
                return (questItem != null);
            }
        };
        return new LinkedList<>(Arrays.asList(itemInInventory));
    }

    @Override
    public NPC onSpawn( final RegularLevel level ) {
        Wandmaker npc = new Wandmaker();
        level.placeInRoom(npc, level.getEntranceRoom(), new Random(), new Predicate<Integer>() {
            @Override
            public boolean test(Integer pos) {
                return (level.getCell(pos) != Terrain.ENTRANCE && level.getCell(pos) != Terrain.SIGN);
            }
        });
        Actor.occupyCell( npc );
        npc.setQuest(this);
        return npc;
    }

    private void tell( NPC giver, String format, Object...args ) {
        GameScene.show( new WndQuest( giver, Utils.format( format, args ) ) );
    }

    abstract protected void placeItem();
    abstract protected String getGiveText();

    private static final String TXT_GIVE_REASON =
            " Being a magic user, I'm quite able to defend myself against local monsters, but "
            + "I'm getting lost in no time, it's very embarrassing. Probably you could help me? "
            + "I would be happy to pay for your service with one of my best wands.";

    @Override
    public boolean give(NPC giver) {
        Journal.add( Journal.Feature.WANDMAKER );
        tell( giver, getGiveText() + TXT_GIVE_REASON );
        placeItem();
        return true;
    }

    abstract protected String getRepeatText();

    public Callback repeat(final NPC giver) {
        return new Callback() {
            @Override
            public void call() {
                tell( giver, getRepeatText(), Dungeon.hero.className() );
            }
        };
    }

    private static final String TXT_BATTLE = "Battle wand";
    private static final String TXT_NON_BATTLE = "Non-battle wand";

    private static final String TXT_MESSAGE =
            "Oh, I see you have succeeded! I do hope it hasn't troubled you too much. "
            + "As I promised, you can choose one of my high quality wands.";
    private static final String TXT_FAREWELL = "Good luck in your quest, %s!";

    protected void onReward() {
        Item questItem = Dungeon.hero.belongings.getItem(getQuestItemClass());
        if (questItem != null) {
            questItem.detach( Dungeon.hero.belongings.backpack );
        }
    }

    @Override
    public void reward(final NPC questGiver) {
        GameScene.show(new WndQuest(questGiver, TXT_MESSAGE, TXT_BATTLE, TXT_NON_BATTLE) {
            @Override
            protected void onSelect( int index ) {
                onReward();

                Item reward = index == 0 ? getBattleWandReward() : getNonBattleWandReward();
                reward.identify();
                if (reward.doPickUp( Dungeon.hero )) {
                    GLog.i( Hero.TXT_YOU_NOW_HAVE, reward.name() );
                } else {
                    Dungeon.level.drop( reward, questGiver.pos ).sprite.drop();
                }

                questGiver.yell( Utils.format(TXT_FAREWELL, Dungeon.hero.className() ) );
                questGiver.destroy();

                questGiver.sprite.die();

                // Since this method triggers only by TouchEvents, it is not possible for the quest to wait for the
                // answer if the reward has been taken or not. Instead the next/REWARDED event must be triggered here.
                stage = Stage.REWARDED;
                interact(questGiver);
            }
        });
    }

    //TODO The methods could be moved to the the Generator class for better encapsulation.
    protected Wand getBattleWandReward() {
        Wand wand;
        switch (Random.Int( 5 )) {
            case 0:
                wand = new WandOfAvalanche();
                break;
            case 1:
                wand = new WandOfDisintegration();
                break;
            case 2:
                wand = new WandOfFirebolt();
                break;
            case 3:
                wand = new WandOfLightning();
                break;
            default: // 4
                wand = new WandOfPoison();
                break;
        }
        wand.random().upgrade();
        return wand;
    }

    protected Wand getNonBattleWandReward() {
        Wand wand;
        switch (Random.Int( 5 )) {
            case 0:
                wand = new WandOfAmok();
                break;
            case 1:
                wand = new WandOfBlink();
                break;
            case 2:
                wand = new WandOfRegrowth();
                break;
            case 3:
                wand = new WandOfSlowness();
                break;
           default: // 4
                wand = new WandOfReach();
                break;
        }
        wand.random().upgrade();
        return wand;
    }

    @Override
    public void finish() {
        Journal.remove( Journal.Feature.WANDMAKER );
    }
}
