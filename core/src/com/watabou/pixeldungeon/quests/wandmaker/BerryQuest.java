package com.watabou.pixeldungeon.quests.wandmaker;

import com.watabou.pixeldungeon.Dungeon;
import com.watabou.pixeldungeon.items.Item;
import com.watabou.pixeldungeon.plants.Rotberry;
import com.watabou.pixeldungeon.quests.QuestManager;

public class BerryQuest extends WandmakerQuest {

    public BerryQuest() {
        super();
    }

    public BerryQuest(int id, int depth, QuestManager questManager) {
        super(id, depth, questManager);
    }

    @Override
    protected Class<? extends Item> getQuestItemClass() {
        return Rotberry.Seed.class;
    }

    @Override
    protected String getGiveText() {
        return "Oh, what a pleasant surprise to meet a decent person in such place! I came here "
                + "for a rare ingredient: a _Rotberry seed_.";
    }

    @Override
    protected void placeItem() {
        int shrubPos = Dungeon.level.randomRespawnCell();
        while (Dungeon.level.heaps.get( shrubPos ) != null) {
            shrubPos = Dungeon.level.randomRespawnCell();
        }
        Dungeon.level.plant( new Rotberry.Seed(), shrubPos );
    }

    @Override
    protected String getRepeatText() {
        return "Any luck with a _Rotberry seed_, %s? No? Don't worry, I'm not in a hurry.";
    }
}
