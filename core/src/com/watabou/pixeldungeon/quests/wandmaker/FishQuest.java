package com.watabou.pixeldungeon.quests.wandmaker;

import com.watabou.pixeldungeon.Dungeon;
import com.watabou.pixeldungeon.items.Heap;
import com.watabou.pixeldungeon.items.Item;
import com.watabou.pixeldungeon.items.quest.PhantomFish;
import com.watabou.pixeldungeon.levels.Level;
import com.watabou.pixeldungeon.quests.QuestManager;
import com.watabou.utils.Random;

public class FishQuest extends WandmakerQuest {

    public FishQuest() {
        super();
    }

    public FishQuest(int id, int depth, QuestManager questManager) {
        super(id, depth, questManager);
    }

    @Override
    protected Class<? extends Item> getQuestItemClass() {
        return PhantomFish.class;
    }

    @Override
    protected String getGiveText() {
        return "Oh, what a pleasant surprise to meet a decent person in such place! I came here "
                + "for a rare ingredient: a _phantom fish_. You can catch it with your bare "
                + "hands, but it's very hard to notice in the water.";
    }

    @Override
    protected void placeItem() {
        Heap heap = null;
        for (int i=0; i < 100; i++) {
            int pos = Random.Int( Level.LENGTH );
            if (Level.isWater(pos)) {
                heap = Dungeon.level.drop( new PhantomFish(), pos );
                heap.type = Heap.Type.HIDDEN;
                heap.sprite.link();
                return;
            }
        }
        if (heap == null) {
            int pos = Dungeon.level.randomRespawnCell();
            while (Dungeon.level.heaps.get( pos ) != null) {
                pos = Dungeon.level.randomRespawnCell();
            }
            Dungeon.level.drop( new PhantomFish(), pos );
        }
    }

    @Override
    protected String getRepeatText() {
        return "Any luck with a _phantom fish_, %s? You may want to try searching for it in one "
                + "of the local pools.";
    }
}

