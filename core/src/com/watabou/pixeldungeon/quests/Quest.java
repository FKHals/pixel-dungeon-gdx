/*
 * Pixel Dungeon
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
package com.watabou.pixeldungeon.quests;

import com.watabou.pixeldungeon.actors.mobs.Mob;
import com.watabou.pixeldungeon.actors.mobs.npcs.NPC;
import com.watabou.pixeldungeon.levels.RegularLevel;
import com.watabou.pixeldungeon.levels.Room;
import com.watabou.utils.*;

import java.util.Collections;
import java.util.LinkedList;

public abstract class Quest<Subject> implements Bundlable, EventSubject.Observer<Subject> {

	public enum Stage {
		NONE, SPAWNED, GIVEN, REWARDED;
	}

	public Stage stage = Stage.NONE;
	public int giverId;  //TODO Does the quest even have to know the quest-givers id at the moment?
	public int depth;
	public int id;

	// A list of Room.Types that are require for the quest
	protected LinkedList<Room.Type> requiredRooms;

	//TODO make order explicit by using something like a priority queue or would that be overkill?
	protected LinkedList<QuestGoal> questGoals;

	protected QuestManager questManager;

	/**
	 * This is needed to avoid an InstantiationException due to reflective calls when reloaded
	 * (on restoreFromBundle())
	 */
	public Quest() {
		this(0, -1, null);
		this.giverId= 0;
		// questManager must be set with setQuestManager()
	}

	public Quest(int id, int depth, QuestManager questManager) {
		this.id = id;
		this.depth = depth;
		this.questManager = questManager;
		questGoals = createQuestGoals();
		requiredRooms = createRequiredRooms();
	}

	private static final String GIVER_ID = "giverid";
	private static final String STAGE = "stage";
	private static final String DEPTH = "depth";
	private static final String ID = "id";

	@Override
	public void storeInBundle( Bundle bundle ) {
		bundle.put( STAGE, stage.ordinal() );
		bundle.put( GIVER_ID, giverId);
		bundle.put( DEPTH, depth );
		bundle.put( ID, id );
	}
	
	@Override
	public void restoreFromBundle( Bundle bundle ) {
		stage = Stage.values()[bundle.getInt( STAGE )];
		giverId = bundle.getInt( GIVER_ID );
		depth = bundle.getInt( DEPTH );
		id = bundle.getInt( ID );
	}

	public void setQuestManager(QuestManager questManager) {
		this.questManager = questManager;
	}

	public LinkedList<Room.Type> getRequiredRooms() {
		return requiredRooms;
	}

	/**
	 * @return A list of rooms that are required for the quest
	 */
	protected LinkedList<Room.Type> createRequiredRooms() {
		return new LinkedList<>();
	}

	/**
	 * Returns a list of the quest goals that should be ordered by priority from low to high.
	 * The ascending order is important so that the most important missing goal can be pointed out
	 * by the quest giver when asking what to do again (see {@link Quest#repeat(NPC)})
	 * @return The goal conditions for the quest to be done/ready to be rewarded
	 */
	abstract protected LinkedList<QuestGoal> createQuestGoals();

	/**
	 * This spawns the quest if not yet spawned or adds the quest to a level's event's list of
	 * observers.
	 * The latter is important to make it possible to trigger quest interactions (using
	 * {@link EventSubject<Object>#notifyObservers(Object)} as trigger) even if not at the same
	 * depth the quest was spawned in.
	 */
	public void spawn(RegularLevel level) {
		if (stage == Stage.NONE) {
			giverId = onSpawn(level).id();
			stage = Stage.SPAWNED;
			DLog.debug("QUEST", "Spawned quest (" + this.getClass().getName() + ") with ID: " + id);
		} else if (stage == Stage.REWARDED) {
			// do nothing
			DLog.debug("QUEST", "Quest " + id + " (" + this.getClass().getName() + ") is already rewarded.");
		} else {
			// the depth does not need to be checked since it will only be spawned by the
			// QuestManager into depths that are supposed to be observed by this (defined in
			// getObservedDepths())
			addToLevelEvents(level);
			DLog.debug("QUEST", "Event spawned by Quest " + id + " (" + this.getClass().getName() + ").");
		}
	}

	/**
	 * This defines the necessary quest "scenery" including the quest giver
	 *@return The quest giver NPC (that gets set as the giver of the Quest class)
	 */
	abstract public NPC onSpawn(RegularLevel level);

	// TODO Maybe this mixed traversal solution ist bad (on the one hand by returning/checking
	//  and on the other hand directly setting the stage at the end of the interaction).
	//  It would be better to settle for a homogenous solution (either or or something completely
	//  different).
	/**
	 * This function is used to traverse between the different (dialogue) stages of the quest
	 */
	public void interact(NPC giver) {
		DLog.debug("QUEST", "Interaction with quest " + id + " at stage " + stage);
		switch (stage) {
			//FIXME This could actually be the default case but it feels wrong to put that at the end
			case SPAWNED:
				boolean isGiven = give(giver);
				if (isGiven) {
					stage = Stage.GIVEN;
				}
				break;
			case GIVEN:
				if (isGoalReached(giver)) {
					reward(giver);
				} else {
					DLog.debug("QUEST", "Goals have not been reached for quest: " + id);
				}
				break;
			// This stage is necessary because at the end of the GIVEN stage (when the goals are completed) the player
			// must talk to the quest-giver to get rewarded but this function has no way to know if the player has
			// canceled the reward-window instead of getting the reward so the reward window itself needs to set the
			// REWARDED-stage and trigger the final interact(NPC).
			case REWARDED:
				finish();
				remove();
				break;
			default:
				break;
		}
	}

	/**
	 * Processes the stage where the player is given this quest for the first time.
	 * @return If the stage has been successfully completed
	 */
	abstract public boolean give(NPC giver);

	/**
	 * Processes the stage where this quest is already given to the player but the player wants
	 * the quest giver to repeat the goal of the quest.
	 */
	abstract public Callback repeat(NPC giver);

	//TODO maybe separate the quest-giver-interaction and the boolean logic (in the best case make the function pure)
	// and move out the interaction by returning something like a Callback including a boolean value?
	/**
	 * Checks if all quest goals have been reached and also starts an according interaction with
	 * the player if some goals are not achieved yet.
	 * If all goals have been reached just return true without further interaction.
	 * If none of the goals has been reached the quest-giver should repeat() (If the quests are
	 * in the supposed ascending order, this should always be the case since the last quest would
	 * then automatically be the first issue to be mentioned).
	 * If one or more goals have not been reached the quest-giver should request() (ask for) the
	 * goal with the highest priority.
	 * @return If all the final quest goals (postcondition) have been reached
	 */
	public boolean isGoalReached(NPC questGiver) {
		Callback giverReaction = null;
		boolean allGoalsReached = true;
		for (QuestGoal goal: questGoals) {
			// this requires that the order of the quests in questGoals is ascending from low to
			// high priority since the last not-reached-goal will be the one that will be requested
			if (!goal.isReached(questGiver)) {
				allGoalsReached = false;
				giverReaction = goal.request(this, questGiver);
			}
		}
		// this is the case if all goals have been reached
		if (giverReaction != null) {
			// trigger the interaction with the quest-giver
			giverReaction.call();
		}
		return allGoalsReached;
	};

	/**
	 * Processes the stage where the quest checks if the goal is reached and the player needs to be
	 * rewarded.
	 */
	abstract public void reward(NPC giver);

	/**
	 * Processes the final stage where the quest is done and cleanup is possible.
	 */
	abstract public void finish();

	private void remove() {
		questManager.remove(this);
		DLog.debug("QUEST", "Quest " + id + " (" + this.getClass().getName() + ") deleted");
	}

	/**
	 * @return The depths of the levels that should be observed by this quest. (By default only the
	 * depth gets observed that the quest is spawned in)
	 */
	public LinkedList<Integer> getObservedDepths() {
		return new LinkedList<>(Collections.singletonList(depth));
	}

	// TODO Evaluate if it would be better to better encapsulate the events so something like this
	//      could be used: level.addKillObserver(this)
	/**
	 * Adds this quest as an observer for level events. This is important as a method separate from
	 * {@link #onSpawn(RegularLevel)} to be able to add quests as observers to events of levels
	 * that the quest is not initially spawned in and therefore need to be called separate from the
	 * spawning (in {@link #spawn(RegularLevel)}).
	 */
	private void addToLevelEvents(RegularLevel level) {
		addToLevelKillEvent(level);
	}

	/**
	 * Adds this quest as an observer for a level's kill-events.
	 * @see #addToLevelEvents(RegularLevel)
	 */
	protected void addToLevelKillEvent(RegularLevel level) {
		// FIXME Find a better way to handle this than just casting it
		try {
			level.getMobKillEvent().addObserver((Quest<Mob>) this);
		} catch (ClassCastException e) {
			DLog.error("QUEST", "Trying add non-Mob-Quest " + id + " (" + this.getClass().getName() + ") to a kill event.");
		}
	}

	/**
	 * Notifies the quest when a certain event (e.g. killing a Mob) happens. (standard Observer Pattern)
	 * It is never necessary here to check for the current depth since the quest gets added
	 * exclusively to the depths that it is relevant for (defined by {@link #getObservedDepths()}).
	 * @param subject The object which changed it's condition somehow so that the event got triggered
	 * @param event The event that got triggered
	 */
	@Override
	public void onNotify(Subject subject, EventSubject<Subject> event) {
		// Do nothing, just for (optional) overriding by subclasses that need to observe something
	}

	@Override
	public int getId() {
		return id;
	}
}
