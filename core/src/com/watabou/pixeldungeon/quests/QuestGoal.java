package com.watabou.pixeldungeon.quests;

import com.watabou.pixeldungeon.actors.mobs.npcs.NPC;
import com.watabou.utils.Callback;

public abstract class QuestGoal {

    /**
     * @return If the goal has been reached
     */
    protected abstract boolean isReached(NPC giver);

    /**
     * This gets called when the goal has not been reached and should tell the player what part of
     * the quest goal is outstanding
     */
    protected Callback request(Quest<?> quest, NPC questGiver) {
        return quest.repeat(questGiver);
    }
}
