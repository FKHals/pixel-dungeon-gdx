package com.watabou.utils;

import java.util.HashSet;

//TODO This is kind of a duplicate of utils.Signal but this has the advantage of the Bundable
// implementation and also being a little bit more concise by the use of HashSet

public abstract class EventSubject<E> implements Bundlable{

    private HashSet<Observer<E>> observers = new HashSet<>();

    public abstract int getId(Observer<E> observer);
    public abstract Observer<E> findById(int observerId);

    private static final String OBSERVERS = "observers";

    @Override
    public void restoreFromBundle( Bundle bundle ) {
        observers = new HashSet<>();
        int[] observerIds = bundle.getIntArray( OBSERVERS );
        for (int observerId : observerIds) {
            Observer<E> observer = findById(observerId);
            if (observer != null) {
                observers.add( observer );
            }
        }
    }

    @Override
    public void storeInBundle( Bundle bundle ) {
        int[] observerIds = new int[observers.size()];
        int i = 0;
        for (Observer<E> observer : observers) {
            observerIds[i] = getId(observer);
            i++;
        }
        bundle.put( OBSERVERS, observerIds );
    }

    public void addObserver(Observer<E> observer) {
        observers.add(observer);
    }

    public void removeObserver(Observer<E> observer) {
        observers.remove(observer);
    }

    public void notifyObservers(E subject) {
        for (Observer<E> observer : this.observers) {
            observer.onNotify(subject, this);
        }
    }

    public interface Observer<E> {
        void onNotify(E subject, EventSubject<E> event);
        int getId();
    }
}
