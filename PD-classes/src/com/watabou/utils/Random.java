/*
 * Copyright (C) 2012-2014  Oleg Dolya
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

package com.watabou.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicLong;

/**
 * A wrapper around java.util.Random that implements a couple of useful features
 */
public class Random implements Bundlable {

	private java.util.Random generator;

	public Random() {
		generator = new java.util.Random();
	}

	public Random(long seed) {
		generator = new java.util.Random(seed);
	}

	public double nextDouble() {
		return generator.nextDouble();
	}

	public static double Double() {
		return new Random().nextDouble();
	}

	public float nextFloat() {
		return generator.nextFloat();
	}

	public static float Float() {
		return new Random().nextFloat();
	}

	public float nextFloat( float max ) {
		return this.nextFloat() * max;
	}

	public static float Float( float max ) {
		return new Random().nextFloat(max);
	}

	public float nextFloat( float min, float max ) {
		return min + this.nextFloat() * (max - min);
	}

	public static float Float( float min, float max ) {
		return new Random().nextFloat(min, max);
	}

	/**
	 * @param upperBound Exclusive upper bound
	 */
	public int nextInt( int upperBound ) {
		return upperBound > 0 ? generator.nextInt(upperBound) : 0;
	}

	public static int Int( int upperBound ) {
		return new Random().nextInt(upperBound);
	}

	public int nextInt( int min, int upperBound ) {
		return min + this.nextInt(upperBound - min);
	}

	public static int Int( int min, int max ) {
		return new Random().nextInt(min, max);
	}

	public int nextIntRange( int min, int max ) {
		return this.nextInt(min, max + 1);
	}

	public static int IntRange( int min, int max ) {
		return new Random().nextIntRange(min, max);
	}

	public int nextNormalIntRange( int min, int max ) {
		return Math.round((this.nextFloat(min, max) + this.nextFloat(min, max)) / 2f);
	}

	public static int NormalIntRange( int min, int max ) {
		return new Random().nextNormalIntRange(min, max);
	}

	public int nextChances(float[] chances) {

		int length = chances.length;
		
		float sum = chances[0];
		for (int i=1; i < length; i++) {
			sum += chances[i];
		}
		
		float value = this.nextFloat(sum);
		sum = chances[0];
		for (int i=0; i < length; i++) {
			if (value < sum) {
				return i;
			}
			sum += chances[i + 1];
		}
		
		return 0;
	}

	public static int chances( float[] chances ) {
		return new Random().nextChances(chances);
	}

	@SuppressWarnings("unchecked")
	public <K> K nextChances(HashMap<K, Float> chances) {
		
		int size = chances.size();

		Object[] values = chances.keySet().toArray();
		float[] probs = new float[size];
		float sum = 0;
		for (int i=0; i < size; i++) {
			probs[i] = chances.get( values[i] );
			sum += probs[i];
		}
		
		float value = this.nextFloat(sum);
		
		sum = probs[0];
		for (int i=0; i < size; i++) {
			if (value < sum) {
				return (K)values[i];
			}
			sum += probs[i + 1];
		}
		
		return null;
	}

	public static <K> K chances( HashMap<K,Float> chances ) {
		return new Random().nextChances(chances);
	}
	
	public int nextIndex( Collection<?> collection ) {
		return this.nextInt(collection.size());
	}

	public static int index( Collection<?> collection ) {
		return Int(collection.size());
	}

	public <T> T nextOneOf( T... array ) {
		return array[this.nextInt(array.length)];
	}

	public static<T> T oneOf( T... array ) {
		return new Random().nextOneOf(array);
	}

	public static<T> T element( T[] array ) {
		return oneOf(array);
	}

	@SuppressWarnings("unchecked")
	public <T> T nextElement( Collection<? extends T> collection ) {
		T[] array = (T[]) collection.toArray();
		return array.length > 0 ?
				this.nextOneOf(array) :
				null;
	}

	public static<T> T element( Collection<? extends T> collection ) {
		return new Random().nextElement(collection);
	}

	public <T> T[] nextShuffle(T[] array ) {
		T[] shuffledArray = array.clone();
		for (int i=0; i < shuffledArray.length - 1; i++) {
			int j = this.nextInt( i, shuffledArray.length );
			if (j != i) {
				T t = shuffledArray[i];
				shuffledArray[i] = shuffledArray[j];
				shuffledArray[j] = t;
			}
		}
		return shuffledArray;
	}

	public <T> ArrayList<T> nextShuffle(ArrayList<T> array ) {
		ArrayList<T> shuffledArray = new ArrayList<>(array);
		for (int i=0; i < shuffledArray.size() - 1; i++) {
			int j = this.nextInt( i, shuffledArray.size() );
			if (j != i) {
				T t = shuffledArray.get(i);
				shuffledArray.set(i, shuffledArray.get(j));
				shuffledArray.set(j, t);
			}
		}
		return shuffledArray;
	}

	public static<T> T[] shuffle( T[] array ) {
		return new Random().nextShuffle(array);
	}

	public <U,V> void nextShuffle( U[] u, V[]v ) {
		for (int i=0; i < u.length - 1; i++) {
			int j = this.nextInt( i, u.length );
			if (j != i) {
				U ut = u[i];
				u[i] = u[j];
				u[j] = ut;
				
				V vt = v[i];
				v[i] = v[j];
				v[j] = vt;
			}
		}
	}

	public static<U,V> void shuffle( U[] u, V[]v ) {
		new Random().nextShuffle(u, v);
	}


	/**
	 * @return The seed value of the wrapped java.util.Random object
	 */
	public long getSeed() {
		// source for this implementation:
		// https://stackoverflow.com/questions/6001368/how-do-i-get-the-seed-from-a-random-in-java/29278559#29278559
		long theSeed = Long.MAX_VALUE;
		try {
			@SuppressWarnings("DiscouragedPrivateApi")
				java.lang.reflect.Field field = java.util.Random.class.getDeclaredField("seed");
			field.setAccessible(true);
			AtomicLong scrambledSeed = (AtomicLong) field.get(generator);   //this needs to be XOR'd with 0x5DEECE66DL
			theSeed = scrambledSeed.get();
		} catch (NoSuchFieldException e) {
			System.err.println("The implementation of java.util.Random probably has changed");
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		// this XOR operation is dependent on the implementation of java.util.Random!
		return theSeed ^ 0x5DEECE66DL;
	}

	private static final String SEED = "seed";

	@Override
	public void restoreFromBundle(Bundle bundle) {
		long seed = bundle.getLong( SEED );
		generator = new java.util.Random(seed);
	}

	@Override
	public void storeInBundle(Bundle bundle) {
		bundle.put( SEED, getSeed() );
	}
}
