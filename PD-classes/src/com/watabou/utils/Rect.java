/*
 * Copyright (C) 2012-2014  Oleg Dolya
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

package com.watabou.utils;

import java.util.ArrayList;

public class Rect implements TwoDimensional {

	public int left;
	public int top;
	public int right;
	public int bottom;
	
	public Rect() {
		this( 0, 0, 0, 0 );
	}
	
	public Rect( Rect rect ) {
		this( rect.left, rect.top, rect.right, rect.bottom );
	}

	public Rect( Point point, int width, int height) {
		this(point.x, point.y, width, height);
	}
	
	public Rect( int left, int top, int right, int bottom ) {
		this.left	= left;
		this.top	= top;
		this.right	= right;
		this.bottom	= bottom;
	}

	public Rect(TwoDimensional twoDimensional) {
		this.left	= 0;
		this.top	= 0;
		this.right	= twoDimensional.width();
		this.bottom	= twoDimensional.height();
	}

	@Override
	public int width() {
		return right - left;
	}

	@Override
	public int height() {
		return bottom - top;
	}
	
	public int square() {
		return (right - left) * (bottom - top);
	}
	
	public Rect set( int left, int top, int right, int bottom ) {
		this.left	= left;
		this.top	= top;
		this.right	= right;
		this.bottom	= bottom;
		return this;
	}
	
	public Rect set( Rect rect ) {
		return set( rect.left, rect.top, rect.right, rect.bottom );
	}
	
	public boolean isEmpty() {
		return right <= left || bottom <= top;
	}
	
	public Rect setEmpty() {
		left = right = top = bottom = 0;
		return this;
	}
	
	public Rect intersect( Rect other ) {
		Rect result = new Rect();
		result.left		= Math.max( left, other.left );
		result.right	= Math.min( right, other.right );
		result.top		= Math.max( top, other.top );
		result.bottom	= Math.min( bottom, other.bottom );
		return result;
	}
	
	public Rect union( int x, int y ) {
		if (isEmpty()) {
			return set( x, y, x + 1, y + 1 );
		} else {
			if (x < left) {
				left = x;
			} else if (x >= right) {
				right = x + 1;
			}
			if (y < top) {
				top = y;
			} else if (y >= bottom) {
				bottom = y + 1;
			}
			return this;
		}
	}
	
	public Rect union( Point p ) {
		return union( p.x, p.y );
	}
	
	public boolean inside( Point p ) {
		return p.x >= left && p.x < right && p.y >= top && p.y < bottom;
	}

	/**
	 * @param subtrahend The value (not factor) that is subtracted from each side (width/height)
	 * @return The shrank Rect with width and height shortened by the subtrahend
	 */
	public Rect shrink( int subtrahend ) {
		return new Rect( left + subtrahend, top + subtrahend,right - subtrahend,
				bottom - subtrahend );
	}

	/**
	 * Change dimensions (width, height) without changing the base point in the top left
	 * corner (left, top)
	 */
	public Rect resize(int width, int height) {
		return new Rect(left, top, left + width, top + height);
	}

	/**
	 * @return A list of points that are contained by the Rect
	 */
	public ArrayList<Point> getPointsInside() {
		return getPointsInside(false);
	}

	public ArrayList<Point> getPointsInside(boolean excludeOuterBoundary) {
		int offset = excludeOuterBoundary ? 1 : 0;
		ArrayList<Point> pointsInside = new ArrayList<>();
		for (int y = top + offset; y <= bottom - offset; y++) {
			for (int x = left + offset; x <= right - offset; x++) {
				pointsInside.add(new Point(x, y));
			}
		}
		return pointsInside;
	}
}
