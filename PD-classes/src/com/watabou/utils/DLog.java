package com.watabou.utils;

import com.badlogic.gdx.Gdx;

import java.util.Arrays;

/**
 * A wrapper for logging with LibGDX or STD:ERR if the former is not available (e.g. if using gradle-tasks like
 * desktop:genmap which does not initialize GDX.app)
 * DLog is an abbreviation for "Developer Logging" in contrast to GLog which is the in-game log which the player can
 * access (read on screen)
 */
public class DLog {

    private static void printLogToStdErr(String tag, String message) {
        System.err.println(tag + ": " + message);
    }

    private static void printLogToStdErr(String tag, String message, Throwable exception) {
        System.err.println(tag + ": " + message + ":\n" + Arrays.toString(exception.getStackTrace()));
    }

    public static void log(String tag, String message) {
        if (Gdx.app != null) {
            Gdx.app.log(tag, message);
        } else {
            printLogToStdErr(tag, message);
        }
    }

    public static void log(String tag, String message, Throwable exception) {
        if (Gdx.app != null) {
            Gdx.app.log(tag, message, exception);
        } else {
            printLogToStdErr(tag, message, exception);
        }
    }

    public static void error(String tag, String message) {
        if (Gdx.app != null) {
            Gdx.app.log(tag, message);
        } else {
            printLogToStdErr(tag, message);
        }
    }

    public static void error(String tag, String message, Throwable exception) {
        if (Gdx.app != null) {
            Gdx.app.error(tag, message, exception);
        } else {
            printLogToStdErr(tag, message, exception);
        }
    }

    public static void debug(String tag, String message) {
        if (Gdx.app != null) {
            Gdx.app.log(tag, message);
        } else {
            printLogToStdErr(tag, message);
        }
    }

    public static void debug(String tag, String message, Throwable exception) {
        if (Gdx.app != null) {
            Gdx.app.debug(tag, message, exception);
        } else {
            printLogToStdErr(tag, message, exception);
        }
    }
}
