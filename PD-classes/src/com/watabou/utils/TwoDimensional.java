package com.watabou.utils;

public interface TwoDimensional {
    int width();
    int height();
}
