package com.watabou.utils;

public class RectF {
	public float left;
	public float right;
	public float top;
	public float bottom;

	public RectF(float left, float top, float right, float bottom) {
		this.left = left;
		this.top = top;
		this.right = right;
		this.bottom = bottom;
	}

	public RectF(RectF other) {
		this.left = other.left;
		this.right = other.right;
		this.top = other.top;
		this.bottom = other.bottom;
	}

	public float width() {
		return right - left;
	}

	public float height() {
		return bottom - top;
	}

	public void offset(float dx, float dy) {
		left += dx;
		right += dx;
		top += dy;
		bottom += dy;
	}
}
