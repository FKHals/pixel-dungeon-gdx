package com.watabou.utils;

/**
 * Assignment of a property for each point in a subset of a space (e.g. a vector field)
 * @param <propertyType> Type of elements the field consists of (e.g. force vectors)
 */
public interface Field<propertyType> extends TwoDimensional {

    /**
     * @return Number of points (elements/cells) in the field
     */
    int size();

    /**
     * @param point Point in Field to be set
     * @param value The value that the Point p is set to
     */
    void setCell(Point point, propertyType value);

    /**
     * @param point Point in Field to get the value from
     * @return The value of the Field at the point
     */
    propertyType getCell(Point point);
}
