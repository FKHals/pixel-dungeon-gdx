package com.watabou.utils;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.assertEquals;

public class RectTest {

    @Test
    public void getPointsInsideExcludingPointsOnPerimeter() {
        final Rect rect = new Rect(1, 1, 2, 2);
        ArrayList<Point> expectedPoints = rect.getPointsInside(false);
        ArrayList<Point> actualPoints = new ArrayList<Point>(
                Arrays.asList(
                        new Point(1, 1), new Point(2, 1),
                        new Point(1, 2), new Point(2, 2)
                )
        );
        assertEquals(expectedPoints, actualPoints);
    }

    @Test
    public void getPointsInsideIncludingPointsOnPerimeter() {
        final Rect rect = new Rect(1, 1, 3, 3);
        ArrayList<Point> expectedPoints = rect.getPointsInside(true);
        ArrayList<Point> actualPoints = new ArrayList<Point>(
                Collections.singletonList(
                        new Point(2, 2)
                )
        );
        assertEquals(expectedPoints, actualPoints);
    }
}