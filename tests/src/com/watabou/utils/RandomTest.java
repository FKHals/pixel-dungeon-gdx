package com.watabou.utils;

import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class RandomTest {

	/**
	 * This test is necessary to be sure that the implementation of java.util.Random has not changed since the
	 * implementation of watabou.utilsRandom.getSeed() depends on technical details of java.util.Random
	 */
	@Test
	public void getSeed() {
		final long expectedSeed = 0;
		Random random = new Random(expectedSeed);
		long actualSeed = random.getSeed();
		assertEquals(expectedSeed, actualSeed);
	}
}
