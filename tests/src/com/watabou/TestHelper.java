package com.watabou;

import com.watabou.pixeldungeon.levels.Level;

import java.util.Arrays;

public class TestHelper {
    /**
     * Linebreak the printed array content to make reading the debugging message easier
     */
    public static String addLineBreaksAtWidth(String commaSeparatedElements, String separator) {
        StringBuilder expectedMapRepresentation = new StringBuilder().append("\n");
        int commaCount = 0;
        for (String mapElement: commaSeparatedElements.split(",")) {
            // add whitespace to single-digit (which already includes a trailing whitespace) entries
            // so that they line up with double-digit entries which makes the maps more readable
            expectedMapRepresentation.append(mapElement).append(separator).append(mapElement.length() > 2 ? "" : " ")
                    .append(commaCount % Level.WIDTH == Level.WIDTH - 1 ? "\n": "");
            commaCount++;
        }
        return expectedMapRepresentation.toString();
    }

    /**
     * @return A string that shows the human-readable comparison of two map-arrays (so that there
     * are linebreak at the level.WIDTH)
     */
    public static String arrayComparison(int[] expectedMap, int[] actualMap) {
        return "Expected: " + addLineBreaksAtWidth(Arrays.toString(expectedMap), "")
                + "\nActual:   " + addLineBreaksAtWidth(Arrays.toString(actualMap), "") + "\n";
    }
}
