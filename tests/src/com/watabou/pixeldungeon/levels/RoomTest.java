package com.watabou.pixeldungeon.levels;

import com.watabou.utils.*;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;

public class RoomTest {

    final static Room ROOM = new Room();
    static {
        ROOM.top = 1;
        ROOM.right = 5;
        ROOM.bottom = 3;
        ROOM.left = 1;
    }

    final static TwoDimensional LEVEL = new TwoDimensional() {
        @Override
        public int width() {
            return 4;
        }
        @Override
        public int height() {
            return 4;
        }
    };

    final static long SEED = 69;

    @Test
    public void random() {

        final Point expectedPoint = new Point(2, 2);
        final Integer expectedCPos = expectedPoint.y * LEVEL.width() + expectedPoint.x;

        Optional<Integer> actualPosition = ROOM.random(new Predicate<Integer>() {
            @Override
            public boolean test(Integer pos) {
                return pos.equals(expectedCPos);
            }
        }, LEVEL, new Random(SEED));
        if (actualPosition.isPresent()) {
            assertEquals(expectedCPos, actualPosition.get());
        }
    }

    @Test
    public void multipleDistinctRandom() {
        // purposely set the count higher than the number of available points to check that the
        // function can deal with that
        final int COUNT = 3;

        /*
             0  1  2  3  4
          1  #  #  #  #  #
          2  #  X  _  X  #
          3  #  #  #  #  #
          #: wall (unpassable)
          X: expected (filtered) point (passable)
          _: empty (passable)
         */
        final Point expectedPoint = new Point(2, 2);
        final ArrayList<Point> expectedPoints = new ArrayList<>(
                Arrays.asList(new Point(4, 2), new Point(2, 2))
        );
        final ArrayList<Integer> expectedPositions = new ArrayList<>();
        for (Point p: expectedPoints) {
            expectedPositions.add(p.y * LEVEL.width() + p.x);
        };

        Optional<ArrayList<Integer>> optPositions =
                ROOM.multipleDistinctRandom(COUNT, new Predicate<Integer>() {
                    @Override
                    public boolean test(Integer pos) {
                        return pos % 2 == 0;
                    }}, LEVEL, new Random(SEED)
                );
        if (optPositions.isPresent() && !optPositions.get().isEmpty()) {
            ArrayList<Integer> actualPositions = optPositions.get();
            // check the sets since the order may be changed
            //assertEquals(new HashSet<>(expectedPositions), new HashSet<>(actualPositions));
            assertEquals(expectedPositions, actualPositions);
        }
    }
}
