package com.watabou.pixeldungeon.levels.painters;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertArrayEquals;

import org.junit.Test;

import java.lang.reflect.Field;
import java.util.Arrays;

import com.watabou.TestHelper;

import com.watabou.pixeldungeon.levels.Level;
import com.watabou.pixeldungeon.levels.Terrain;
import com.watabou.utils.Point;
import com.watabou.utils.Rect;

public class PainterTest {

	private static Level level = new Level() {
		@Override
		protected boolean build(GenerationConstraints constraints) {
			return true;
		}
		@Override
		protected void decorate(GenerationConstraints constraints) {}
		@Override
		protected void createMobs(GenerationConstraints constraints) {}
		@Override
		protected void createItems(GenerationConstraints constraints) {}
	};

	private static int[] filledMap;
	
	private static Field map;

	// initialize (private) map field in Level using reflection since Level is abstract and all
	// child-class uses constructors that are not made to be used in isolation (and therefore fail)
	static {
		filledMap = new int[Level.LENGTH];
		Arrays.fill(filledMap, Terrain.WALL.getValue());

		map = null;
		try {
			map = Level.class.getDeclaredField("map");
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		}

		assert map != null;
		map.setAccessible(true);

		resetLevelMap();
	}

	private static void resetLevelMap() {
		try {
			map.set(level, filledMap.clone());
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void setting() throws IllegalAccessException {

		resetLevelMap();

		Painter.set( level, 0, Terrain.EMPTY );
		assertEquals(level.getCell(0), Terrain.EMPTY);

		Painter.set( level, 1, 0, Terrain.EMPTY );
		assertEquals(level.getCell(1), Terrain.EMPTY);

		Painter.set( level, new Point(2, 0), Terrain.EMPTY );
		assertEquals(level.getCell(2), Terrain.EMPTY);

		// adjust expected map for comparison
		int[] expectedMap = filledMap.clone();
		Arrays.fill(expectedMap, 0, 3, Terrain.EMPTY.getValue());

		int[] actualMap = (int[]) map.get(level);
		assertArrayEquals(
				TestHelper.arrayComparison(expectedMap, actualMap),
				expectedMap,
				actualMap
		);
	}

	@Test
	public void filling() throws IllegalAccessException {

		resetLevelMap();
		int[] expectedMap = filledMap.clone();
		Arrays.fill(expectedMap, 0, Level.WIDTH, Terrain.EMPTY.getValue());
		Painter.fill( level, 0, 0, Level.WIDTH, 1, Terrain.EMPTY );
		int[] actualMap = (int[]) map.get(level);
		assertArrayEquals(
				TestHelper.arrayComparison(expectedMap, actualMap),
				expectedMap,
				actualMap
		);

		resetLevelMap();
		expectedMap = filledMap.clone();
		Arrays.fill(expectedMap, 0, Level.WIDTH * 3 + 1, Terrain.EMPTY.getValue());
		Painter.fill( level, new Rect(0, 0, Level.WIDTH, 2), Terrain.EMPTY );
		actualMap = (int[]) map.get(level);
		assertArrayEquals(
				TestHelper.arrayComparison(expectedMap, actualMap),
				expectedMap,
				actualMap
		);

		resetLevelMap();
		expectedMap = filledMap.clone();
		Arrays.fill(expectedMap, Level.WIDTH + 1, Level.WIDTH * 2, Terrain.EMPTY.getValue());
		Arrays.fill(expectedMap, Level.WIDTH * 2 + 1, Level.WIDTH * 3, Terrain.EMPTY.getValue());
		Painter.fill( level, new Rect(0, 0, Level.WIDTH, 3), 1, Terrain.EMPTY );
		actualMap = (int[]) map.get(level);
		assertArrayEquals(
				TestHelper.arrayComparison(expectedMap, actualMap),
				expectedMap,
				actualMap
		);
	}
}
