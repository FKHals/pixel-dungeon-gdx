package com.watabou.pixeldungeon;

import static org.junit.Assert.assertArrayEquals;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.lang.reflect.Field;
import java.util.HashSet;

import com.watabou.TestHelper;

import com.watabou.pixeldungeon.actors.hero.HeroClass;
import com.watabou.pixeldungeon.items.Generator;
import com.watabou.pixeldungeon.scenes.StartScene;
import com.watabou.pixeldungeon.levels.Level;
import com.watabou.utils.Random;


@RunWith(GdxTestRunner.class)
public class DungeonTest {

    private int[] getMapFromLevel(Level level) {
        int[] map;

        Field mapField = null;
        try {
            mapField = Level.class.getDeclaredField("map");
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }

        assert mapField != null;
        mapField.setAccessible(true);

        try {
            map = (int[]) mapField.get(level);
        } catch (java.lang.IllegalAccessException e) {
            e.printStackTrace();
            return null;
        }

        return map;
    }

    /**
     * Initialize all components that are used during Dungeon-/Level-generation so that no problems
     * arise when executing Dungeon.init() or Dungeon.newLevel()
     */
    private void prepareDungeonContext() {
        // populate Badges.global with something non-null to prevent calls to Game.instance for
        // restoring the actual value which is not of interest in this test (see Badges.loadGlobal())
        Field badgesGlobalField = null;
        try {
            badgesGlobalField = Badges.class.getDeclaredField("global");
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        assert badgesGlobalField != null;
        badgesGlobalField.setAccessible(true);
        try {
            badgesGlobalField.set(null, new HashSet<Badges.Badge>());
        } catch (java.lang.IllegalAccessException e) {
            e.printStackTrace();
        }

        // populate StartScene.curClass with something non-null to prevent NullPointerExceptions
        Field startSceneCurClassField = null;
        try {
            startSceneCurClassField = StartScene.class.getDeclaredField("curClass");
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        assert startSceneCurClassField != null;
        startSceneCurClassField.setAccessible(true);
        try {
            startSceneCurClassField.set(null, HeroClass.WARRIOR);
        } catch (java.lang.IllegalAccessException e) {
            e.printStackTrace();
        }

        // prevent another NullPointerException
        Generator.reset();
    }

    /**
     * Determine if the dungeon generation (the sequential generation of multiple levels)
     * works deterministically when using the same seed which would be the desired property.
     * The levels get generated twice until the same depth, and compared for equality.
     */
    @Test
    public void levelGenerationDeterminism() {

        prepareDungeonContext();

        final long SEED = new Random().getSeed();
        final boolean createMobsAndItems = false;
        final int testDepthBound = 26;
        Level actualLevel;
        Level expectedLevel;
        int[][] actualMap = new int[testDepthBound][Level.LENGTH];
        int[][] expectedMap = new int[testDepthBound][Level.LENGTH];

        Dungeon.init(new Random(SEED));

        for (int depth = 0; depth < testDepthBound; depth++) {
            actualLevel = Dungeon.newLevel(createMobsAndItems);
            actualMap[depth] = getMapFromLevel(actualLevel);
        }

        // emulate starting a new game
        Dungeon.init(new Random(SEED));

        for (int depth = 0; depth < testDepthBound; depth++) {
            expectedLevel = Dungeon.newLevel(createMobsAndItems);
            expectedMap[depth] = getMapFromLevel(expectedLevel);
        }

        // compare every single generated level layout with the corresponding previously generated
        // layout from the first pass
        for (int depth = 0; depth < testDepthBound; depth++) {
            assertArrayEquals(
                    "Depth: " + depth + "\n" +
                            TestHelper.arrayComparison(expectedMap[depth], actualMap[depth]),
                    expectedMap,
                    actualMap
            );
        }
    }

    /**
     * Determine if the dungeon generation has changed unintentionally by comparing with a
     * reference dungeon layout
     */
    @Test
    public void levelGenerationReferenceEquality() {

        prepareDungeonContext();

        final boolean createMobsAndItems = false;
        final int testDepthBound = 26;
        Level actualLevel;
        int[][] actualMap = new int[testDepthBound][Level.LENGTH];

        Dungeon.init(new Random(DungeonTestReference.SEED));

        for (int depth = 0; depth < testDepthBound; depth++) {
            actualLevel = Dungeon.newLevel(createMobsAndItems);
            actualMap[depth] = getMapFromLevel(actualLevel);
        }

        // uncomment the next line to regenerate the code needed for the DungeonTestReference to STD:ERR.
        //DungeonTestReference.createReferenceArrayCode(actualMap);

        // compare every single generated level layout with the corresponding reference layout
        for (int depth = 0; depth < testDepthBound; depth++) {
            assertArrayEquals(
                    "Depth: " + depth + "\n" +
                            TestHelper.arrayComparison(
                                    actualMap[depth],
                                    DungeonTestReference.LEVELS()[depth]
                            ),
                    actualMap[depth],
                    DungeonTestReference.LEVELS()[depth]);
        }
    }
}
